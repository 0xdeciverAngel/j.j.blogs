---
title: Redis - 第四章 | 可視化工具
date: 2019-04-08 15:23:21
categories:
- Redis
tags: 
- Redis
- Redis Desktop Manager
---

這邊介紹一下幾款Redis的可視化工具，因為在開發操作Redis的過程中，一直透過指令方式，其實是又花時間又不夠直覺，以下這邊推薦幾款可視化工具，供選擇使用。

## [Redis Desktop Manager](https://redisdesktop.com/)
Redis Desktop Manager（RedisDesktopManager，RDM）是一個快速、簡單、支持跨平台的 Redis 桌面管理工具，基於 Qt 5 開發，支持通過 SSH Tunnel 連接。

![](http://redisdesktop.com/static/docs/rdm_main.png)

![](http://redisdesktop.com/static/docs/rdm_main2.png)

>Redis Desktop Manager 真的滿好用的，但是目前最新的Windwos、Mac版本是需要收費，而Linux版本是免費。
>這邊提供一個 [Redis桌面管理工具 V0.9.3.39](http://www.pc6.com/mac/486661.html) 下載地點，如有疑慮請勿下載唷。

<br />
## [FastoRedis](https://fastoredis.com/)
FastoRedis（FastoNoSQL的分支）- 是一個跨平台的開源Redis管理工具（即Admin GUI）。 它為Redis的redis-cli外殼提供了相同的引擎。 你可以用redis-cli shell寫的所有東西 - 你可以用Fastoredis寫的！ 我們的程序適用於大多數Linux系統，也適用於Windows，Mac OS X，FreeBSD和Android平台。

![](https://fastoredis.com/images/slider/main-1.png)

![](https://fastoredis.com/images/slider/main-2.png)

![](https://fastoredis.com/images/slider/main-3.png)

>我安裝FastoRedis在Mac容易閃退⋯⋯所以我後來就沒在使用這款了。

<br />
## [TreeNMS](http://www.treesoft.cn/index.html)
Treesoft 又分 TreeDMS、TreeNMS
- TreeDMS支援：MySQL，Oracle，PostgreSQL, SQL Server, MongoDB, Hive
- TreeNMS支援：Redis, memcached

### TreeDMS

![](http://www.treesoft.cn/images/51.gif)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/redis/chapter4/01.png)

### TreeNMS

![](http://www.treesoft.cn/images/81.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/redis/chapter4/02.png)

>這款看起來非常強大，但是我自己是沒有使用過的唷。

<br />
>註：以上參考了
[有什么好用的redis可视化管理工具？](https://www.zhihu.com/question/57728399)
[八一八你用过的Redis可视化工具](https://www.azurew.com/9245.html)

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>