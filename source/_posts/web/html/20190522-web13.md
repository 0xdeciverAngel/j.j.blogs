---
title: Web - 第十三章 | HTML 將向量圖形添加到Web
date: 2019-05-22 10:25:00
categories:
- Web
tags: 
- Web
- HTML
---

在看到這篇文章以前，我是真的沒有研究了解過，可能因為不是主攻這塊的麻，但是學無邊界，就是看到有稍微沾到邊的，基本上都要去了解，哪怕某一天可以拿來用或是解決問題是吧。

文中提到，向量圖形在很多情況下非常有用 — 它們擁有較小的文件尺寸，卻高度可縮放，所以它們不會在鏡頭拉近或者放大圖像時像素化，這邊的目的並不是教你SVG；僅僅是告訴你它是什麼，以及如何在網頁中添加它。

## 什麼是向量圖形？
在網上，你會和兩種類型的圖片打交道 — 點陣圖和向量圖:

點陣圖使用像素網格來定義—一個點陣圖文件精確得包含了每個像素的位置和它的色彩信息。流行的點陣圖格式包括Bitmap ( .bmp), PNG ( .png), JPEG ( .jpg), and GIF ( .gif.)
向量圖使用算法來定義—一個向量圖文件包含了圖形和路徑的定義，電腦可以根據這些定義計算出當它們在屏幕上渲染時應該呈現的樣子。 SVG格式可以讓我們創造用於Web的精彩的向量圖形。

它並排展示了兩個看起來一致的圖像。不同的是，左邊的是PNG，而右邊的是SVG圖像。

<img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Bitmap_VS_SVG.svg/300px-Bitmap_VS_SVG.svg.png" decoding="async" width="300" height="192" class="thumbimage" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Bitmap_VS_SVG.svg/450px-Bitmap_VS_SVG.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Bitmap_VS_SVG.svg/600px-Bitmap_VS_SVG.svg.png 2x" data-file-width="512" data-file-height="327">

當你放大網頁的時候，區別就會變得明顯起來— 隨著你的放大，PNG 圖片變得像素化了，因為它存儲是每個像素的顏色和位置信息— 當它被放大時，每個像素就被放大以填滿屏幕上更多的像素，所以圖像就會開始變得馬賽克感覺。向量圖像看起來仍然效果很好且清晰，因為無論它的尺寸如何，都使用算法來計算出圖像的形狀，僅僅是根據放大的倍數來調整算法中的值。

>此外，向量圖形相較於同樣的點陣圖，通常擁有更小的體積，因為它們僅需儲存少量的算法，而不是逐個儲存每個像素的信息。

<br/>
## SVG是什麼？
SVG  是用於描述向量圖像的XML語言。它基本上是像HTML一樣的標記，只是你有許多不同的元素來定義要顯示在圖像中的形狀，以及要應用於這些形狀的效果。SVG用於標記圖形，而不是內容。非常簡單，你有一些元素來創建簡單圖形，如&lt;circle&gt; 和&lt;rect&gt;。更高級的SVG功能包括  &lt;feColorMatrix&gt;（使用變換矩陣轉換顏色）&lt;animate&gt; （向量圖形的動畫部分）和  &lt;mask&gt;（在圖像頂部應用蒙版）

```html
<svg version="1.1"
     baseProfile="full"
     width="300" height="200"
     xmlns="http://www.w3.org/2000/svg">
  <rect width="100%" height="100%" fill="black" />
  <circle cx="150" cy="100" r="90" fill="blue" />
</svg>
```

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter13/01.png)

您可以在文本編輯器中手動編寫簡單的SVG，但是對於復雜的圖像，這很快就開始變得非常困難。為了創建SVG圖像，大多數人使用向量圖形編輯器，如  Inkscape  或  Illustrator。這些軟件包允許您使用各種圖形工具創建各種插圖，並創建照片的近似值（例如Inkscape的跟踪點陣圖功能）。

SVG除了迄今為止所描述的以外還有其他優點：

- 向量圖像中的文本仍然可訪問（這也有利於  SEO )）。
- SVG 可以很好地適應樣式/腳本，因為圖像的每個組件都是可以通過CSS或通過JavaScript編寫的樣式的元素。

那麼為什麼會有人想使用柵格圖形而不是SVG？其實SVG 確實有一些缺點：

- SVG非常容易變得複雜，這意味著文件大小會增加; 複雜的SVG也會在瀏覽器中佔用很長的處理時間。
- SVG可能比柵格圖像更難創建，具體取決於您嘗試創建哪種圖像。
- 舊版瀏覽器不支持SVG，因此如果您需要在網站上支持舊版本的IE，則可能不適合（SVG從IE9開始得到支持）。

由於上述原因，柵格圖形更適合照片那樣複雜精密的圖像。

<br/>
## 將SVG添加到頁面

### 快捷方式：&lt;img&gt;
要通過&lt;img&gt;元素嵌入SVG，你只需要按照預期的方式在src屬性中引用它。你將需要一個height或width屬性（或者如果您的SVG沒有固有的寬高比）。

```html
<img 
    src="equilateral.svg" 
    alt="triangle with all three sides equal"
    height="87px"
    width="100px" />
```

優點

- 快速，熟悉的圖像語法與alt屬性中提供的內置文本等效。
- 可以通過在&lt;a&gt;元素嵌套<img>，使圖像輕鬆地成為超鏈接。

缺點
- 無法使用JavaScript操作圖像。
- 如果要使用CSS控制SVG內容，則必須在SVG代碼中包含內聯CSS樣式。（從SVG文件調用的外部樣式表不起作用）
- 不能用CSS偽類來重設圖像樣式（如:focus）。

### 疑難解答和跨瀏覽器支持
對於不支持SVG（IE 8及更低版本，Android 2.3及更低版本）的瀏覽器，您可以從src屬性引用PNG或JPG，並使用srcset屬性只有最近的瀏覽器才能識別）來引用SVG。在這種情況下，僅支持瀏覽器將加載SVG -較舊的瀏覽器將加載PNG。

```html
<img src="equilateral.png" alt="triangle with equal sides" srcset="equilateral.svg">
```

您還可以使用SVG作為CSS背景圖像，如下所示。在下面的代碼中，舊版瀏覽器會堅持他們理解的PNG，而較新的瀏覽器將加載SVG：

```css
background: url("fallback.png") no-repeat center;
background-image: url("image.svg");
background-size: contain;
```

像上面描述的&lt;img&gt;方法一樣，使用CSS背景圖像插入SVG意味著它不能被JavaScript操作，並且也受到相同的CSS限制。

如果SVG根本沒有顯示，可能是因為你的服務器設置不正確。如果是這個問題，[這篇文章](https://developer.mozilla.org/zh-CN/docs/Web/SVG/Tutorial/Getting_Started#A_Word_on_Webservers)將告訴你正確方向。

### 如何在HTML中引入SVG代碼
在文本編輯器中打開SVG文件，複製SVG代碼，並將其粘貼到HTML文檔中-這有時稱為將SVG內聯或內聯SVG。確保您的SVG代碼在<svg></svg>標籤中（不要在外面添加任何內容）。

```html
<svg width="300" height="200">
    <rect width="100%" height="100%" fill="green" />
</svg>
```

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter13/02.png)

優點

- 將SVG 內聯減少HTTP 請求，可以減少加載時間。
- 您可以為SVG元素分配class和id，並使用CSS修改樣式，無論是在SVG中，還是HTML文檔中的CSS樣式規則。實際上，您可以使用任何  SVG外觀屬性  作為CSS屬性。
- 內聯SVG是唯一可以讓您在SVG圖像上使用CSS交互（如:focus）和CSS動畫的方法（即使在常規樣式表中）。
- 您可以通過將SVG標記包在&lt;a&gt;元素中，使其成為超鏈接。

缺點

- 這種方法只適用於在一個地方使用的SVG。多次使用會導致資源密集型維護（resource-intensive maintenance）。
- 額外的SVG 代碼會增加HTML文件的大小。
- 瀏覽器不能像緩存普通圖片一樣緩存內聯SVG。
- 您可能會在&lt;foreignObject&gt;元素中包含回退，但支持SVG的瀏覽器仍然會下載任何後備圖像。你需要考慮僅僅為支持過時的瀏覽器，而增加額外開銷是否真的值得。

### 如何使用&lt;iframe&gt;嵌入SVG 

```html
<iframe src="triangle.svg" width="500" height="500" sandbox>
    <img src="triangle.png" alt="Triangle with three unequal sides" />
</iframe>
```

這絕對不是最好的方法：

缺點

- 如你所知，iframe有一個回退機制，如果瀏覽器不支持iframe，則只會顯示回退。
- 此外，除非SVG和您當前的網頁具有相同的origin，否則你不能在主頁面上使用JavaScript來操縱SVG。

>補充：點陣圖（Bitmap），又稱柵格圖（英語：Raster graphics）或位圖，是使用像素陣列(Pixel-array/Dot-matrix點陣)來表示的圖像。

>這邊轉載了大量的文章和內容，但是每個部分我都有實際去做過，並了解過。這邊轉載主要是做筆記記錄。

>註：以上參考了
[MDN web docs](https://developer.mozilla.org/zh-TW/) 的 [Adding vector graphics to the Web
](https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Adding_vector_graphics_to_the_Web) 文章。
[MDN web docs](https://developer.mozilla.org/zh-TW/) 的 [Getting Started
](https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Getting_Started) 文章。
[維基百科](https://zh.wikipedia.org/wiki/Wikipedia:%E9%A6%96%E9%A1%B5) 的 [可縮放向量圖形](https://zh.wikipedia.org/wiki/%E5%8F%AF%E7%B8%AE%E6%94%BE%E5%90%91%E9%87%8F%E5%9C%96%E5%BD%A2) 文章。

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>