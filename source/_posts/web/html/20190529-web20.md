---
title: Web - 第二十章 | HTML 原生Form表單組件(二)
date: 2019-05-29 10:00:00
categories:
- Web
tags: 
- Web
- HTML
---

延續前面[Web - 第十九章 | HTML 原生Form表單組件(一)](https://morosedog.gitlab.io/j.j.blogs/web-html-20190528-web19/)，這章將會紀錄「可選中項(Checkable items)」、「按鈕(Buttons)」、「高級表單組件(Advanced form widgets)」、「其他小組件(Other widgets)」。

## 可選中項(Checkable items)
可選中項是狀態可以通過單擊它們來更改小組件。有兩種可選中項：複選框和單選按鈕。兩者都使用`checked`屬性，以指示該組件的默認狀態:"選中"或"未選中"。

值得注意的是，這些小組件與其他表單小組件不一樣。對於大多數表單組件，一旦表單提交，所有具有`name`屬性的小組件都會被發送，即使沒有任何值被填。對於可選中項，只有在勾選時才發送它們的值。如果他們沒有被勾選，就不會發送任何東西，甚至連他們的名字也沒有。

為了獲得最大的可用性和可訪問性，建議您在&lt;fieldset&gt;中包圍每個相關項目的列表，並使用&lt;legend&gt;提供對列表的全面描述。每個單獨的&lt;label&gt;/ &lt;input&gt;元素都應該包含在它自己的列表項中(或者類似的)。

您還需要為這些類型的輸入提供value屬性，如果您想讓它們具有意義——如果沒有提供任何值，則復選框和單選按鈕被賦予一個on值。

簡單的範例：(複選框、單選按鈕)

```html
<form>
   <fieldset>
      <legend>選擇你喜歡的所有蔬菜</legend>
      <ul>
         <li>
            <label for="carrots">蘿蔔</label>
            <input type="checkbox" checked id="carrots" name="carrots" value="carrots">
         </li>
         <li>
            <label for="peas">碗豆</label>
            <input type="checkbox" id="peas" name="peas" value="peas">
         </li>
         <li>
            <label for="cabbage">捲心菜</label>
            <input type="checkbox" id="cabbage" name="cabbage" value="cabbage">
         </li>
         <li>
            <label for="cauli">花椰菜</label>
            <input type="checkbox" id="cauli" name="cauli" value="cauli">
         </li>
         <li>
            <label for="broc">青花菜</label>
            <input type="checkbox" id="broc" name="broc" value="broc">
         </li>
      </ul>
   </fieldset>
   <fieldset>
      <legend>你最喜歡吃什麼？</legend>
      <ul>
         <li>
            <label for="soup">湯</label>
            <input type="radio" checked id="soup" name="meal" value="soup">
         </li>
         <li>
            <label for="curry">咖哩</label>
            <input type="radio" id="curry" name="meal" value="curry">
         </li>
         <li>
            <label for="pizza">比薩</label>
            <input type="radio" id="pizza" name="meal" value="pizza">
         </li>
         <li>
            <label for="tacos">玉米餅</label>
            <input type="radio" id="tacos" name="meal" value="tacos">
         </li>
         <li>
            <label for="bolognaise">番茄肉醬</label>
            <input type="radio" id="bolognaise" name="meal"  value="bolognaise">
         </li>
      </ul>
   </fieldset>
</form>
```

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter20/01.png)

### 複選框(Check box)
使用type屬性值為checkbox的&lt;input&gt;元素來創建一個複選框。

>注：包含checked屬性使復選框在頁面加載時自動被選中。

```html
<input type="checkbox" checked id="carrots" name="carrots" value="carrots">
```

### 單選按鈕(Radio button)
使用type屬性值為radio的&lt;input&gt;元素來創建一個單選按鈕。

```html
<input type="radio" checked id="soup" name="meal" value="soup">
```

幾個單選按鈕可以連接在一起。如果它們的name屬性共享相同的值，那麼它們將被認為屬於同一組的按鈕。同一組中只有一個按鈕可以同時被選；這意味著當其中一個被選中時，所有其他的都將自動未選中。如果沒有選中任何一個，那麼整個單選按鈕池就被認為處於未知狀態，並且沒有以表單的形式發送任何值。

<br/>
## 按鈕(Buttons)
在HTML表單中，有三種按鈕：

- Submit：將表單數據發送到服務器。
- Reset：將所有表單小組件重新設置為它們的默認值。
- Button：沒有自動生效的按鈕，但是可以使用JavaScript代碼進行定制。如果您省略了type屬性，那麼這就是默認值。

使用&lt;button&gt;元素或者&lt;input&gt;元素來創建一個按鈕。type屬性的值指定顯示什麼類型的按鈕。


### 提交(submit)
```html
<form>
   <p>
      <button type="submit">
      這是<br><strong>&lt;button&gt;的提交按鈕</strong>
      </button>
   </p>
   <p>
      <input type="submit" value="&lt;input&gt;type=submit的提交按鈕">
   </p>
```

### 重置(reset)
```html
   <p>
      <button type="reset">
      這是<br><strong>&lt;button&gt;的重置按鈕</strong>
      </button>
   </p>
   <p>
      <input type="reset" value="&lt;input&gt;type=reset的重置按鈕">
   </p>
```

### 匿名(anonymous)
```html
   <p>
      <button type="button">
      這是<br><strong>&lt;button&gt;的匿名按鈕</strong>
      </button>
   </p>
   <p>
      <input type="button" value="&lt;input&gt;type=button的匿名按鈕">
   </p>
</form>
```

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter20/02.png)

不管您使用的是&lt;button&gt;元素還是&lt;input&gt;元素，按鈕的行為都是一樣的。然而，有一些顯著的不同之處。

- 使用&lt;button&gt;元素允許您在它們的標籤中使用HTML內容，這些內容被插入到打開和關閉&lt;button&gt;標籤中。另一方面，&lt;input&gt;元素是空元素;它們的標籤被插入到value屬性中，因此只接受純文本內容。

- 使用&lt;button&gt;元素，可以有一個不同於按鈕標籤的值(通過將其設置為value屬性)。這在IE 8之前的版本中是不可靠的。

>從技術上講，使用&lt;button&gt;元素或&lt;input&gt;元素定義的按鈕幾乎沒有區別。唯一值得注意的區別是按鈕本身的標籤。在&lt;input&gt;元素中，標籤只能是字符數據，而在&lt;button&gt;元素中，標籤可以是HTML，因此可以相應地進行樣式化。

<br/>
## 高級表單組件(Advanced form widgets)
我覺得這篇很酷，本來沒有這麼常在接觸使用，沒想這邊有這麼多組件，可以豐富我的表單。

### 數字(Numbers)
用於數字的小組件是用&lt;input&gt;元素創建的，它的type屬性設置為number。這個控件看起來像一個文本域，但是只允許浮點數，並且通常提供一些按鈕來增加或減少小組件的值。

- 通過設置min和max屬性來約束該值。
- 通過設置step屬性來指定增加和減少按鈕更改小組件的值的數量。

下方創建一個數字小組件，其值被限制為1到10之間的任何值，而其增加和減少按鈕的值將更改為2。

```html
<input type="number" name="age" id="age" min="1" max="10" step="2">
```

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter20/03.gif)

>注：在10以下的Internet Explorer版本中不支持number輸入。

### 滑塊(Sliders)
另一種選擇數字的方法是使用滑塊。從視覺上講，滑塊比文本字段更不准確，因此它們被用來選擇一個確切值並不重要的數字。

滑塊是通過把&lt;input&gt;元素的type屬性值設置為range來創建的。正確配置滑塊是很重要的；為了達到這個目的，強烈建議您設置min、max和step屬性。


```html
<input type="range" name="beans" id="beans" min="0" max="500" step="10">
<span class="beancount">250</span>
```

```javascript
   var beans = document.querySelector('#beans');
   var count = document.querySelector('.beancount');

   count.textContent = beans.value;

   beans.oninput = function() {
     count.textContent = beans.value;
   }
```

滑塊的一個問題是，它們不提供任何形式的視覺反饋，以了解當前的值是什麼。您需要使用JavaScript來添加這一點，但這相對來說比較容易。在本例中，我們添加了一個空的&lt;span&gt;元素，其中我們將寫入滑塊的當前值，並在更改時更新它。

這裡我們將對范圍輸入的引用和兩個變量的span存儲在這裡，然後我們立即將span的textContent設置為輸入的當前value。最後，我們設置了一個oninput事件處理程序，以便每次移動範圍滑塊時，都會將span textContent更新為新的輸入值。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter20/04.gif)

>注：在10以下的Internet Explorer版本中不支持range 。

### 日期時間選擇器(Date and time picker)
使用&lt;input&gt;元素和一個type屬性的適當的值來創建日期和時間控制，這取決於您是否希望收集日期、時間或兩者。

#### 本地時間
創建一個小組件來顯示和選擇一個日期，但是沒有任何特定的時區信息。

```html
<input type="datetime-local" name="datetime" id="datetime">
```

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter20/05.gif)

#### 月
創建了一個小組件來顯示和挑選一個月。

```html
<input type="month" name="month" id="month">
```
![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter20/06.gif)

#### 時間
創建一個小組件來顯示並選擇一個時間值。

```html
<input type="time" name="time" id="time">
```

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter20/07.gif)

#### 星期
創建一個小組件來顯示並挑選一個星期號和它的年份。

```html
<input type="week" name="week" id="week">
```

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter20/08.gif)

所有日期和時間控制都可以使用min和max屬性來約束。

```html
<input type="date" name="myDate" min="2013-06-01" max="2013-08-31" id="myDate">
```

>警告：日期和時間窗口小組件仍然很不受支持。目前，Chrome、Edge和Opera都支持它們，但IE瀏覽器沒有支持，Firefox和Safari對這些都沒有太大的支持。

### 顏色選擇器(Color picker)
顏色總是有點難處理。有很多方式來表達它們:RGB值(十進製或十六進制)、HSL值、關鍵字等等。顏色小組件允許用戶在文本和可視的方式中選擇顏色。

顏色小組件是使用&lt;input&gt;元素創建的，它的type屬性設置為值color。

```html
<input type="color" name="color" id="color">
```

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter20/09.gif)

>警告：並不是所有瀏覽器都支持拾色器。IE中沒有支持，Safari目前也不支持它。其他主要的瀏覽器都支持它。

## 其他小組件
這邊的組件，也非常有卻，我覺得最有趣的部分是圖像為傳送點擊的x,y值，而像其他的我基本上都有碰過，所以還好。而進度條這部分，我較為少使用，不過也是非常有趣的一部分。

### 文件選擇器
創建一個文件選擇器小組件，您可以使用&lt;input&gt;元素，它的type屬性設置為file。被接受的文件類型可以使用accept屬性來約束。此外，如果您想讓用戶選擇多個文件，那麼可以通過添加multiple屬性來實現。

```html
<input type="file" name="file" id="file" accept="image/*" multiple>
```

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter20/10.gif)

### 隱藏內容
有時候，由於技術原因，有些數據是用表單發送的，但不顯示給用戶，這有時是很方便的。要做到這一點，您可以在表單中添加一個不可見的元素。要做到這一點，需要使用&lt;input&gt;和它的type屬性設置為hidden值。

```html
<input type="hidden" id="timestamp" name="timestamp" value="1286705410">
```

### 圖像按鈕
圖像按鈕控件是一個與&lt;img&gt;元素完全相同的元素，除了當用戶點擊它時，它的行為就像一個提交按鈕(見上面)。

圖像按鈕是使用&lt;input&gt;元素創建的，該元素的type屬性設置為image值。這個元素支持與&lt;img&gt;元素相同的屬性，加上其他表單按鈕支持的所有屬性。

```html
<input type="image" alt="Click me!" src="my-img.png" width="80" height="30" />
```

如果使用圖像按鈕來提交表單，這個小組件不會提交它的值；相反，在圖像上單擊的X和Y坐標是被提交的(坐標是相對於圖像的，這意味著圖像的左上角表示坐標0，0)，坐標被發送為兩個鍵/值對：

- X值鍵是name屬性的值，後面是字符串“.x”。
- Y值鍵是name屬性的值，後面是字符串“.y”。

當您點擊這個小組件的圖像時，您將被發送到一個URL。

```url
http://foo.com?pos.x=123&pos.y=456
```

這是構建“熱圖”的一種非常方便的方式。

### 儀表和進度條
儀表和進度條是數值的可視化表示。

#### 進度條
一個進度條表示一個值，它會隨著時間的變化而變化到最大的值，這個值由max屬性指定。這樣的一個bar是使用&lt;progress&gt;元素創建的。

```html
<progress max="100" value="75">75/100</progress>
```

這是為了實現任何需要進度報告的內容，例如下載的總文件的百分比，或者問卷中填寫的問題的數量。

>&lt;progress&gt;元素中的內容是不支持該元素的瀏覽器的回退，以及輔助技術對其發出的聲音。

#### 儀表
一個儀表表示一個固定值，這個值由一個min和一個max值所定。這個值是作為一個條形顯示的，並且為了知道這個工具條是什麼樣子的，我們將這個值與其他一些設置值進行比較

- low和high值範圍劃分為三個部分：
  - 該範圍的較低部分是在min和low值(包括那些值)之間。
  - 該範圍的中間部分是在low和high值之間(不包括那些值)。
  - 該範圍的較高部分是在high和max值(包括那些值)之間。


- optimum值定義了&lt;meter&gt;元素的最優值。在與htmlattrxref(“low”、“meter”)和high值的聯合中，它定義了該範圍的哪個部分是優先的：
  - 如果optimum值在較低的範圍內，則較低的範圍被認為是首選項，中等範圍被認為是平均值，而較高的範圍被認為是最壞的部分。
  - 如果optimum值在該範圍的中等部分，則較低的範圍被認為是一個平均值，中等範圍被認為是優先的部分，而較高的範圍也被認為是平均值。
  - 如果optimum值在較高的範圍內，則較低的範圍被認為是最壞的部分，中等範圍被認為是一般的部分，較高的範圍被認為是優先的部分。
  
所有實現&lt;meter&gt;元素的瀏覽器都使用這些值來改變米尺的顏色。

- 如果當前值位於該範圍的優先部分，則該條是綠色的。
- 如果當前值位於該範圍的平均部分，則該條是黃色的。
- 如果當前值處於最糟糕的範圍，則該條是紅色的。

這樣的一個工具欄是使用&lt;meter&gt;元素創建的。這是用於實現任何類型的儀表，例如一個顯示磁盤上使用的總空間的條，當它開始滿時，它會變成紅色。

```html
<meter min="0" max="100" value="75" low="33" high="66" optimum="50">75</meter>
```

&lt;meter&gt;元素中的內容是不支持該元素的瀏覽器的回退，以及輔助技術對其發出的聲音。

>注：對進度條和儀表的支持是相當不錯的，在Internet Explorer中沒有支持，但是其他瀏覽器支持它。

>這邊轉載了大量的文章和內容，但是每個部分我都有實際去做過，並了解過。這邊轉載主要是做筆記記錄。

>註：以上參考了
[MDN web docs](https://developer.mozilla.org/zh-TW/) 的 [The native form widgets](https://developer.mozilla.org/en-US/docs/Learn/HTML/Forms/The_native_form_widgets) 文章。

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>