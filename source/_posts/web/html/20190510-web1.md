---
title: Web - 第一章 | HTML介紹 (一)
date: 2019-05-10 09:00:00
categories:
- Web
tags: 
- Web
- HTML
---

HTML是什麼？超文字標籤語言 (Hypertext Markup Language；HTML) 可用以建構網頁內容，並賦予其含意和用途。
例如某段內容要
- 分段落
- 項目符號
- 插入圖片
- 資料表格
這些都是透過HTML來實現的。

<br />
## HTML 是什麼？
HTML (Hypertext Markup Language) 並不是一種程式語言，而是一種用來告訴瀏覽網頁的人該如何組織該網頁的標記式語言(markup language)，它可以依照網頁開發者的意願變得困難或簡單。HTML包含一系列的元素(elements)讓你圍住、包裹或標記不同部分的內容，使得它們得以呈現出不同的風格樣式。被標籤(tags)包住的內容會變成超連結，或者斜體字，以及諸如此類的功能。

建立一個簡單的範例：
建立一個test.html的檔案，並且在裡面寫入
```html
This is J.J.Huang test. This is J.J.Huang test2.
```

使用瀏覽器將其打開，可以看到呈現的頁面如下：

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter1/01.png)

我們想要讓`This is J.J.Huang test.`這行字獨立出來，不讓它跟其他東西排在一起，我們可以用段落標籤(paragraph tag <p>)讓它自成段落：

```html
<p>This is J.J.Huang test.</p> This is J.J.Huang test2.
```

使用瀏覽器將其打開，可以看到呈現的頁面如下：

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter1/02.png)

>注意：在HTML中元素是不區分大小寫的。
例如 :  &lt;title&gt;
這個元素可以被撰寫成&lt;title&gt;, &lt;TITLE&gt;, &lt;Title&gt;, &lt;TiTlE&gt;，之類的，都可以正常運作。
但請培養良好的習慣，所有的元素請用小寫，為了保持一致性跟可讀性還有其他可能的原因。

<br />
## HTML元素分析

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter1/03.png)

- 起始標籤(opening tag)：起始標籤包含了這個元素的名字(在這裡是 p)，夾在 <、> (angle brackets) 中間。

- 結束標籤(closing tag)：結束標籤和起始標籤長得差不多，不過在名字前面多了一條斜線 (forward slash) 。

- 內容(content)：這個元素的內容。上面例子中就是一段字。

- 元素(element)：以上全部加起來就是元素。

### 自己動手試試看
我們可以使用 [jsfiddle線上程式碼編輯器](https://jsfiddle.net/) 網站。

我們可以在HTML那邊輸入以下元素，然後點選左上角的**Run**，就會立刻將結果顯示在右下角了。
```html
<p>this is J.J.Huang test.</p>
<em>12345</em>
```

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter1/04.png)

### 巢狀元素(Nesting elements)
我們可以把一個元素，放在另一個元素中做使用，但是元素的起始和結束標籤的順序要注意，記得由內而外去做關閉的順序就不會錯了。

我們一樣使用線上程式編輯器做示範，在HTML裡面輸入
```html
<p>My name is J.J.Huang, you can call me <strong>J.J.</strong></p>
```
[&lt;strong&gt;](https://www.w3schools.com/tags/tag_strong.asp)是用來強調這裡面的內容(content)，會將其變粗體。

在上述範例中我們先用了 p 元素 , 然後才用 strong 元素 , 所以我們必須先關 strong 元素 , 然後再關 p 元素。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter1/05.png)

錯誤範例：
```
<p>My name is J.J.Huang, you can call me <strong>J.J.</p></strong>
```

### 區塊級元素 vs. 內聯元素(Block versus inline elements)
在HTML中有兩個你應該要知道的、重要的元素類別—塊元素(block-level elements)以及內聯元素(inline elements)。

- 區塊級元素來自於它們是一個頁面中一個個可見的區塊。任何經過區塊元素的內容，不論從前面或者後面經過，都會出現在新的一行。在頁面中，區塊級元素更加傾向於結構化元素(structural elements)，舉例來說，比如圖片、清單、導航選單(navigation menus)、置底(footers)等等。一個區塊級元素不應該被放在內聯元素的巢狀結構中，但區塊級元素有可能被放在同為區塊級元素的巢狀結構當中。 

- 內聯元素指的是放在區塊級元素中的那些元素，而且這些元素只被一小部分的文件內容圍繞而非一整段而且群聚的內容。一個內聯元素不會在文件中產生新的一行，內聯元素只會普通的出現在一段文字中，舉例來說比如 &lt;a&gt; element (hyperlink) 或者斜體，比如說 &lt;em&gt; 或&lt;strong&gt;。

範例：
```html
<em>first</em><em>second</em><em>third</em>

<p>fourth</p><p>fifth</p><p>sixth</p>
```

&lt;em&gt; 是一個內聯元素，所以你可以看到下面的圖中，前三個元素互相緊鄰在同一行，兩兩中間並無任何空白。而 &lt;p&gt; 是一個塊元素，所以每個元素都自成一行，並且上下都有一些空間。(這些空間是由於瀏覽器套用預設的CSS styling到這些段落上的緣故).

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter1/06.png)


### 空元素(Empty elements)
不是所有元素都是起始標籤、內容、結束標籤的格式。有些元素只有一個標籤，這些標籤通常用來在文件中插入/嵌入物件。例如 <img> 元素是用來嵌入圖片檔。

```html
<img src="https://gitlab.com/MoroseDog/j.j.blogs/raw/master/source/images/other/QrCode.png">
```
效果如下:
可以看到我們在網頁上面插入了一張圖片。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/web/html/chapter1/07.png)

>我相信介紹到這邊應該有很多需要做吸收了，所以這一部分到這邊為止，後面會再繼續書說明屬性(Attributes)、解析HTML文檔、特殊字元引用方式、HTML註解方式⋯等等。


>註：以上參考了
[MDN web docs](https://developer.mozilla.org/zh-TW/) 的 [Getting started with HTML
](https://developer.mozilla.org/zh-TW/docs/Learn/HTML/Introduction_to_HTML/Getting_started) 文章。

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>