---
title: x64dbg - 第二十七章 | 反匯編練習(十六)
date: 2019-09-01 09:00:00
categories:
- x64dbg
tags: 
- x64dbg
---

## 目標程式
檔案下載：[urlegal33.exe](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/download/x64dbg/chapter27/urlegal33.7z)
解壓密碼：morosedog

<br>
## 程式簡介
`URLegal`能檢查你的書籤文件並找出無效鏈接。你能選擇測試單一文件或是檢查子目錄，文件中的每一`HTTP URL`將自動通過連接到目標網站和接收具體的`HTML`頁面進行測試，結果在一文本窗口中進行展示。

<br>
## 前置作業

- 下載目標程式並解壓縮
- 執行`SETUP.EXE`，一路`Next`安裝
- `Finish`開啟主程式

<br>
## 任務目標

- 移除所有的`Nag`視窗

<br>
## 分析程式

- 執行`URLegal.exe`
- 開啟主程式
- 點擊`File`
- 點擊`Register...`
- 彈出`Registering`視窗
- 輸入`Name`、`Code`
- 點擊`Validata My Codes`
- 彈出`Name / Code mis-match. Try again.` (名稱/代碼不匹配。 再試一次)

- 關閉主程式
- 彈出`How did you like URLegal?`視窗
- 點擊`右上角X`
- 會自對開啟`Windows 說明及支援`

- 點擊`I will Register Soon`
- 正常關閉主程式

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter27/01.gif)

檢驗顯示是使用`Microsoft Visual C++ 6.0`編寫。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter27/02.png)

<br>
## 額外補充

- 模式對話框，[DialogBoxParam](https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-dialogboxparama)

```C
INT_PTR DialogBoxParamA(
  HINSTANCE hInstance,
  LPCSTR    lpTemplateName,
  HWND      hWndParent,
  DLGPROC   lpDialogFunc,
  LPARAM    dwInitParam
);
```

- 無模式對話框，[CreateDialogParam](https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-createdialogparama)

```C
HWND CreateDialogParamA(
  HINSTANCE hInstance,
  LPCSTR    lpTemplateName,
  HWND      hWndParent,
  DLGPROC   lpDialogFunc,
  LPARAM    dwInitParam
);
```

- `hInstance`，類型：`HINSTANCE`：包含對話框模板的模塊句柄。如果此參數為`NULL`，則使用當前可執行文件。
  - `hInstance`對`Windwos`來說是一個獨一無二的數字，是用來標記這個對話框的句柄

- 兩個模式的對話框，區別為是否允許使用者在不同窗口間進行切換；模式對話框為不允許，非模式對話框允許。

- 模式對話框，是由`Windwos`為它內建一個消息循環；非模式對話框，則是通過使用者程序中的消息循環派送。

- 模式對話框的簡單範例：
![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter27/03.gif)

<br>
## 輔助程式
檔案下載：[eXeScope.exe](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/download/x64dbg/chapter27/eXeScope650.7z)
解壓密碼：morosedog

<br>
## 輔助程式簡介
這是一套可將程式中的英文字挑出來加以修改、翻譯，將軟體轉成中文的操作介面來，它包括可更改選單`menu`、字體`font`、對話方塊`dialog`，它還能直接修改`EXE`，`DLL`，`OCX`..`etc`.

<br>
## 輔助程式前置作業

- 下載目標程式並解壓縮
- 右鍵點選`eXeScope.exe`
- 點選`內容`
- 點選`相容性`
- 勾選`以系統管理員的身份執行此程式`

>註：不然開啟程式都會是僅可讀取模式，無法進行寫入修改。

## 輔助程式使用簡單示範

- 開啟`eXeScope.exe`
- 點擊`文件(F)`->`打開()O`
- 選擇`C:\Program Files (x86)\URLegal\Urlegal.exe`
  - 建議先備份一份`Urlegal.exe`

- 展開`資源`->`對話框`
- 找尋標題為`How did you like URLegal?`

- 點擊`測試顯示` (可以預覽該對話框)
- 點擊`對話框編輯器(&D)` (可以編輯對話框的物件位置、大小..等等)

- 編輯對話框內的網址
- `Dialog: 103`、`Static: http://www.worldlynx.net/pgerhart/`
  > 注意：`103 (十進制)`，`0x67 (十六進制)`表示為`Nag`視窗的`hInstance`
- 修改`http://www.worldlynx.net/pgerhart/` -> `https://morosedog.gitlab.io/j.j.blogs/`

- 點擊`保持更新`
- 點擊`確定`

- 執行`URLegal.exe`
- 開啟主程式
- 關閉主程式
- 彈出`How did you like URLegal?`視窗
- `URL`已經確定被修改了

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter27/04.gif)

<br>
## 搜尋思路

- 使用搜尋字串找關鍵字
- 根據堆疊的調用，判斷產生`Nag`視窗的函數位置
- 使用`eXeScope`工具查找關鍵位址

<br>
## 修改思路

- 針對彈出的`Nag`視窗做處理

<br>
## 實際分析

- 開啟`URLegal.exe`
- 於反匯編視窗點選右鍵選擇`搜尋(S)`->`目前模組`->`指令(O)`
- 輸入`push 0x67`
```C
位址       反組譯    
00401240 push 67
```
- 點擊`位址=00401240 反組譯=push 67`
- 跳轉到`00401240   | 6A 67                 | push 67                                  |`

- 向上觀察
```C
00401220   | 55                    | push ebp                                 |
00401221   | 8BEC                  | mov ebp,esp                              |
00401223   | 6A FF                 | push FFFFFFFF                            |
00401225   | 68 05744100           | push urlegal.417405                      |
0040122A   | 64:A1 00000000        | mov eax,dword ptr fs:[0]                 |
00401230   | 50                    | push eax                                 |
00401231   | 64:8925 00000000      | mov dword ptr fs:[0],esp                 |
00401238   | 51                    | push ecx                                 |
00401239   | 894D F0               | mov dword ptr ss:[ebp-10],ecx            |
0040123C   | 8B45 08               | mov eax,dword ptr ss:[ebp+8]             |
0040123F   | 50                    | push eax                                 |
00401240   | 6A 67                 | push 67                                  |
```
- 找尋`push ebp`入口點
- `00401220   | 55                    | push ebp                                 |`右鍵選擇`尋找參考(R)`->`選定的位址(S)`
- 可見以下位址參考
```C
位址       反組譯                
004023BB call urlegal.401220
```
- 點擊`位址=004023BB 反組譯=call urlegal.401220`
- 跳轉到`004023BB   | E8 60EEFFFF           | call urlegal.401220                      |`

- 向上觀察
```C
004023AA   | E8 691A0000           | call urlegal.403E18                      |
004023AF   | 85C0                  | test eax,eax                             |
004023B1   | 75 43                 | jne urlegal.4023F6                       |
004023B3   | 6A 00                 | push 0                                   |
004023B5   | 8D8D 98FEFFFF         | lea ecx,dword ptr ss:[ebp-168]           |
004023BB   | E8 60EEFFFF           | call urlegal.401220                      |
```
- 會發現`004023B1   | 75 43                 | jne urlegal.4023F6                       |`跳轉未實現
- 可跳過`004023BB   | E8 60EEFFFF           | call urlegal.401220                      |`

- `test eax,eax`影響`jne`的跳轉結果
- `call urlegal.403E18`會異動`eax`的值
- `004023AA`設定中斷點
- 移除其他中斷點
- `F9`執行程式
- 關閉主程式

- 斷點在`004023AA   | E8 691A0000           | call urlegal.403E18                      |`
- `F7`步入

- 觀察程式
```C
00403E18   | 55                    | push ebp                                 |
00403E19   | 8BEC                  | mov ebp,esp                              |
00403E1B   | 51                    | push ecx                                 | ecx:"艇A"==&"甋A"
00403E1C   | 894D FC               | mov dword ptr ss:[ebp-4],ecx             |
00403E1F   | 8B45 FC               | mov eax,dword ptr ss:[ebp-4]             |
00403E22   | 8B40 2C               | mov eax,dword ptr ds:[eax+2C]            |
00403E25   | 8BE5                  | mov esp,ebp                              |
00403E27   | 5D                    | pop ebp                                  |
00403E28   | C3                    | ret                                      |
```
- `F8`一步一步過，並持續觀察

- 步過到下方指令時
```C
00403E1F   | 8B45 FC               | mov eax,dword ptr ss:[ebp-4]             |
00403E22   | 8B40 2C               | mov eax,dword ptr ds:[eax+2C]            |
```
- `mov eax,dword ptr ss:[ebp-4]`與`mov eax,dword ptr ds:[eax+2C]`會異動`eax`的值

- `00403E1F   | 8B45 FC               | mov eax,dword ptr ss:[ebp-4]             |`按下空白鍵
- 將指令修改為`mov eax, 0x1`，按下確定
- 修改後如下
```C
// 修改前
00403E18   | 55                    | push ebp                                 |
00403E19   | 8BEC                  | mov ebp,esp                              |
00403E1B   | 51                    | push ecx                                 | ecx:"艇A"==&"甋A"
00403E1C   | 894D FC               | mov dword ptr ss:[ebp-4],ecx             |
00403E1F   | 8B45 FC               | mov eax,dword ptr ss:[ebp-4]             |
00403E22   | 8B40 2C               | mov eax,dword ptr ds:[eax+2C]            |
00403E25   | 8BE5                  | mov esp,ebp                              |
00403E27   | 5D                    | pop ebp                                  |
00403E28   | C3                    | ret                                      |
// 修改後
00403E18   | 55                    | push ebp                                 |
00403E19   | 8BEC                  | mov ebp,esp                              |
00403E1B   | 51                    | push ecx                                 | ecx:"艇A"==&"甋A"
00403E1C   | 894D FC               | mov dword ptr ss:[ebp-4],ecx             |
00403E1F   | B8 01000000           | mov eax,1                                |
00403E24   | 90                    | nop                                      |
00403E25   | 8BE5                  | mov esp,ebp                              |
00403E27   | 5D                    | pop ebp                                  |
00403E28   | C3                    | ret                                      |
```
- `F9`執行程式
- 主程式正常關閉
- 沒有彈出`Nag`視窗

<br>
## 分析總結

- `004023AA`判斷是否註冊的位址
- `00403E1F`、`00403E22`判斷是否跳轉關鍵

<br>
## 修改思路

-  修改直接賦予`eax`的值為`0x1`

<br>
## 實際修改

- 開啟`URLegal.exe`
- 反匯編視窗中按下`Ctrl + G`輸入`00403E1F`，會跳轉到`00403E1F`位址
- `00403E1F   | 8B45 FC               | mov eax,dword ptr ss:[ebp-4]             |`按下空白鍵
- 將指令修改為`mov eax, 0x1`，按下確定
- 修改後如下
```C
// 修改前
00403E18   | 55                    | push ebp                                 |
00403E19   | 8BEC                  | mov ebp,esp                              |
00403E1B   | 51                    | push ecx                                 | ecx:"艇A"==&"甋A"
00403E1C   | 894D FC               | mov dword ptr ss:[ebp-4],ecx             |
00403E1F   | 8B45 FC               | mov eax,dword ptr ss:[ebp-4]             |
00403E22   | 8B40 2C               | mov eax,dword ptr ds:[eax+2C]            |
00403E25   | 8BE5                  | mov esp,ebp                              |
00403E27   | 5D                    | pop ebp                                  |
00403E28   | C3                    | ret                                      |
// 修改後
00403E18   | 55                    | push ebp                                 |
00403E19   | 8BEC                  | mov ebp,esp                              |
00403E1B   | 51                    | push ecx                                 | ecx:"艇A"==&"甋A"
00403E1C   | 894D FC               | mov dword ptr ss:[ebp-4],ecx             |
00403E1F   | B8 01000000           | mov eax,1                                |
00403E24   | 90                    | nop                                      |
00403E25   | 8BE5                  | mov esp,ebp                              |
00403E27   | 5D                    | pop ebp                                  |
00403E28   | C3                    | ret                                      |
```

- 點擊`修補程式` 或是快捷鍵`Ctrl + P`
- 點擊`修補檔案(P)`
- 另存檔名`URLegal.crack.exe`
- 恭喜補丁產生`URLegal.crack.exe`

<br>
## 心得分享

- 搜尋的方式這邊僅示範了`尋找參考`
- 亦可使用暫停的方式，找尋堆疊也可找到關鍵位置

<br />
>註：以上參考了
[x64dbg](https://x64dbg.com/#start)
[x64dbg’s documentation!](http://help.x64dbg.com/en/latest/)
[CSDN billvsme的专栏](https://blog.csdn.net/billvsme) 的 [OllyDbg 使用笔记 （十五）](https://blog.csdn.net/billvsme/article/details/39076261)

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>