---
title: x64dbg - 第二章 | 認識x64dbg
date: 2019-07-22 09:00:00
categories:
- x64dbg
tags: 
- x64dbg
---

## x64dbg CPU視窗介紹

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter2/01.png)

<br>
## x64dbg CPU視窗細部介紹

1. 跳轉標示
2. 程式執行時的記憶體位址
3. 16進制的機器碼
4. 組合語言的語法
5. 註解
6. 組合語言執行狀態
  - `MOV`的語法，會顯示原來的值
  - `JMP`語法，會顯示是否會跳到哪行
7. 記憶體的內容，共有5個`Dump`，可同時觀察多個記憶體位址的內容
8. 命令輸入位置

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter2/02.png)

<br>
## x64dbg 語言調整
- 語言包路徑：`/snapshot_2019-07-02_16-06/release/translations`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter2/03.png)

<br>
## x64dbg 外觀顏色、字型調整
- `選項`->`外觀`->`顏色`
- `選項`->`外觀`->`字型`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter2/04.gif)

<br>
## x64dbg 快捷鍵設定
基本上快捷鍵與`OllyDBG`為相同

- `選項`->`快捷鍵`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter2/05.gif)

<br>
## x64dbg 偏好設定

- `選項`->`偏好設定`->`事件`
  - 建議先將`系統中斷點 *`取消打勾
- `選項`->`偏好設定`->`引擎`
- `選項`->`偏好設定`->`異常`
  - 建議新增忽略異常範圍`00000000 ~ FFFFFFFF`
- `選項`->`偏好設定`->`反組譯`
- `選項`->`偏好設定`->`介面`
- `選項`->`偏好設定`->`雜項`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter2/06.gif)

<br>
## x64dbg 外掛(Plugins)
可以通過將* .dp32（x32插件）或* .dp64（x64插件）複製到“plugins”目錄來安裝插件。

- x32dbg plugins 路徑`/snapshot_2019-07-02_16-06/release/x32\plugins`
- x64dbg plugins 路徑`/snapshot_2019-07-02_16-06/release/x64\plugins`

- [x64dbg Plugins 列表](https://github.com/x64dbg/x64dbg/wiki/Plugins)

<br>
## x64dbg 圖示快捷鍵

1. 開啟`F3`
2. 重新啟動`Ctrl + F2`
3. 關閉`Alt + F2`
4. 執行`F9`
5. 暫停`F12`
6. 單步步入`F7`
7. 單步步過`F8`
8. 跟蹤步入`Ctrl + Alt + F7`
9. 跟蹤步過`Ctrl + Alt + F8`
10. 執行到返回`Ctrl + F9`
11. 執行到使用者代碼`Alt + F9`
12. Scylla`Ctrl + I`
13. 修補程式`Ctrl + P`
14. 註解`Ctrl + Alt + C`
15. 標籤`Ctrl + Alt + L`
16. 書籤`Ctrl + Alt + B`
17. 函數`Ctrl + Alt + F`
18. 變數
19. 尋找字串
20. 搜尋跨模組呼叫
21. 計算機
22. 檢查更新

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter2/hotkey_icon.png)

<br>
## x64dbg 檢視視窗快捷鍵

1. CPU`Alt + C`
2. 日誌視窗`Alt +L`
3. 筆記
4. 中斷點`Alt + B`
5. 記憶體映射`Alt + M`
6. 呼叫堆疊`Alt + K`
7. SEH 鏈
8. 腳本`Alt + S`
9. 符號資訊`Ctrl + Alt + S`
10. 模組`Alt + E`
11. 原始碼`Ctrl + Shift + S`
12. 引用`Alt + R`
13. 執行緒`Alt + T`
14. Handles
15. 圖形`Alt + G`
16. 追蹤
17. 修補程式檔`Ctrl + P`
18. 註解`Ctrl + Alt + C`
19. 標籤`Ctrl + Alt + L`
20. 書籤`Ctrl + Alt + B`
21. 函數`Ctrl + Alt + F`
22. 變數
23. 指令`Ctrl + Return`
24. 前一個分頁`Alt + Left`
25. 下一個分頁`Alt + Right`
26. 前一個視圖`Ctrl + Shift + Tab`
27. 後一個視圖`Ctrl + Tab`
28. 隱藏分頁欄`Ctrl + W`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter2/07.png)

<br>
## x64dbg 除錯快捷鍵

1. 執行`F9`
2. 執行到選取處`F4`
3. 暫停`F12`
4. 重新啟動`Ctrl + F2`
5. 關閉`Alt + F2`
6. 單步步入`F7`
7. 單步步過`F8`
8. 執行到返回`Ctrl +F9`
9. 執行到使用者代碼`Alt + F9`
10. 進階
 - 執行 (拋出列外)`Shift +F9`
 - 執行 (忽略例外)`Ctrl + Alt + Shift +F9`
 - Run until expression`Shift +F4`
 - 單部步入 (拋出例外)`Shift +F7`
 - 單部步入 (忽略例外)`Ctrl + Alt + Shift +F7`
 - 單部步入 (source)`F11`
 - 單部步過 (拋出例外)`Shift +F8`
 - 單部步過 (忽略例外)`Ctrl + Alt + Shift +F8`
 - 單部步過 (source)`F10`
 - 執行到返回 (拋出列外)`Ctrl + Shift + F9`
 - 跳過下一行指令
 - Undo last instruction`Alt + U`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter2/08.png)

<br>
## x64dbg 追蹤快捷鍵

1. 自動步入`Ctrl + F7`
2. 自動步過`Ctrl + F8`
3. 自動運行命令
4. 跟蹤步入`Ctrl + Alt + F7`
5. 跟蹤步過`Ctrl + Alt + F8`
6. 追蹤紀錄

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter2/09.png)

<br />
>註：以上參考了
[x64dbg](https://x64dbg.com/#start)
[x64dbg’s documentation!](http://help.x64dbg.com/en/latest/)

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>