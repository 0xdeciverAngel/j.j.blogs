---
title: x64dbg - 第九章 | 反匯編練習(二) 上
date: 2019-07-29 09:00:00
categories:
- x64dbg
tags: 
- x64dbg
---

## 前置作業
因為要反匯編的程式是簡體字，避免在分析過程中看到亂碼

- 使用[Locale Emulator](https://www.azofreeware.com/2014/05/locale-emulator-1201-applocale.html)用`中文(簡體)`開啟`x64dbg`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter9/01.png)

- 安裝`x64dbg_tol`外掛 [[參考](https://morosedog.gitlab.io/j.j.blogs/x64dbg-201907025-x64dbg-5/)]
>註：用於顯示`x64dbg`顯示中文的自動註解

<br>
## 目標程式
檔案下載：[TraceMe.exe](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/download/x64dbg/chapter9/TraceMe.7z)
解壓密碼：morosedog

<br>
## 任務目標

- 讓程式使其驗證通過。

<br>
## 分析程式

- 開啟`TraceMe.exe`
- 直接點選`Check`
- 跳出訊息視窗`你輸入字符要大於四個!`
- 輸入用戶名和序列號，點選`Check`
- 跳出訊息視窗`序列號錯誤, 再來一次!`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter9/02.gif)

- 檢驗顯示是使用`Microsoft Visual C++ v6.0`編寫。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter9/03.png)

<br>
## 搜尋思路

- `C++`撰寫的，使用函數搜尋 (輸入框函數：[GetDlgItemText](https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getdlgitemtexta))

<br>
## 修改思路

- 移除驗證的部分或是跳過驗證
- 驗證的邏輯分析，算出正確序號

<br>
## 實際分析

- 開啟`TraceMe.exe`
- 於反匯編視窗點選右鍵選擇`搜尋(S)`->`目前模組`->`跨模組呼叫(I)`
- 發現`GetDlgItemTextA`，對其設定中斷點
- `F9`執行程式
- 輸入用戶名`abcdefg`和序列號`12345678`，並按下`Check`
- 斷點在`004011A3 | 8B3D A0404000            | mov edi,dword ptr ds:[<&GetDlgItemTextA>]   |`
- `F8`一步一步過，並觀察反匯編視窗的註解和資訊視窗的內容

- 步過到下方指令時
```C
004011D0 | 74 76                    | je traceme.401248                           |
004011D2 | 83FB 05                  | cmp ebx,5                                   |
004011D5 | 7C 71                    | jl traceme.401248                           |
```
- 可以發現跳轉實現的話是到`00401248`
- 我們使用滑鼠指定到`00401248`並觀察資訊視窗，可以發現此判斷是判斷用戶名要大於四個以上
```C
eax=61 'a'
dword ptr [esp+1C]=[0018F6A8 "你输入字符要大于四个！"]=E4CAE3C4
.text:00401248 traceme.exe:$1248 #1248
```

- `F8`繼續步過，並觀察反匯編視窗的註解和資訊視窗的內容

- 步過到下方指令時
```C
004011E3 | 52                       | push edx                                    | edx:"abcdefg"
004011E4 | 50                       | push eax                                    | eax:"12345678"
004011E5 | E8 56010000              | call traceme.401340                         |
```
- 發現這邊將用戶名和序列號傳入`00401340`，這邊假設該函數是計算序列號的地方

- `F8`繼續步過，並觀察反匯編視窗的註解和資訊視窗的內容

- 步過到下方指令時
```C
004011F3 | 85C0                     | test eax,eax                                |
004011F5 | 74 37                    | je traceme.40122E                           |
```
- 可以發現跳轉實現的話是到`0040122E`
- 我們使用滑鼠指定到`0040122E`並觀察資訊視窗，可以發現這邊表示序列號是錯誤
```C
edx=02020D19
dword ptr [esp+34]=[0018F6C0 "序列号错误，再来一次！"]=D0C1F2D0
.text:0040122E traceme.exe:$122E #122E
```
- 這邊我假設不能讓他實現跳轉
- `je`跳轉實現為`ZF=1`，我們將其修改為`ZF=0`

- `F8`繼續步過，並觀察反匯編視窗的註解和資訊視窗的內容

- 步過到下方指令時
```C
004011F7 | 8D4C24 0C                | lea ecx,dword ptr ss:[esp+C]                |
004011FB | 51                       | push ecx                                    | ecx:"恭喜你！成功！"
```
- 可以發現準備了`恭喜你！成功！`的字樣

- `F8`繼續步過，並觀察反匯編視窗的註解和資訊視窗的內容

- 步過到下方指令時
```C
00401281 | FF15 C8404000            | call dword ptr ds:[<&DialogBoxParamA>]      |
```
- 發現會卡住，無法繼續步過
- 此時點擊開啟的程式，會發現跳出訊息視窗為`恭喜你！成功！`
>註：函數：[DialogBoxParam](https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-dialogboxparama)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter9/04.gif)

<br>
## 分析總結

1. `00401340`，假設該函數是計算序列號的地方。
2. `004011F5`，判斷跳轉序列號失敗的位址。
3. `004011F7`，序列號成功的位址。

<br>
## 修改思路
根據分析總結做對應

1. 步入`00401340`，分析並找出計算方式或是找出正確序號。
2. 將`004011F5`該段用`NOP`做填充。
3. 在失敗錯誤的跳轉指令，修改成直接跳轉到成功的位址。

<br>
## 實際修改
這邊使用修改思路的第2個方法做示範。

- 將所有中斷點移除
- 反匯編視窗中按下`Ctrl + G`輸入`0040122E`，會跳轉到`0040122E`位址
- 對`004011F5`按右鍵選擇`二進位`->`用 NOP 填充 (Ctrl + 9)`
- `F9`執行
- 輸入用戶名`abcdefg`和序列號`12345678`，並按下`Check`
- 彈出`恭喜你！成功！`視窗

- 點擊`修補程式` 或是快捷鍵`Ctrl + P`
- 點擊`修補檔案(P)`
- 另存檔名`Fix_TraceMe.exe`
- 恭喜補丁產生`Fix_TraceMe.exe`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter9/05.gif)

至於修改思路的第3個方法，這邊就不是示範了。

## 經驗分享
有時在動態分析程式，一直按`F7`或`F8`來觀察暫存器、堆疊和記憶體，不過按太快會直接跳過關鍵的區塊，可以使用`-`或 `+`的快捷鍵，察看前面幾個執行的過程。

<br />
>註：以上參考了
[x64dbg](https://x64dbg.com/#start)
[x64dbg’s documentation!](http://help.x64dbg.com/en/latest/)
[CSDN billvsme的专栏](https://blog.csdn.net/billvsme) 的 [OllyDbg 使用笔记 （二）](https://blog.csdn.net/billvsme/article/details/38308121)

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>