---
title: x64dbg - 第一章 | 下載及安裝
date: 2019-07-21 20:35:00
categories:
- x64dbg
tags: 
- x64dbg
---

x64dbg 是開放原始碼的反編譯 (Debugger) 工具，可以針對32位元或64位元的軟體來進行反編譯，並且 GUI 介面與功能都非常的好用。

<br>
## x64dbg 下載
這邊僅提供寫這篇文章的時候，所看到的最新版`2019-07-02_16-06`，建議就是找最新的版本進行下載

- [x64dbg 官方](https://x64dbg.com/#start)
- [x64dbg GitHub](https://github.com/x64dbg/x64dbg)
- [官方載點 snapshot_2019-07-02_16-06.zip](https://sourceforge.net/projects/x64dbg/files/snapshots/snapshot_2019-07-02_16-06.zip/download)
- [部落格載點 snapshot_2019-07-02_16-06.zip](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/download/x64dbg/chapter1/snapshot_2019-07-02_16-06.zip)

<br>
## x64dbg 解壓縮目錄結構

```tree
├── commithash.txt
├── pluginsdk
│   ├── DeviceNameResolver
│   │   ├── DeviceNameResolver.h
│   │   ├── DeviceNameResolver_x64.a
│   │   ├── DeviceNameResolver_x64.lib
│   │   ├── DeviceNameResolver_x86.a
│   │   └── DeviceNameResolver_x86.lib
│   ├── TitanEngine
│   │   ├── TitanEngine.h
│   │   ├── TitanEngine_x64.a
│   │   ├── TitanEngine_x64.lib
│   │   ├── TitanEngine_x86.a
│   │   └── TitanEngine_x86.lib
│   ├── XEDParse
│   │   ├── XEDParse.h
│   │   ├── XEDParse_x64.a
│   │   ├── XEDParse_x64.lib
│   │   ├── XEDParse_x86.a
│   │   └── XEDParse_x86.lib
│   ├── _dbgfunctions.h
│   ├── _plugin_types.h
│   ├── _plugins.h
│   ├── _scriptapi.h
│   ├── _scriptapi_argument.h
│   ├── _scriptapi_assembler.h
│   ├── _scriptapi_bookmark.h
│   ├── _scriptapi_comment.h
│   ├── _scriptapi_debug.h
│   ├── _scriptapi_flag.h
│   ├── _scriptapi_function.h
│   ├── _scriptapi_gui.h
│   ├── _scriptapi_label.h
│   ├── _scriptapi_memory.h
│   ├── _scriptapi_misc.h
│   ├── _scriptapi_module.h
│   ├── _scriptapi_pattern.h
│   ├── _scriptapi_register.h
│   ├── _scriptapi_stack.h
│   ├── _scriptapi_symbol.h
│   ├── bridgegraph.h
│   ├── bridgelist.h
│   ├── bridgemain.h
│   ├── dbghelp
│   │   ├── dbghelp.h
│   │   ├── dbghelp_x64.a
│   │   ├── dbghelp_x64.lib
│   │   ├── dbghelp_x86.a
│   │   └── dbghelp_x86.lib
│   ├── jansson
│   │   ├── jansson.h
│   │   ├── jansson_config.h
│   │   ├── jansson_x64.a
│   │   ├── jansson_x64.lib
│   │   ├── jansson_x64dbg.h
│   │   ├── jansson_x86.a
│   │   └── jansson_x86.lib
│   ├── lz4
│   │   ├── lz4.h
│   │   ├── lz4_x64.a
│   │   ├── lz4_x64.lib
│   │   ├── lz4_x86.a
│   │   ├── lz4_x86.lib
│   │   ├── lz4file.h
│   │   └── lz4hc.h
│   ├── x32bridge.lib
│   ├── x32dbg.lib
│   ├── x64bridge.lib
│   └── x64dbg.lib
└── release
    ├── errordb.txt
    ├── exceptiondb.txt
    ├── mnemdb.json
    ├── ntstatusdb.txt
    ├── translations
    │   ├── x64dbg_af.qm
    │   ├── x64dbg_ar.qm
    │   ├── x64dbg_bs.qm
    │   ├── x64dbg_ca.qm
    │   ├── x64dbg_ceb.qm
    │   ├── x64dbg_cs.qm
    │   ├── x64dbg_da.qm
    │   ├── x64dbg_de.qm
    │   ├── x64dbg_el.qm
    │   ├── x64dbg_es_ES.qm
    │   ├── x64dbg_fa.qm
    │   ├── x64dbg_fi.qm
    │   ├── x64dbg_fil.qm
    │   ├── x64dbg_fr.qm
    │   ├── x64dbg_he.qm
    │   ├── x64dbg_hu.qm
    │   ├── x64dbg_it.qm
    │   ├── x64dbg_ja.qm
    │   ├── x64dbg_ka.qm
    │   ├── x64dbg_ko.qm
    │   ├── x64dbg_nl.qm
    │   ├── x64dbg_no.qm
    │   ├── x64dbg_pl.qm
    │   ├── x64dbg_pt_BR.qm
    │   ├── x64dbg_pt_PT.qm
    │   ├── x64dbg_ro.qm
    │   ├── x64dbg_ru.qm
    │   ├── x64dbg_sr.qm
    │   ├── x64dbg_sv_SE.qm
    │   ├── x64dbg_th.qm
    │   ├── x64dbg_tr.qm
    │   ├── x64dbg_uk.qm
    │   ├── x64dbg_vi.qm
    │   ├── x64dbg_zh_CN.qm
    │   └── x64dbg_zh_TW.qm
    ├── winconstants.txt
    ├── x32
    │   ├── DeviceNameResolver.dll
    │   ├── Qt5Core.dll
    │   ├── Qt5Gui.dll
    │   ├── Qt5Network.dll
    │   ├── Qt5Widgets.dll
    │   ├── Scylla.dll
    │   ├── TitanEngine.dll
    │   ├── XEDParse.dll
    │   ├── asmjit.dll
    │   ├── dbghelp.dll
    │   ├── jansson.dll
    │   ├── ldconvert.dll
    │   ├── libeay32.dll
    │   ├── lz4.dll
    │   ├── msdia140.dll
    │   ├── msvcp120.dll
    │   ├── msvcr120.dll
    │   ├── platforms
    │   │   └── qwindows.dll
    │   ├── ssleay32.dll
    │   ├── symsrv.dll
    │   ├── x32_bridge.dll
    │   ├── x32_dbg.dll
    │   ├── x32bridge.dll
    │   ├── x32dbg.dll
    │   ├── x32dbg.exe
    │   ├── x32gui.dll
    │   └── yara.dll
    ├── x64
    │   ├── DeviceNameResolver.dll
    │   ├── Qt5Core.dll
    │   ├── Qt5Gui.dll
    │   ├── Qt5Network.dll
    │   ├── Qt5Widgets.dll
    │   ├── Scylla.dll
    │   ├── TitanEngine.dll
    │   ├── XEDParse.dll
    │   ├── asmjit.dll
    │   ├── dbghelp.dll
    │   ├── jansson.dll
    │   ├── ldconvert.dll
    │   ├── libeay32.dll
    │   ├── lz4.dll
    │   ├── msdia140.dll
    │   ├── msvcp120.dll
    │   ├── msvcr120.dll
    │   ├── platforms
    │   │   └── qwindows.dll
    │   ├── ssleay32.dll
    │   ├── symsrv.dll
    │   ├── x64_bridge.dll
    │   ├── x64_dbg.dll
    │   ├── x64bridge.dll
    │   ├── x64dbg.dll
    │   ├── x64dbg.exe
    │   ├── x64gui.dll
    │   └── yara.dll
    ├── x64dbg.chm
    └── x96dbg.exe
```

<br>
## x64dbg 使用

有幾種使用方式：

- 第一種：
  - 點擊`/snapshot_2019-07-02_16-06/release/x96dbg.exe`
  - 會開啟`Launcher`視窗，可以自己選擇`x32dbg`或是`x64dbg`版來進行開啟
  ![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter1/01.png)
  
- 第二種
  - 點擊`/snapshot_2019-07-02_16-06/release/x96dbg.exe`
  - 會開啟`Launcher`視窗
  - 點擊`Setup`
  - `Do you want to register a shell extension?` (你想註冊一個shell擴展嗎？)
  - 點擊是(`Y`)
  - `Do you want to create Desktop Shortcuts?` (是否要創建桌面快捷方式？)
  - 點擊是(`Y`)
  - `Do you want to register the database icon?` (您想註冊數據庫圖標嗎？)
  - 點擊是(`Y`)
  - `New configuration written!` (新配置寫入！)
  - 桌面會建立兩個快捷，分別是`x32dbg`和`x64dbg`
  ![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter1/02.png)
  ![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter1/04.png)
  ![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter1/05.png)
  ![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter1/06.png)

- 第三種
  - 直接點擊使用
  - 點擊`/snapshot_2019-07-02_16-06/release/x32/x32dbg.exe`
  - 點擊`/snapshot_2019-07-02_16-06/release/x64/x64dbg.exe`

<br>
## x64dbg 介面

- x32dbg
![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter1/07.png)

- x64dbg
![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter1/08.png)

<br />
>註：以上參考了
[x64dbg](https://x64dbg.com/#start)
[x64dbg’s documentation!](http://help.x64dbg.com/en/latest/)

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>