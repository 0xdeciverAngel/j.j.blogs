---
title: x64dbg - 第二十二章 | 反匯編練習(十一)
date: 2019-08-27 09:00:00
categories:
- x64dbg
tags: 
- x64dbg
---

## 目標程式
檔案下載：[Teksched.exe](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/download/x64dbg/chapter22/Teksched.7z)
解壓密碼：morosedog

<br>
## 程式簡介
`TechScheduler`讓您安排程序或系統選項，以在指定時間運行。你可以用它來調度程序或批處理文件，系統活動（如關閉或重新啟動），網絡登出，文件活動，按鍵，和屏幕捕獲。你也可以用它來編程在線活動，包括網絡維護和FTP下載、複製、移動或刪除指定的文件，並通過電子郵件通知。

<br>
## 前置作業

下載目標程式並解壓縮，`Teksched.exe`右鍵選擇`內容`->`相容性`->勾選`以相容模式執行這個程式`->選擇`Windows XP (Service Pack 3)`->勾選`以系統管理員的身份執行此程式`。

>註：因為軟體老舊，所以用Windows XP來執行，方可正常開啟。

<br>
## 任務目標

- 移除提示視窗(Nag)。
- 破解註冊。

<br>
## 分析程式

- 開啟`Teksched.exe`
- 彈出`Nag`視窗

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter22/01.png)

- 見標題`<unregistered>`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter22/02.png)

- 點擊`Help`->`About`
- 顯示`<Unregistered Versjon>`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter22/03.png)

- 點擊`Use Reg Key`
- 要求輸入`First Name`、`Last Name`、`Registration Key`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter22/04.png)

- 點擊`Register`
- 彈出`Enter a First Name value now.`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter22/05.png)

檢驗顯示是使用`Borland Delphi 6.0 - 7.0`編寫。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter22/06.png)

<br>
## 額外補充

- `Delphi`的特色，反匯編會看到調用很多的`call`
- `ret`上方搭配`push` 如同`jmp xxxxxx`

<br>
## 搜尋思路

- 使用搜尋字串找關鍵字。

<br>
## 修改思路

- 修改驗證的邏輯

<br>
## 實際分析

- 開啟`Teksched.exe`
- 於反匯編視窗點選右鍵選擇`搜尋(S)`->`目前模組`->`字串引用(S)`
- `搜尋`輸入`Regis`
```C
位址       反組譯                     字串                                                                                                                                                                                                                                                                                                                 
0048FB4C push teksched.48FE38    "RegisterAutomation"
004A55F9 mov eax,teksched.4A5978 "Error! You are attempting to register the wrong level of this program (standard,advanced,service)"
004A561D mov eax,teksched.4A59E4 "Error! You are attempting to register the wrong version of this program"
004A584B mov eax,teksched.4A5AA8 "Registration Key accepted!"
004A586C mov eax,teksched.4A5ACC "Registration Key Failed!"
004AC746 mov ecx,teksched.4AC85C "Error %d connecting to remote registry on %s"
004AC987 mov ecx,teksched.4AC9E0 "Error %d connecting to remote registry on %s"
004C48EE push teksched.4C4994    "UnRegisterTypeLib"
004CB926 mov ecx,teksched.4CBA10 "Unable to register icmp.dll"
004E3BC7 mov edx,teksched.4E46C8 "Registry Dump of this job"
004E4EA1 mov ecx,teksched.4E50D0 "bJobRegistry"
004E533E mov ecx,teksched.4E5590 "bJobRegistry"
0052A7DE mov edx,teksched.52A8A8 "RegisteredOwner"
0053AB7A mov edx,teksched.53ADE4 "<Unregistered Version>"
0053BAB9 mov edx,teksched.53BEE0 "TechScheduler <UNREGISTERED VERSION>"
0053BAD9 push teksched.53BF10    " This unregistered version is "
0053BB05 mov edx,teksched.53BF54 "TechScheduler Registered Version"
0053BC06 mov ecx,teksched.53BFC8 "iRegistryTarget"
0053C301 mov ecx,teksched.53C9AC "iRegistryTarget"
0053C327 mov ecx,teksched.53C9AC "iRegistryTarget"
005544BF mov edx,teksched.5545C4 "Registry Target: HKEY_CURRENT_USER"
005544D7 mov edx,teksched.5545F0 "Registry Target: HKEY_LOCAL_MACHINE"
0056A788 mov esi,teksched.56ACCC ")Registry AutoRefresh Procedure executed. "
0056B904 mov eax,teksched.56C898 "Error retrieving data from the registry"
0056C05A mov eax,teksched.56C898 "Error retrieving data from the registry"
00587490 mov ecx,teksched.58C998 "Debug:  Registry AutoRefresh procedure starting, kicking off registry re-read of job list"
005875FF push teksched.58CA80    " will be started from Registry AutoRefresh Procedure."
0058E374 mov eax,teksched.58F004 "TechScheduler Registration Period Expired\r\nPlease register this product with Dean Software Design at\r\nwww.winutils.com.\r\n\r\nClick YES to enter registration Info\r\n\r\nClick NO to load the Dean Software Web Site\r\n\r\nClick CANCEL to exit."
00590277 mov ecx,teksched.590B7C "Debug:  Main Startup v6.01 02/19/05 <unregistered>"
0059028C mov ecx,teksched.590BB8 "Debug:  Main Startup v6.01 02/19/05 <registered>"
00592332 mov eax,teksched.5926DC "You have been using the unregistered version of TechScheduler for more than 30 days now. We would appreciate you registering this copy. Would you like to view the registration help information?"
0059792C mov ecx,teksched.597CE0 "Debug. Removing all jobs from registry..."
00598EEF push teksched.598FEC    " <unregistered>"
0059932D mov edx,teksched.599440 "Debug:  Failure to delete job from registry: "
0059947C mov eax,teksched.5994D4 "The number of jobs in the registry exceeds the limit for this Shareware version. To add more jobs you need to upgrade to the registered version of TechScheduler. For more information visit the Dean Software Design site at www.winutils.com\r\n\r\nDo you want to browse to a secure web site to register now?"
0059EE7E mov eax,teksched.59F338 "/REGISTER="
0059F038 mov eax,teksched.59F350 "Command Line Registration Codes failed.."
005A20FF mov ecx,teksched.5A252C "iRegistryTarget"
```
- 點擊`Registration Key accepted!` (註冊密鑰被接受！)
- 到達`004A584B`位址，觀察指令
```C
004A5829   | 64:8910               | mov dword ptr fs:[eax],edx               |
004A582C   | 68 41584A00           | push teksched.4A5841                     |
004A5831   | 8B45 CC               | mov eax,dword ptr ss:[ebp-34]            |
004A5834   | E8 5BE8F5FF           | call teksched.404094                     |
004A5839   | C3                    | ret                                      |
004A583A   | E9 E9EFF5FF           | jmp teksched.404828                      |
004A583F   | EB F0                 | jmp teksched.4A5831                      |
004A5841   | C645 F3 01            | mov byte ptr ss:[ebp-D],1                |
004A5845   | 807D 08 00            | cmp byte ptr ss:[ebp+8],0                |
004A5849   | 75 0A                 | jne teksched.4A5855                      |
004A584B   | B8 A85A4A00           | mov eax,teksched.4A5AA8                  | 4A5AA8:"Registration Key accepted!"
004A5850   | E8 A339F9FF           | call teksched.4391F8                     |
004A5855   | A1 F4535A00           | mov eax,dword ptr ds:[5A53F4]            | 005A53F4:"莛Z"
004A585A   | C600 00               | mov byte ptr ds:[eax],0                  |
004A585D   | EB 17                 | jmp teksched.4A5876                      |
004A585F   | 807D 08 00            | cmp byte ptr ss:[ebp+8],0                |
004A5863   | 75 11                 | jne teksched.4A5876                      |
004A5865   | 6A 30                 | push 30                                  |
004A5867   | E8 C829F6FF           | call <JMP.&MessageBeep>                  |
004A586C   | B8 CC5A4A00           | mov eax,teksched.4A5ACC                  | 4A5ACC:"Registration Key Failed!"
004A5871   | E8 8239F9FF           | call teksched.4391F8                     |
```
- `004A5849   | 75 0A                 | jne teksched.4A5855                      |`會跳過註冊成功
- `004A5849`設定中斷點

- `F9`執行程式
- 點擊`Help`->`About`
- 點擊`Use Reg Key`
- 輸入`First Name`:`abc`、`Last Name`:`def`、`Registration Key`:`12345678`
- 點擊`Register`
- 彈出`'g{I' is not a valid integer value.`錯誤訊息

- 發現程式並沒有斷點在`004A5849`

----------------------------------------------------------------------------------------------------

- 向上觀察
```C
004A55ED   | 74 19                 | je teksched.4A5608                       |
004A55EF   | 807D 08 00            | cmp byte ptr ss:[ebp+8],0                |
004A55F3   | 0F85 7D020000         | jne teksched.4A5876                      |
004A55F9   | B8 78594A00           | mov eax,teksched.4A5978                  | 4A5978:"Error! You are attempting to register the wrong level of this program (standard,advanced,service)"
004A55FE   | E8 F53BF9FF           | call teksched.4391F8                     |
004A5603   | E9 6E020000           | jmp teksched.4A5876                      |
004A5608   | 66:BE 5802            | mov si,258                               |
004A560C   | 66:81FE BC02          | cmp si,2BC                               |
004A5611   | 76 19                 | jbe teksched.4A562C                      |
004A5613   | 807D 08 00            | cmp byte ptr ss:[ebp+8],0                |
004A5617   | 0F85 59020000         | jne teksched.4A5876                      |
004A561D   | B8 E4594A00           | mov eax,teksched.4A59E4                  | 4A59E4:"Error! You are attempting to register the wrong version of this program"
004A5622   | E8 D13BF9FF           | call teksched.4391F8                     |
004A5627   | E9 4A020000           | jmp teksched.4A5876                      |
004A562C   | A1 BC5C5A00           | mov eax,dword ptr ds:[5A5CBC]            |
```
- `004A55F9`、`004A561D`註解訊息如下
  - `"Error! You are attempting to register the wrong level of this program (standard,advanced,service)"`
  - `"Error! You are attempting to register the wrong version of this program"`
- `004A55ED   | 74 19                 | je teksched.4A5608                       |`判斷跳過`004A55F9`
- `004A5611   | 76 19                 | jbe teksched.4A562C                      |`判斷跳過`004A561D`

- 假設`Register`會執行
- `004A55ED`、`004A5611`設定中斷點

- 點擊`Register`
- 彈出`'g{I' is not a valid integer value.`錯誤訊息

- 發現程式並沒有斷點在`004A55ED`、`004A5611`
- 移除`004A55ED`、`004A5611`中斷點

----------------------------------------------------------------------------------------------------

- 繼續向上觀察
```C
004A52BE   | EB 19                 | jmp teksched.4A52D9                      |
004A52C0   | 807D 08 00            | cmp byte ptr ss:[ebp+8],0                |
004A52C4   | 0F85 AC050000         | jne teksched.4A5876                      |
004A52CA   | B8 08594A00           | mov eax,teksched.4A5908                  | 4A5908:"Enter a First Name value now.."
004A52CF   | E8 243FF9FF           | call teksched.4391F8                     |
004A52D4   | E9 9D050000           | jmp teksched.4A5876                      |
004A52D9   | 8B45 E8               | mov eax,dword ptr ss:[ebp-18]            | [ebp-18]:&"G"
004A52DC   | E8 6BFFF5FF           | call teksched.40524C                     |
004A52E1   | 85C0                  | test eax,eax                             |
004A52E3   | 0F8E C2000000         | jle teksched.4A53AB                      |
004A52E9   | 66:BF 0100            | mov di,1                                 |
004A52ED   | 8B1D 64575A00         | mov ebx,dword ptr ds:[5A5764]            | 005A5764:"Z"
004A52F3   | 43                    | inc ebx                                  |
004A52F4   | 8B45 E8               | mov eax,dword ptr ss:[ebp-18]            | [ebp-18]:&"G"
004A52F7   | 8A00                  | mov al,byte ptr ds:[eax]                 |
004A52F9   | E8 3EDBF5FF           | call teksched.402E3C                     |
004A52FE   | 50                    | push eax                                 |
004A52FF   | 8A03                  | mov al,byte ptr ds:[ebx]                 |
004A5301   | E8 36DBF5FF           | call teksched.402E3C                     |
004A5306   | 5A                    | pop edx                                  |
004A5307   | 3AD0                  | cmp dl,al                                |
004A5309   | 75 0B                 | jne teksched.4A5316                      |
004A530B   | 0FB7C7                | movzx eax,di                             |
004A530E   | 8B15 BC5C5A00         | mov edx,dword ptr ds:[5A5CBC]            |
004A5314   | 0102                  | add dword ptr ds:[edx],eax               |
004A5316   | 47                    | inc edi                                  |
004A5317   | 83C3 02               | add ebx,2                                |
004A531A   | 66:83FF 1B            | cmp di,1B                                |
004A531E   | 75 D4                 | jne teksched.4A52F4                      |
004A5320   | 66:BF 0100            | mov di,1                                 |
004A5324   | 8B1D 64575A00         | mov ebx,dword ptr ds:[5A5764]            | 005A5764:"Z"
004A532A   | 43                    | inc ebx                                  |
004A532B   | 8B45 E8               | mov eax,dword ptr ss:[ebp-18]            | [ebp-18]:&"G"
004A532E   | E8 19FFF5FF           | call teksched.40524C                     |
004A5333   | 8B55 E8               | mov edx,dword ptr ss:[ebp-18]            | [ebp-18]:&"G"
004A5336   | 8A4402 FF             | mov al,byte ptr ds:[edx+eax-1]           |
004A533A   | E8 FDDAF5FF           | call teksched.402E3C                     |
004A533F   | 50                    | push eax                                 |
004A5340   | 8A03                  | mov al,byte ptr ds:[ebx]                 |
004A5342   | E8 F5DAF5FF           | call teksched.402E3C                     |
004A5347   | 5A                    | pop edx                                  |
004A5348   | 3AD0                  | cmp dl,al                                |
004A534A   | 75 0B                 | jne teksched.4A5357                      |
004A534C   | 0FB7C7                | movzx eax,di                             |
004A534F   | 8B15 BC5C5A00         | mov edx,dword ptr ds:[5A5CBC]            |
004A5355   | 0102                  | add dword ptr ds:[edx],eax               |
004A5357   | 47                    | inc edi                                  |
004A5358   | 83C3 02               | add ebx,2                                |
004A535B   | 66:83FF 1B            | cmp di,1B                                |
004A535F   | 75 CA                 | jne teksched.4A532B                      |
004A5361   | 8B45 E8               | mov eax,dword ptr ss:[ebp-18]            | [ebp-18]:&"G"
004A5364   | 8A00                  | mov al,byte ptr ds:[eax]                 |
004A5366   | E8 D1DAF5FF           | call teksched.402E3C                     |
004A536B   | 8BD0                  | mov edx,eax                              |
004A536D   | 8D45 C0               | lea eax,dword ptr ss:[ebp-40]            |
004A5370   | E8 FFFDF5FF           | call teksched.405174                     |
004A5375   | 8B55 C0               | mov edx,dword ptr ss:[ebp-40]            |
004A5378   | 8D45 D8               | lea eax,dword ptr ss:[ebp-28]            |
004A537B   | E8 D4FEF5FF           | call teksched.405254                     |
004A5380   | 8B45 E8               | mov eax,dword ptr ss:[ebp-18]            | [ebp-18]:&"G"
004A5383   | E8 C4FEF5FF           | call teksched.40524C                     |
004A5388   | 8B55 E8               | mov edx,dword ptr ss:[ebp-18]            | [ebp-18]:&"G"
004A538B   | 8A4402 FF             | mov al,byte ptr ds:[edx+eax-1]           |
004A538F   | E8 A8DAF5FF           | call teksched.402E3C                     |
004A5394   | 8BD0                  | mov edx,eax                              |
004A5396   | 8D45 BC               | lea eax,dword ptr ss:[ebp-44]            |
004A5399   | E8 D6FDF5FF           | call teksched.405174                     |
004A539E   | 8B55 BC               | mov edx,dword ptr ss:[ebp-44]            |
004A53A1   | 8D45 D8               | lea eax,dword ptr ss:[ebp-28]            |
004A53A4   | E8 ABFEF5FF           | call teksched.405254                     |
004A53A9   | EB 19                 | jmp teksched.4A53C4                      |
004A53AB   | 807D 08 00            | cmp byte ptr ss:[ebp+8],0                |
004A53AF   | 0F85 C1040000         | jne teksched.4A5876                      |
004A53B5   | B8 30594A00           | mov eax,teksched.4A5930                  | 4A5930:"Enter a Last Name value now.."
004A53BA   | E8 393EF9FF           | call teksched.4391F8                     |
004A53BF   | E9 B2040000           | jmp teksched.4A5876                      |
004A53C4   | 8B45 F4               | mov eax,dword ptr ss:[ebp-C]             |
004A53C7   | E8 80FEF5FF           | call teksched.40524C                     |
004A53CC   | 48                    | dec eax                                  |
004A53CD   | 7D 19                 | jge teksched.4A53E8                      |
004A53CF   | 807D 08 00            | cmp byte ptr ss:[ebp+8],0                |
004A53D3   | 0F85 9D040000         | jne teksched.4A5876                      |
004A53D9   | B8 58594A00           | mov eax,teksched.4A5958                  | 4A5958:"Enter a Key value now.."
004A53DE   | E8 153EF9FF           | call teksched.4391F8                     |
004A53E3   | E9 8E040000           | jmp teksched.4A5876                      |
004A53E8   | 8D95 BCFEFFFF         | lea edx,dword ptr ss:[ebp-144]           |
```
- `004A52CA`、`004A53B5`、`004A53D9`註解訊息如下
  - `"Enter a First Name value now.."`
  - `"Enter a Last Name value now.."`
  - `"Enter a Key value now.."`
- `004A52BE   | EB 19                 | jmp teksched.4A52D9                      |`判斷跳過`004A52CA`
- `004A53A9   | EB 19                 | jmp teksched.4A53C4                      |`判斷跳過`004A53B5`
- `004A53CD   | 7D 19                 | jge teksched.4A53E8                      |`判斷跳過`004A53D9`

- 假設`Register`會執行
- `004A52BE`、`004A53A9`、`004A53CD`設定中斷點

- 點擊`Register`
- 斷點在`004A52BE   | EB 19                 | jmp teksched.4A52D9                      |`
- `F8`一步一步過，並持續觀察

- 步過到下方指令時
```C
004A5461   | E8 0A4CF6FF           | call teksched.40A070                     |
```
- 彈出`'g{I' is not a valid integer value.`錯誤訊息
- 對`004A5461`按右鍵選擇`二進位`->`用 NOP 填充 (Ctrl + 9)`
```C
// 修改前
004A5461   | E8 0A4CF6FF           | call teksched.40A070                     |
// 修改後
004A5461   | 90                    | nop                                      |
004A5462   | 90                    | nop                                      |
004A5463   | 90                    | nop                                      |
004A5464   | 90                    | nop                                      |
004A5465   | 90                    | nop                                      |
```
- 點擊`Register`
- `F9`執行程式，持續跳過中斷點
- 彈出視窗`Error! You are attempting to register the wrong level of this program (standard,advanced,service)`

- 在此可以確認錯誤視窗的問題被修正了

----------------------------------------------------------------------------------------------------

- 移除所有中斷點
- `004A52BE`設定中斷點 ("Enter a First Name value now.."上方判斷的地方)

- 輸入`First Name`:`abc`、`Last Name`:`def`、`Registration Key`:`12345678`
- 點擊`Register`

- `F8`一步一步過，並持續觀察

- 步過到下方指令時
```C
004A55ED   | 74 19                 | je teksched.4A5608                       |
004A55EF   | 807D 08 00            | cmp byte ptr ss:[ebp+8],0                |
004A55F3   | 0F85 7D020000         | jne teksched.4A5876                      |
004A55F9   | B8 78594A00           | mov eax,teksched.4A5978                  | 4A5978:"Error! You are attempting to register the wrong level of this program (standard,advanced,service)"
004A55FE   | E8 F53BF9FF           | call teksched.4391F8                     |
004A5603   | E9 6E020000           | jmp teksched.4A5876                      |
004A5608   | 66:BE 5802            | mov si,258                               |
```
- `004A55ED   | 74 19                 | je teksched.4A5608                       |`跳轉未實現
- `je`跳轉實現為`ZF=1`，我們將其修改為`ZF=1`

- `F8`繼續步過，持續觀察

- 步過到下方指令時
```C
004A567E   | 0F85 DB010000         | jne teksched.4A585F                      |
```
- `004A567E   | 0F85 DB010000         | jne teksched.4A585F                      |`跳轉已實現
  - 會跳轉到`Registration Key Failed!`
- `jne`跳轉實現為`ZF=0`，我們將其修改為`ZF=1`

- `F8`繼續步過，持續觀察

- 步過到下方指令時
```C
004A5849   | 75 0A                 | jne teksched.4A5855                      |
```
- `004A5849   | 75 0A                 | jne teksched.4A5855                      |`跳轉未實現
- 會執行到`004A584B   | B8 A85A4A00           | mov eax,teksched.4A5AA8                  | 4A5AA8:"Registration Key accepted!"`
- `F8`繼續步過，持續觀察

- 步過到下方指令時
```C
004A5850   | E8 A339F9FF           | call teksched.4391F8                     |
```
- 彈出`Registration Key accepted!`視窗
- 點擊`OK`
- 回到`x64dbg`
- `F9`執行程式

- 回到主程式可見`Licensed Version. Do Not Copy!`

- 恭喜程式真正被破解

<br>
## 分析總結

- `004A5461`跳出錯誤訊息的位址

- `004A55ED`，未實現跳轉則彈出失敗訊息。
- `004A567E`，實現跳轉則彈出失敗訊息。

<br>
## 修改思路
根據分析總結

- `004A5461`使用 NOP 填充
- `004A55ED`使用`jmp`跳轉
- `004A567E`使用 NOP 填充

<br>
## 實際修改

- 開啟`Teksched.exe`
- `004A5461`按右鍵選擇`二進位`->`用 NOP 填充 (Ctrl + 9)`
- 修改後如下
```C
// 修改前
004A5461   | E8 0A4CF6FF           | call teksched.40A070                     |
// 修改後
004A5461   | 90                    | nop                                      |
004A5462   | 90                    | nop                                      |
004A5463   | 90                    | nop                                      |
004A5464   | 90                    | nop                                      |
004A5465   | 90                    | nop                                      |
```

- `004A55ED   | 74 19                 | je teksched.4A5608                       |`按下空白鍵
- 將指令修改為`jmp 0x004A5608`，按下確定
- 修改後如下
```C
// 修改前
004A55ED   | 74 19                 | je teksched.4A5608                       |
// 修改後
004A55ED   | 74 19                 | jmp teksched.4A5608                       |
```

- `004A567E`按右鍵選擇`二進位`->`用 NOP 填充 (Ctrl + 9)`
- 修改後如下
```C
// 修改前
004A567E   | 0F85 DB010000         | jne teksched.4A585F                      |
// 修改後
004A567E   | 90                    | nop                                      |
004A567F   | 90                    | nop                                      |
004A5680   | 90                    | nop                                      |
004A5681   | 90                    | nop                                      |
004A5682   | 90                    | nop                                      |
004A5683   | 90                    | nop                                      |
```

- 點擊`修補程式` 或是快捷鍵`Ctrl + P`
- 點擊`修補檔案(P)`
- 另存檔名`Teksched.crack.exe`
- 恭喜補丁產生`Teksched.crack.exe`

>註：開啟新的補丁.exe不要忘記設定相容性和管理員執行。

<br />
>註：以上參考了
[x64dbg](https://x64dbg.com/#start)
[x64dbg’s documentation!](http://help.x64dbg.com/en/latest/)
[CSDN billvsme的专栏](https://blog.csdn.net/billvsme) 的 [OllyDbg 使用笔记 （十一）](https://blog.csdn.net/billvsme/article/details/38870809)

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>