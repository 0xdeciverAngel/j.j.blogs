---
title: x64dbg - 第二十三章 | 反匯編練習(十二)
date: 2019-08-28 09:00:00
categories:
- x64dbg
tags: 
- x64dbg
---

## 目標程式
檔案下載：[XoftSpy413_96.exe](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/download/x64dbg/chapter23/XoftSpy413_96.7z)
解壓密碼：morosedog

<br>
## 程式簡介
保護你的隱私和身份！間諜軟件可以在您不知情的情況下安裝在計算機上。很難找到並且難以刪除，間諜軟件可用於竊取您的機密信息，從而導致身份盜用和信用卡欺詐。任何上網或與其他人共享文件的人都有風險。xoftspy檢測並刪除嘗試在您的PC上安裝的所有間諜軟件。主要功能包括：完整的PC掃描，包括運行流程，註冊表項，文件和文件夾，檢測和刪除間諜軟件，廣告軟件，彈出式發生器，鍵盤記錄程序，特洛伊木馬，瀏覽器劫持程序和惡意軟件，自動定義和功能更新，全面的客戶技術支持，防止身份盜用和信用卡欺詐。

<br>
## 前置作業

- 下載目標程式並解壓縮
- 執行`XoftSpy413_96.exe`，一路`Next`安裝
- `Finish`開啟主程式
- 在`Loading definitions from file.`會卡比較久
- `XoftSpy.exe`，右鍵選擇`內容`->`相容性`->勾選`以相容模式執行這個程式`->選擇`Windows XP (Service Pack 3)`->勾選`以系統管理員的身份執行此程式`。

>註：因為軟體老舊，所以用Windows XP來執行，方可正常開啟。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter23/01.gif)

<br>
## 任務目標

- 破解註冊。

<br>
## 分析程式

- 開啟`XoftSpy.exe`
- 在`Loading definitions from file.`會卡比較久
- 點擊`About`
- 點擊`Registration Code`
- 點擊`OK`
- 彈出`Please fill all the details`視窗
- 輸入`Enter Username`、`Enter Registration Code`
- 點擊`OK`
- 彈出`Invalid code.`視窗

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter23/02.gif)

檢驗顯示是使用`Microsoft Visual C++ 6.0`編寫。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/x64dbg/chapter23/03.png)

<br>
## 額外補充

- [逆向常用API函數](https://blog.csdn.net/LeoCfyb/article/details/8578202)

<br>
## 搜尋思路

- `C++`撰寫的，使用函數搜尋 (輸入框函數：[GetWindowTextA](https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getwindowtexta))
- 使用搜尋字串找關鍵字。

<br>
## 修改思路

- 修改驗證的邏輯

<br>
## 實際分析

- 開啟`XoftSpy.exe`
- 於反匯編視窗點選右鍵選擇`搜尋(S)`->`目前模組`->`跨模組呼叫(I)`
- 發現`GetWindowTextA`，對其設定中斷點
- `F9`執行程式
- 在`Loading definitions from file.`會卡比較久

- 斷點在`00461E51   | FF15 F0034700         | call dword ptr ds:[<&GetWindowTextA>]    |`
- 主程式還沒開啟，標示該斷點並非為輸入註冊碼的地方所使用
- `00461E51`移除中斷點
- `F9`繼續執行

- 斷點在`0045B75D   | FF15 F0034700         | call dword ptr ds:[<&GetWindowTextA>]    |`
- 主程式還沒開啟，標示該斷點並非為輸入註冊碼的地方所使用
- `0045B75D`移除中斷點
- `F9`繼續執行

- 點擊`About`
- 點擊`Registration Code`
- 輸入`Enter Username`、`Enter Registration Code`
- 點擊`OK`

- 斷點在`004625AB   | FF15 F0034700         | call dword ptr ds:[<&GetWindowTextA>]    |`
- `F8`一步一步過，並持續觀察
- 有三個`Text`所以會在`004625AB`斷點三次

- 步過到下方指令時
```C
0041749C   | 75 45                 | jne xoftspy.4174E3                       |
0041749E   | 6A 00                 | push 0                                   |
004174A0   | 68 04544800           | push xoftspy.485404                      | 485404:"XoftSpy"
004174A5   | 68 C4684800           | push xoftspy.4868C4                      | 4868C4:"Invalid code."
004174AA   | 8BCE                  | mov ecx,esi                              | esi:"x G"==&"苾A"
004174AC   | E8 664F0400           | call xoftspy.45C417                      |
```
- `0041749C   | 75 45                 | jne xoftspy.4174E3                       |`跳轉未實現
- 未跳過`Invalid code.`訊息
- `jne`跳轉實現為`ZF=0`，我們將其修改為`ZF=0`

- 跳轉到`004174E3`
- `F8`繼續步過，持續觀察

- 步過到下方指令時
```C
00401105 | E8 5D020000              | call <JMP.&MessageBoxA>                     |
```
- 彈出視窗`Congratulations! successfully registered`
- 點擊`確定`
- `F9`執行程式
- 回到程式`About`
- `Enter Registration Code`按鈕尚未消失

- 發現程式並沒有真正破解

- 在此可以確認單純的顯示註冊成功內容

----------------------------------------------------------------------------------------------------

- 反匯編視窗中按下`Ctrl + G`輸入`0041749C`，會跳轉到`0041749C`位址
- 向上觀察
```C
00417495   | E8 F6010000           | call xoftspy.417690                      |
0041749A   | 84C0                  | test al,al                               |
0041749C   | 75 45                 | jne xoftspy.4174E3                       |
```
- `0041749A`決定`0041749C`跳轉是否實現
- `00417495`的`call xoftspy.417690`影響`al`
- 移除所有中斷點
- `00417495`設定中斷點

- 點擊`Registration Code`
- 輸入`Enter Username`、`Enter Registration Code`
- 點擊`OK`

- 斷點在`00417495   | E8 F6010000           | call xoftspy.417690                      |`
- `F7`步入

- 觀察程式
```C
00417690   | 6A FF                 | push FFFFFFFF                            |
00417692   | 68 A89C4600           | push xoftspy.469CA8                      |
00417697   | 64:A1 00000000        | mov eax,dword ptr fs:[0]                 |
0041769D   | 50                    | push eax                                 |
0041769E   | 64:8925 00000000      | mov dword ptr fs:[0],esp                 |
004176A5   | 83EC 1C               | sub esp,1C                               |
004176A8   | 53                    | push ebx                                 | ebx:&"5678"
004176A9   | 56                    | push esi                                 | esi:"x G"==&"苾A"
004176AA   | 57                    | push edi                                 | edi:""==&"1234-5678"
004176AB   | 8B4424 38             | mov eax,dword ptr ss:[esp+38]            |
004176AF   | 68 48FA4800           | push xoftspy.48FA48                      |
004176B4   | 50                    | push eax                                 |
004176B5   | C74424 38 01000000    | mov dword ptr ss:[esp+38],1              |
004176BD   | E8 8A160300           | call xoftspy.448D4C                      |
004176C2   | 83C4 08               | add esp,8                                |
004176C5   | 85C0                  | test eax,eax                             |
004176C7   | 0F84 28020000         | je xoftspy.4178F5                        |
004176CD   | 8B4C24 3C             | mov ecx,dword ptr ss:[esp+3C]            | [esp+3C]:&"8EG"
004176D1   | 68 48FA4800           | push xoftspy.48FA48                      |
004176D6   | 51                    | push ecx                                 |
004176D7   | E8 70160300           | call xoftspy.448D4C                      |
004176DC   | 83C4 08               | add esp,8                                |
004176DF   | 85C0                  | test eax,eax                             |
004176E1   | 0F84 0E020000         | je xoftspy.4178F5                        |
004176E7   | 8B5424 3C             | mov edx,dword ptr ss:[esp+3C]            | [esp+3C]:&"8EG"5678"
004176EB   | 837A F8 15            | cmp dword ptr ds:[edx-8],15              | edx-8:L"\t"
004176EF   | 0F8C 00020000         | jl xoftspy.4178F5                        |
004176F5   | 8D4424 1C             | lea eax,dword ptr ss:[esp+1C]            | [esp+1C]:"1234-5678"
004176F9   | 6A 0B                 | push B                                   |
004176FB   | 50                    | push eax                                 |
004176FC   | 8D4C24 44             | lea ecx,dword ptr ss:[esp+44]            |
00417700   | E8 93060400           | call xoftspy.457D98                      |
00417705   | 8B4C24 1C             | mov ecx,dword ptr ss:[esp+1C]            | [esp+1C]:"1234-5678"
00417709   | 33F6                  | xor esi,esi                              | esi:"x G"==&"苾A"
0041770B   | 33D2                  | xor edx,edx                              | edx:"1234-5678"
0041770D   | C64424 30 02          | mov byte ptr ss:[esp+30],2               |
00417712   | 897424 20             | mov dword ptr ss:[esp+20],esi            | [esp+20]:"MOROSE"
00417716   | 33C0                  | xor eax,eax                              |
00417718   | 0FBE3C08              | movsx edi,byte ptr ds:[eax+ecx]          | edi:""==&"1234-5678"
0041771C   | 03D7                  | add edx,edi                              | edx:"1234-5678", edi:""==&"1234-5678"
0041771E   | 40                    | inc eax                                  |
0041771F   | 83F8 0A               | cmp eax,A                                | A:'\n'
00417722   | 7E F4                 | jle xoftspy.417718                       |
00417724   | 895424 24             | mov dword ptr ss:[esp+24],edx            | [esp+24]:&"MOROSE"
00417728   | 8B5424 38             | mov edx,dword ptr ss:[esp+38]            |
0041772C   | 33C0                  | xor eax,eax                              |
0041772E   | 8B4A F8               | mov ecx,dword ptr ds:[edx-8]             | edx-8:L"\t"
00417731   | 85C9                  | test ecx,ecx                             |
00417733   | 7E 0F                 | jle xoftspy.417744                       |
00417735   | 0FBE3C10              | movsx edi,byte ptr ds:[eax+edx]          | edi:""==&"1234-5678", eax+edx*1:"1234-5678"34-5678"
00417739   | 03F7                  | add esi,edi                              | esi:"x G"==&"苾A", edi:""==&"1234-5678"
0041773B   | 40                    | inc eax                                  |
0041773C   | 3BC1                  | cmp eax,ecx                              |
0041773E   | 7C F5                 | jl xoftspy.417735                        |
00417740   | 897424 20             | mov dword ptr ss:[esp+20],esi            | [esp+20]:"MOROSE"
00417744   | 8B4C24 3C             | mov ecx,dword ptr ss:[esp+3C]            | [esp+3C]:&"8EG"
00417748   | 55                    | push ebp                                 |
00417749   | 8D5424 10             | lea edx,dword ptr ss:[esp+10]            | [esp+10]:&"8EG"5678"
0041774D   | 8B41 F8               | mov eax,dword ptr ds:[ecx-8]             | [ecx-8]:&"MOROSE"
00417750   | 8D4C24 40             | lea ecx,dword ptr ss:[esp+40]            |
00417754   | 83C0 F4               | add eax,FFFFFFF4                         |
00417757   | 50                    | push eax                                 |
00417758   | 52                    | push edx                                 | edx:"1234-5678"
00417759   | E8 BE050400           | call xoftspy.457D1C                      |
0041775E   | 8D4424 1C             | lea eax,dword ptr ss:[esp+1C]            | [esp+1C]:"1234-5678"
00417762   | 6A 03                 | push 3                                   |
00417764   | 50                    | push eax                                 |
00417765   | 8D4C24 18             | lea ecx,dword ptr ss:[esp+18]            |
00417769   | C64424 3C 03          | mov byte ptr ss:[esp+3C],3               |
0041776E   | E8 25060400           | call xoftspy.457D98                      |
00417773   | 8B4C24 1C             | mov ecx,dword ptr ss:[esp+1C]            | [esp+1C]:"1234-5678"
00417777   | C64424 34 04          | mov byte ptr ss:[esp+34],4               |
0041777C   | 51                    | push ecx                                 |
0041777D   | E8 5D210300           | call xoftspy.4498DF                      |
00417782   | 99                    | cdq                                      |
00417783   | 83C4 04               | add esp,4                                |
00417786   | 2BC2                  | sub eax,edx                              | edx:"1234-5678"
00417788   | 8D5424 18             | lea edx,dword ptr ss:[esp+18]            |
0041778C   | 8BF0                  | mov esi,eax                              | esi:"x G"==&"苾A"
0041778E   | 6A 03                 | push 3                                   |
00417790   | 6A 03                 | push 3                                   |
00417792   | 52                    | push edx                                 | edx:"1234-5678"
00417793   | 8D4C24 1C             | lea ecx,dword ptr ss:[esp+1C]            | [esp+1C]:"1234-5678"
00417797   | D1FE                  | sar esi,1                                | esi:"x G"==&"苾A"
00417799   | E8 E8040400           | call xoftspy.457C86                      |
0041779E   | 8B4424 18             | mov eax,dword ptr ss:[esp+18]            |
004177A2   | C64424 34 05          | mov byte ptr ss:[esp+34],5               |
004177A7   | 50                    | push eax                                 |
004177A8   | E8 32210300           | call xoftspy.4498DF                      |
004177AD   | 99                    | cdq                                      |
004177AE   | 83C4 04               | add esp,4                                |
004177B1   | 83E2 03               | and edx,3                                | edx:"1234-5678"
004177B4   | 03C2                  | add eax,edx                              | edx:"1234-5678"
004177B6   | 8D4C24 14             | lea ecx,dword ptr ss:[esp+14]            |
004177BA   | 6A 03                 | push 3                                   |
004177BC   | 8BF8                  | mov edi,eax                              | edi:""==&"1234-5678"
004177BE   | 51                    | push ecx                                 |
004177BF   | 8D4C24 18             | lea ecx,dword ptr ss:[esp+18]            |
004177C3   | C1FF 02               | sar edi,2                                | edi:""==&"1234-5678"
004177C6   | E8 51050400           | call xoftspy.457D1C                      |
004177CB   | 8B5424 14             | mov edx,dword ptr ss:[esp+14]            |
004177CF   | 52                    | push edx                                 | edx:"1234-5678"
004177D0   | E8 0A210300           | call xoftspy.4498DF                      |
004177D5   | 8BC8                  | mov ecx,eax                              |
004177D7   | B8 56555555           | mov eax,55555556                         |
004177DC   | F7E9                  | imul ecx                                 |
004177DE   | 8BC2                  | mov eax,edx                              | edx:"1234-5678"
004177E0   | 83C4 04               | add esp,4                                |
004177E3   | C1E8 1F               | shr eax,1F                               |
004177E6   | 03D0                  | add edx,eax                              | edx:"1234-5678"
004177E8   | 8B4424 3C             | mov eax,dword ptr ss:[esp+3C]            | [esp+3C]:&"8EG"
004177EC   | 8BDA                  | mov ebx,edx                              | ebx:&"5678", edx:"1234-5678"
004177EE   | 0FBE08                | movsx ecx,byte ptr ds:[eax]              |
004177F1   | 8B68 F8               | mov ebp,dword ptr ds:[eax-8]             |
004177F4   | 3BF1                  | cmp esi,ecx                              | esi:"x G"==&"苾A"
004177F6   | 0FBE50 01             | movsx edx,byte ptr ds:[eax+1]            | edx:"1234-5678"
004177FA   | 0FBE4428 FF           | movsx eax,byte ptr ds:[eax+ebp-1]        |
004177FF   | 5D                    | pop ebp                                  |
00417800   | 0F85 A9000000         | jne xoftspy.4178AF                       |
00417806   | 3BFA                  | cmp edi,edx                              | edi:""==&"1234-5678", edx:"1234-5678"34-5678"
00417808   | 0F85 A1000000         | jne xoftspy.4178AF                       |
0041780E   | 3BD8                  | cmp ebx,eax                              | ebx:&"5678"
00417810   | 0F85 99000000         | jne xoftspy.4178AF                       |
00417816   | 8B4424 20             | mov eax,dword ptr ss:[esp+20]            | [esp+20]:"MOROSE"
0041781A   | B9 0A000000           | mov ecx,A                                | A:'\n'
0041781F   | 99                    | cdq                                      |
00417820   | F7F9                  | idiv ecx                                 |
00417822   | 8B4424 24             | mov eax,dword ptr ss:[esp+24]            | [esp+24]:&"MOROSE"
00417826   | BE 0A000000           | mov esi,A                                | esi:"x G"==&"苾A", A:'\n'
0041782B   | 8BCA                  | mov ecx,edx                              | edx:"1234-5678"
0041782D   | 99                    | cdq                                      |
0041782E   | F7FE                  | idiv esi                                 | esi:"x G"==&"苾A"
00417830   | 3BD1                  | cmp edx,ecx                              | edx:"1234-5678"
00417832   | 75 7B                 | jne xoftspy.4178AF                       |
00417834   | 8D4C24 10             | lea ecx,dword ptr ss:[esp+10]            | [esp+10]:&"8EG"
00417838   | C64424 30 05          | mov byte ptr ss:[esp+30],5               |
0041783D   | E8 5E690400           | call xoftspy.45E1A0                      |
00417842   | 8D4C24 14             | lea ecx,dword ptr ss:[esp+14]            |
00417846   | C64424 30 04          | mov byte ptr ss:[esp+30],4               |
0041784B   | E8 50690400           | call xoftspy.45E1A0                      |
00417850   | 8D4C24 18             | lea ecx,dword ptr ss:[esp+18]            |
00417854   | C64424 30 03          | mov byte ptr ss:[esp+30],3               |
00417859   | E8 42690400           | call xoftspy.45E1A0                      |
0041785E   | 8D4C24 0C             | lea ecx,dword ptr ss:[esp+C]             | [esp+C]:"x G"==&"苾A"
00417862   | C64424 30 02          | mov byte ptr ss:[esp+30],2               |
00417867   | E8 34690400           | call xoftspy.45E1A0                      |
0041786C   | 8D4C24 1C             | lea ecx,dword ptr ss:[esp+1C]            | [esp+1C]:"1234-5678"
00417870   | C64424 30 01          | mov byte ptr ss:[esp+30],1               |
00417875   | E8 26690400           | call xoftspy.45E1A0                      |
0041787A   | 8D4C24 38             | lea ecx,dword ptr ss:[esp+38]            |
0041787E   | C64424 30 00          | mov byte ptr ss:[esp+30],0               |
00417883   | E8 18690400           | call xoftspy.45E1A0                      |
00417888   | 8D4C24 3C             | lea ecx,dword ptr ss:[esp+3C]            | [esp+3C]:&"8EG"
0041788C   | C74424 30 FFFFFFFF    | mov dword ptr ss:[esp+30],FFFFFFFF       |
00417894   | E8 07690400           | call xoftspy.45E1A0                      |
00417899   | 5F                    | pop edi                                  | edi:""==&"1234-5678"
0041789A   | 5E                    | pop esi                                  | esi:"x G"==&"苾A"
0041789B   | B0 01                 | mov al,1                                 |
0041789D   | 5B                    | pop ebx                                  | ebx:&"5678"
0041789E   | 8B4C24 1C             | mov ecx,dword ptr ss:[esp+1C]            | [esp+1C]:"1234-5678"
004178A2   | 64:890D 00000000      | mov dword ptr fs:[0],ecx                 |
004178A9   | 83C4 28               | add esp,28                               |
004178AC   | C2 0800               | ret 8                                    |
004178AF   | 8D4C24 10             | lea ecx,dword ptr ss:[esp+10]            | [esp+10]:&"8EG"
004178B3   | C64424 30 05          | mov byte ptr ss:[esp+30],5               |
004178B8   | E8 E3680400           | call xoftspy.45E1A0                      |
004178BD   | 8D4C24 14             | lea ecx,dword ptr ss:[esp+14]            |
004178C1   | C64424 30 04          | mov byte ptr ss:[esp+30],4               |
004178C6   | E8 D5680400           | call xoftspy.45E1A0                      |
004178CB   | 8D4C24 18             | lea ecx,dword ptr ss:[esp+18]            |
004178CF   | C64424 30 03          | mov byte ptr ss:[esp+30],3               |
004178D4   | E8 C7680400           | call xoftspy.45E1A0                      |
004178D9   | 8D4C24 0C             | lea ecx,dword ptr ss:[esp+C]             | [esp+C]:"x G"==&"苾A"
004178DD   | C64424 30 02          | mov byte ptr ss:[esp+30],2               |
004178E2   | E8 B9680400           | call xoftspy.45E1A0                      |
004178E7   | 8D4C24 1C             | lea ecx,dword ptr ss:[esp+1C]            | [esp+1C]:"1234-5678"
004178EB   | C64424 30 01          | mov byte ptr ss:[esp+30],1               |
004178F0   | E8 AB680400           | call xoftspy.45E1A0                      |
004178F5   | 8D4C24 38             | lea ecx,dword ptr ss:[esp+38]            |
004178F9   | C64424 30 00          | mov byte ptr ss:[esp+30],0               |
004178FE   | E8 9D680400           | call xoftspy.45E1A0                      |
00417903   | 8D4C24 3C             | lea ecx,dword ptr ss:[esp+3C]            | [esp+3C]:&"8EG"
00417907   | C74424 30 FFFFFFFF    | mov dword ptr ss:[esp+30],FFFFFFFF       |
0041790F   | E8 8C680400           | call xoftspy.45E1A0                      |
00417914   | 8B4C24 28             | mov ecx,dword ptr ss:[esp+28]            | [esp+28]:&"1234-5678"
00417918   | 5F                    | pop edi                                  | edi:""==&"1234-5678"
00417919   | 5E                    | pop esi                                  | esi:"x G"==&"苾A"
0041791A   | 32C0                  | xor al,al                                |
0041791C   | 5B                    | pop ebx                                  | ebx:&"5678"
0041791D   | 64:890D 00000000      | mov dword ptr fs:[0],ecx                 |
00417924   | 83C4 28               | add esp,28                               |
00417927   | C2 0800               | ret 8                                    |
```
- 中間過程應該是在驗證註冊的內容
- `0041791A   | 32C0                  | xor al,al                                |`最後傳入`eax`的值

- 在此可以假設`0041791A`回傳的`eax`是否註冊成功

- `0041791A   | 32C0                  | xor al,al                                |`按下空白鍵
- 將指令修改為`mov al, 0x1`，按下確定
- 修改後如下
```C
// 修改前
0041791A   | 32C0                  | xor al,al                                |
// 修改後
0041791A   | 32C0                  | mov al,1                                |
```
- `F9`執行程式
- 彈出視窗`Congratulations! successfully registered`
- 點擊`確定`
- `F9`執行程式
- 回到程式`About`
- `Enter Registration Code`按鈕尚未消失

- 發現程式並沒有真正破解

- 可以假設有其他驗證位置沒有找到(重複驗證，心跳驗證)

----------------------------------------------------------------------------------------------------

- `About`裡面有`This XoftSpy license has not been registered`提示 (此XoftSpy許可證尚未註冊)
- 移除所有中斷點
- 於反匯編視窗點選右鍵選擇`搜尋(S)`->`目前模組`->`字串引用(S)`
```C
位址       反組譯                                         字串                                                                                                                                                                                                                                                                                                                                                                                    
00401070 mov eax,xoftspy.4706B0                      "PQH"==&"CAboutPage"
004010C5 mov dword ptr ds:[esi+C4],xoftspy.475150    &"落NG"
004011AA mov dword ptr ds:[ecx],xoftspy.475160       "~[F"==&"貉NG"
0040120A mov dword ptr ds:[ecx],xoftspy.475160       "~[F"==&"貉NG"
004012F2 mov dword ptr ds:[ecx],xoftspy.475160       "~[F"==&"貉NG"
004013D0 mov eax,xoftspy.4706C8                      &"8EG"
00401424 mov dword ptr ss:[esp+14],xoftspy.475150    &"落NG"
0040149B push xoftspy.4851C4                         "This license of XoftSpy has been registered"
004014AD push xoftspy.485194                         "This XoftSpy license has not been registered"
00401555 mov dword ptr ss:[esp+14],xoftspy.475160    "~[F"==&"貉NG"
00401630 push xoftspy.48524C                         "Arial"
00401662 push xoftspy.48524C                         "Arial"
00401682 push xoftspy.48524C                         "Arial"
004016F9 push xoftspy.48521C                         "This version of XoftSpy is Registered to %s."
0040174E push xoftspy.4851F0                         "This version of XoftSpy is Un-Registered."
```
- 點擊`This license of XoftSpy has been registered` (XoftSpy的許可證已經註冊)
- 到達`0040149B`位址，觀察指令
```C
00401492   | E8 093C0300           | call xoftspy.4350A0                      |
00401497   | 84C0                  | test al,al                               |
00401499   | 74 12                 | je xoftspy.4014AD                        |
0040149B   | 68 C4514800           | push xoftspy.4851C4                      | 4851C4:"This license of XoftSpy has been registered"
004014A0   | 8D4C24 08             | lea ecx,dword ptr ss:[esp+8]             |
004014A4   | E8 34CE0500           | call xoftspy.45E2DD                      |
004014A9   | 6A 00                 | push 0                                   |
004014AB   | EB 10                 | jmp xoftspy.4014BD                       |
004014AD   | 68 94514800           | push xoftspy.485194                      | 485194:"This XoftSpy license has not been registered"
004014B2   | 8D4C24 08             | lea ecx,dword ptr ss:[esp+8]             |
004014B6   | E8 22CE0500           | call xoftspy.45E2DD                      |
```
- `00401499`設定中斷點
- 點擊`Registration Code`
- 輸入`Enter Username`、`Enter Registration Code`
- 點擊`OK`

- 斷點在`00401499   | 74 12                 | je xoftspy.4014AD                        |`跳轉已實現
- `je`跳轉實現為`ZF=1`，我們將其修改為`ZF=0`
- `F9`執行程式

- 發現會一直斷點在`00401499   | 74 12                 | je xoftspy.4014AD                        |`跳轉已實現
- 假設`00401499`為重複驗證
- 對`00401499`按右鍵選擇`二進位`->`用 NOP 填充 (Ctrl + 9)`
```C
// 修改前
00401499   | 74 12                 | je xoftspy.4014AD                        |
// 修改後
00401499   | 90                    | nop                                      |
0040149A   | 90                    | nop                                      |
```
- 移除所有中斷點
- `F9`執行程式
- 回到程式`About`
- `Enter Registration Code`按鈕消失了

- 恭喜程式真正被破解

<br>
## 分析總結

- `00401499`為重複驗證位址

<br>
## 修改思路
根據分析總結

- `00401499`使用 NOP 填充

<br>
## 實際修改

- 開啟`XoftSpy.exe`
- 反匯編視窗中按下`Ctrl + G`輸入`00401499`，會跳轉到`00401499`位址
- 對`00401499`按右鍵選擇`二進位`->`用 NOP 填充 (Ctrl + 9)`
```C
// 修改前
00401499   | 74 12                 | je xoftspy.4014AD                        |
// 修改後
00401499   | 90                    | nop                                      |
0040149A   | 90                    | nop                                      |
```

- 點擊`修補程式` 或是快捷鍵`Ctrl + P`
- 點擊`修補檔案(P)`
- 另存檔名`XoftSpy.crack.exe`
- 恭喜補丁產生`XoftSpy.crack.exe`

>註：開啟新的補丁.exe不要忘記設定相容性和管理員執行。

<br />
>註：以上參考了
[x64dbg](https://x64dbg.com/#start)
[x64dbg’s documentation!](http://help.x64dbg.com/en/latest/)
[CSDN billvsme的专栏](https://blog.csdn.net/billvsme) 的 [OllyDbg 使用笔记 （十二）](https://blog.csdn.net/billvsme/article/details/38986377)
[CSDN 残烛之泪的专栏](https://blog.csdn.net/LeoCfyb) 的 [逆向常用API函數](https://blog.csdn.net/LeoCfyb/article/details/8578202)

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>