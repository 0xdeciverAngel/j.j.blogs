---
title: OllyDBG - 第八章 | 反匯編練習 (二) 上
date: 2019-07-12 16:30:00
categories:
- OllyDBG
tags: 
- OllyDBG
---

這篇反匯編開始前，要告訴大家，反匯編這是需要很大的耐心和毅力，經過不斷的追蹤分析才可以看到一些些蛛絲馬跡；當然經驗和能力也會影響的，但這是需要時間來做累積的。

首先在我沒有事先參考任何文章來做的反匯編，所以建議大家不要急著看人家的破解過程，試著自己找看看，加油。

## 檔案下載

目的程式：
[TraceMe.7z](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/download/ollydbg/chapter8/TraceMe.7z)

解壓縮密碼：
```
morosedog
```

---

由於目的程式是簡體中文的，所以在反匯編過程有很多亂碼，我又找不到插件可以解決這問題，於是這邊提供一個簡體的OllyDBG

[吾爱破解专用版Ollydbg.rar](https://down.52pojie.cn/Tools/Debuggers/%E5%90%BE%E7%88%B1%E7%A0%B4%E8%A7%A3%E4%B8%93%E7%94%A8%E7%89%88Ollydbg.rar)

預設解壓縮密碼：
```
www.52pojie.cn
```

>註：轉至[吾爱破解](https://www.52pojie.cn/)

---

開啟簡體軟體，這邊推薦使用[Locale Emulator](https://www.azofreeware.com/2014/05/locale-emulator-1201-applocale.html)

>註：轉至[阿榮福利味](https://www.azofreeware.com/)

## 使用PEid檢驗
檢驗顯示是使用`Microsoft Visual C++ v6.0`編寫。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/ollydbg/chapter8/01.png)

## 開啟`TraceMe.exe`使用並分析

- 開啟 `TraceMe.exe`
- 直接點選`Check`
- 跳出訊息視窗`你輸入字符要大於四個!`
- 輸入`用戶名`和`序列號`，點選`Check`
- 跳出訊息視窗`序列號錯誤, 再來一次!`

分析：基本上看到text輸入框，一定會用到GetDlgItemText是C++中的函數，調用這個函數以獲得與對話框中的控件相關的標題或文本

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/ollydbg/chapter8/02.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/ollydbg/chapter8/03.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/ollydbg/chapter8/04.png)

## 目標說明
讓程式使其驗證通過。

## 思路 (本人弱弱的，勿噴)

首先視窗中有一些函數，所以應該可以利用[OllyDBG - 第三章 | 函數參考](https://morosedog.gitlab.io/j.j.blogs/ollydbg-20190707-OllyDBG-3/)的方式來找到斷點，接續進行分析找到驗證序號的地方;

至於找什麼函數呢？因為有輸入框，所以程式要確認序號是否正確的時候，需要先取得輸入框的內容，這時候就會用到函數[GetDlgItemText](https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getdlgitemtexta)

1. 移除驗證的部分或是跳過驗證
2. 找出驗證的邏輯加以計算，算出正確序號

## 使用OllyDBG分析 (方法一)

- 啟動`OllyDBG`
- 按下快捷鍵`F3`
- 選擇`TraceMe.exe`
- 在入口點地址後面加上註解 (良好的註解有助於分析)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/ollydbg/chapter8/05.png)

- 按下`Ctrl + N`
- 瀏覽一下函數，在上方可以判定是一個訊息窗口，所以基本上找類似`GetDlgItemText`的函數
  - 

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/ollydbg/chapter8/06.png)

- 在該函數按下右鍵
- 選擇`在每個參考上設定斷點`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/ollydbg/chapter8/07.png)

- 按下`F9`執行程式
- 輸入用戶名`abcdefg`、序列號`12345678`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/ollydbg/chapter8/08.png)

- 按下`Check`按鈕
- 看到斷點斷在`004011A3   .  8B3D A0404000 mov edi,dword ptr ds:[<&USER32.GetDlgIte>;  user32.GetDlgItemTextA`
- `F8`一步一步往下看 (多觀察資訊視窗)
- 會發現執行到`004011F5   . /74 37         je short TraceMe.0040122E`，然後跳轉到`0040122E   > \8D5424 34     lea edx,dword ptr ss:[esp+0x34]`
- 再繼續往下就會看到OD自動註解的內容`序列號錯誤, 再來一次!`
- 繼續下去到`00401281   .  FF15 C8404000 call dword ptr ds:[<&USER32.DialogBoxPar>; \DialogBoxParamA`停住
- 跳出訊息視窗內容為`序列號錯誤, 再來一次!`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/ollydbg/chapter8/09.gif)

### 分析

第一種分析：

- 我們剛剛發現執行到`004011F5   . /74 37         je short TraceMe.0040122E`就跳到準備錯誤訊息的那邊
  >註：`je` 若等於則跳越 `x = y`	`ZF=1`
- 所以關鍵就是這行的上一行指令`004011F3   .  85C0          test eax,eax`
  >註：`test`和`cmp`都是比較用的，他們有什麼差別，這邊暫時不提
- 所以我預期這邊讓他不做跳轉就可以`Check`成功

第二種分析：

- 在追蹤的過程中會發，如果有仔細觀察資訊視窗可以發現下面三行`push`所帶的值很眼熟
- 然後就是`004011E5   .  E8 56010000   call TraceMe.00401340`
- 所以我猜測這個`call`是計算序列號的地方
  >註：函数的返回值都是儲存在`eax`
- 所以下面的`004011F3   .  85C0          test eax,eax`是比較的地方

```C
004011DB   .  53            push ebx ; ebx=00000007
004011E3   .  52            push edx ; edx=0018F6D8, (ASCII "abcdefg")
004011E4   .  50            push eax ; eax=0018F728, (ASCII "12345678")

004011E5   .  E8 56010000   call TraceMe.00401340 ; 猜測是計算序列號的地方
```

>註：`test`比對的結果放在`ZF：零值旗標`

---

- 按下`Alt + B`
- 先刪除或是停用之前的斷點`Breakpoints, 条目 0 地址=004011A3 模块=TraceMe 激活=始终 反汇编=mov edi,dword ptr ds:[<&USER32.GetDlgItemTextA>]`
- 然後在`004011F3   .  85C0          test eax,eax`的上一行設定新的斷點

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/ollydbg/chapter8/10.png)

- `Ctrl + F2`重新開始
- `F9`開始
- 輸入用戶名`abcdefg`、序列號`12345678`
- 按下`Check`按鈕
- 觀察暫存器的視窗Z (也就是ZF) 的參數
- 在執行過`004011F3   .  85C0          test eax,eax`後，`ZF=1`
- 此時我們修改其值為`0`
- 繼續`F8`
- 會看到沒有跳轉走，而是繼續往下，並可以看到OD自動註解`恭喜您!成功!`
- 繼續`F8`
- 繼續下去到`00401281   .  FF15 C8404000 call dword ptr ds:[<&USER32.DialogBoxPar>; \DialogBoxParamA`停住
- 跳出訊息視窗內容為`恭喜您!成功!`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/ollydbg/chapter8/11.gif)

- 到這邊很明顯我們只要把`004011F3   .  85C0          test eax,eax`判斷修改或是移除即可
- `Ctrl + F2`重新開始
- `F9`開始
- 輸入用戶名`abcdefg`、序列號`12345678`
- 按下`Check`按鈕
- 在`004011F3   .  85C0          test eax,eax`按下右鍵選擇`編譯`
- 輸入`NOP`，按下`組譯`
- 會看到指令被換成了`NOP`
- 繼續`F8`
- 恭喜你!成功!

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/ollydbg/chapter8/12.gif)

>註：以上將指令重新編譯成`NOP`，這個俗稱暴破 (暴力破解)；而不是叫做破解。
真正的破解應該是找到序號的計算方法，然後寫出註冊機來產生序號，這才稱得上是破解。

- 選擇兩個修改後`NOP`的指令
- 按下右鍵選擇`複製到可執行檔案` -> `選擇`
- 在視窗按下右鍵選擇`備份` -> `儲存資料到檔案`
- 另存在別的位置，檔名改為`TraceMe(Cracked).exe`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/ollydbg/chapter8/13.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/ollydbg/chapter8/14.png)

- 執行`TraceMe(Cracked).exe`
- 輸入用戶名`abcdefg`、序列號`12345678` (程式已經被爆破，輸入什麼都不重要了)
- 目標達成

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/ollydbg/chapter8/15.png)


## 總結
這個`TraceMe`，我至少重新分析了不下30次，才可以略有小成的分析出點東西，然後在不斷地看組合語言的過程，看不懂該行指令在做什麼，或是猜測不到不確的時候，就是去看[x86組合語言 - 第三章 | 基本指令集](http://localhost:4000/j.j.blogs/x86-20190703-x86-3/)，或是上網直接查詢，慢慢了解，才終於發現看懂了！！！真的有很大的成就感，後面將會繼續針對這個`TraceMe`繼續分析。

<br />
>註：我這邊提供一些可以參考的文章 (以上的練習我沒事先參考)
[CSDN billvsme的专栏](https://blog.csdn.net/billvsme) 的 [OllyDbg 使用笔记 （二）](https://blog.csdn.net/billvsme/article/details/38308121)

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>