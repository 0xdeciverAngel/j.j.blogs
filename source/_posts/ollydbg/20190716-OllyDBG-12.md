---
title: OllyDBG - 第十二章 | 反匯編練習 (三) 下
date: 2019-07-16 23:25:00
categories:
- OllyDBG
tags: 
- OllyDBG
---

不斷反匯編不斷的對不了解不懂的組合語言，努力看懂⋯⋯。

## 檔案下載

相關的目的程式和使用的工具，請直接至[OllyDBG - 第十一章 | 反匯編練習 (三) 上](https://morosedog.gitlab.io/j.j.blogs/ollydbg-20190715-OllyDBG-11/)，這邊下載

## 分析

延續上一篇的分析過程

- 首先必須要有`Keyfile.dat`的檔案
- 然後下方會去讀取這個檔案[ReadFile函數](https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-readfile)
- 而`test eax,eax`應該是再次確認有讀取到檔，沒有的則跳失敗
- `xor ebx,ebx` 和 `xor esi,esi`為清零

- 在看`cmp dword ptr ds:[402173],10`這句指令要搭配下方的`jl short 004010F7`一起觀看
- `jl`表示若低於則跳越

- 所以`cmp dword ptr ds:[402173],10`就是說`[402173]`裡面存放的值，如果小於十六進制的10 (也就是16)，則跳到`004010F7`失敗得地方
- 那`402173`是哪裡來的？
- 可以看到上方`ReadFile`前面`push`進去的參數有一個`push 00402173`
- 而`push 00402173`這是第四個參數，`lpNumberOfBytesRead`表示讀取了多少個`Byte`

>**這邊的分析結果就是`Keyfile.dat`裡面的內容，必須要大於16個字**

```C
0040109A   > \6A 00         push 0                                   ; /pOverlapped = NULL
0040109C   .  68 73214000   push 00402173                            ; |pBytesRead = reverseM.00402173
004010A1   .  6A 46         push 46                                  ; |BytesToRead = 46 (70.)
004010A3   .  68 1A214000   push 0040211A                            ; |Buffer = reverseM.0040211A
004010A8   .  50            push eax                                 ; |hFile
004010A9   .  E8 2F020000   call <jmp.&KERNEL32.ReadFile>            ; \ReadFile
004010AE   .  85C0          test eax,eax
004010B0   .  75 02         jnz short 004010B4
004010B2   .  EB 43         jmp short 004010F7
004010B4   >  33DB          xor ebx,ebx
004010B6   .  33F6          xor esi,esi
004010B8   .  833D 73214000>cmp dword ptr ds:[402173],10
004010BF   .  7C 36         jl short 004010F7
```

- 將`KeyFile.dat`編輯輸入`AAAAAAAAAAAAAAAAAAAA` (故意填寫20個)
- 成功繼續向下執行了
- 當前 ebx = 00000000 
  0040211A = 0040211A (ASCII "AAAAAAAAAAAAAAAAAAAA")
- `mov al,byte ptr ds:[ebx+40211A]`這句表示每次拿`0040211A`裡面的一個字元，然後根據`ebx`增加偏移量，也就是每次迴圈一次就往後拿一個字元
- `cmp al,0` 和 `je short 004010D3` 表示拿當前取得字元，去和十六進制的0比較 (也就是空字元（Null）)，`je`若等於則跳越到`004010D3`
  - 而`004010D3`判斷`esi`若低於8 則跳到失敗

>**分析到這邊可以判斷出，每讀出一個字元，如果讀到為`空字元（Null）`，則會去確認目前該esi的長度是否大於8，如果低於就失敗**

- 繼續分析`cmp al,47`，比較讀取的字元是否為十六進制的47 (也就是`G`)，如果不為`G`就跳過`inc esi`，這句是esi+。

>**分析到這邊結合剛剛上面得分析，esi 需要 大於8，然後這邊如果不為`G`，就不+1，所以`Keyfile.dat`裡面的字元，必須要是`G`，而且必須至少有8個`G`**

```C
004010C1   > /8A83 1A214000 mov al,byte ptr ds:[ebx+40211A]
004010C7   . |3C 00         cmp al,0
004010C9   . |74 08         je short 004010D3
004010CB   . |3C 47         cmp al,47
004010CD   . |75 01         jnz short 004010D0
004010CF   . |46            inc esi
004010D0   > |43            inc ebx
004010D1   .^\EB EE         jmp short 004010C1
004010D3   >  83FE 08       cmp esi,8
004010D6   .  7C 1F         jl short 004010F7
004010D8   .  E9 28010000   jmp 00401205
```

綜合以上的序號條件

- 必須要有`KeyFile.dat`檔
- 內容的長度必須要等於或是大於16個字元
- 內容裡面`0`的部分不能出現在第八位以前
- 內容必須要包含`G`有八個以上

- 將`KeyFile.dat`編輯輸入`GGGGGGGGAAAAAAAA` (成功)
- 將`KeyFile.dat`編輯輸入`GAGAGAGAGAGAGAGA` (成功)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/ollydbg/chapter12/01.gif)

- 將`KeyFile.dat`編輯輸入`GGGGGGGAAAAAAAAA` (失敗，因為只有7個G)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/ollydbg/chapter12/02.gif)

## 總結
因為必須要盡量一行一行去分析，並且去說明。希望透過這樣的分析，可以更加的懂反匯編的過程和語法。

已經做了幾個反匯編練習，對於組合語言的認識越是進步，對於跳越使令或許還不熟，那沒關係，有表可以參考，參考久了就是你的。

## 額外補充
這邊要補充的是
- 通常`cmp xxx, xxx`，需要一起看下一行的跳越指令(`je`,`jne`,`jl`...等等)，因為這樣一起看，才可以知道這個`cmp`是希望比對這兩個值，是什麼條件。
- `mov al,byte ptr ds:[ebx+40211A]`再來補充，當用`[]`包起來的，就表示是要取這個地址裡面的`值`，像這句就是，將`ebx`地址加上`40211A`，然後取值。

假設ebx = 00000001
00000001 + 0x40211A = 0x40211B

假設0x40211B 裡面的值為 47 47 47 47。

那就是說al 的值將會是 00000047。 (al = eax (8-bit))

<br />
>註：我這邊提供一些可以參考的文章 (以上的練習我沒事先參考)
[CSDN billvsme的专栏](https://blog.csdn.net/billvsme) 的 [OllyDbg 使用笔记 （三）](https://blog.csdn.net/billvsme/article/details/38335013)

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>