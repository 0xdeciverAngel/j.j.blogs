---
title: Cheat Engine - 第二章 | CE Tutorial Step 1
date: 2019-06-22 09:00:00
categories:
- Cheat Engine
tags: 
- Cheat Engine
---

從這章開始正式進入CE的使用教學，透過官方提供的教程，一步一步進行學習。

## 開啟 Tutorial

## 使用程序選擇器 選擇 Tutorial 程序
點擊 -> Help -> Cheat Engine Tutorial 或是 Cheat Engine Tutorial (64-bit)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter2/01.png)

```
Welcome to the Cheat Engine Tutorial (v3.3)

This tutorial will teach you the basics of cheating in video games. It will also show you foundational aspects of using Cheat Engine (or CE for short). Follow the steps below to get started.

1: Open Cheat Engine if it currently isn't running.
2: Click on the "Open Process" icon (it's the top-left icon with the computer on it, below "File".).
3: With the Process List window now open, look for this tutorial's process in the list. It will look something like "00001F98-Tutorial-x86_64.exe" or "0000047C-Tutorial-i386.exe". (The first 8 numbers/letters will probably be different.)
4: Once you've found the process, click on it to select it, then click the "Open" button. (Don't worry about all the other buttons right now. You can learn about them later if you're interested.)

Congratulations! If you did everything correctly, the process window should be gone with Cheat Engine now attached to the tutorial (you will see the process name towards the top-center of CE).

Click the "Next" button below to continue, or fill in the password and click the "OK" button to proceed to that step.)

If you're having problems, simply head over to forum.cheatengine.org, then click on "Tutorials" to view beginner-friendly guides!
```

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter2/02.png)

這邊是在教您第一步，如何使用程序選擇器，選擇這個 Tutorial 程序，如果您選擇完畢後，就可以下一步了，正式進入使用 Cheat Engine 的使用。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter2/03.gif)

## 總結
這邊篇就是這麼簡單！正所謂第一步很重要，就是先教您如何使用程序選擇器，去針對需要的程序做選擇。後面將會在進行對程序內做掃描。

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>