---
title: Cheat Engine - 第三章 | CE Tutorial Step 2
date: 2019-06-23 09:00:00
categories:
- Cheat Engine
tags: 
- Cheat Engine
---

這邊開始正式進入CE的使用教學，後面幾章將不會在文章前面在說些什麼了⋯都讓我們直接開始吧！

## 教程分析

第二關：精確值搜索（密碼是 090453）。

這關重現了經典的打怪現場，你有 100 滴血，點擊 Hit me 你會被怪打一下，然後你的血量就少了，最後就會血量為零死去。
不過 Tutorial 會給你一條新的生命的。實際遊戲中通常不會有這種好事，開始一次新的搜索吧）

你需要做的是：把它改成 1000。

>密碼是用來Tutorial Step 1 右下角輸入框輸入後，可以直接跳到這關卡。

## 解題

### 第一步

- 首先看到頁面上的 `Health:100`
- 接著我們在需要搜尋的值那邊輸入`100`
- 點擊`首次搜尋`
- 會將搜尋結果顯示在`搜尋的結果地址表`的框內

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter3/01.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter3/02.png)

>註：`搜尋的結果地址表`裡面有黑色字，也有綠色字，這個在後面會在特別提到。

### 第二步

- 點擊 `Hit me`，讓其扣血
- 根據 `Health:97` 數值，在需要搜尋的值重新輸入 (這邊範例為97)
- 點擊`再次搜尋`
- 此時會發現`搜尋的結果地址表`的結果變少了 (有時候會精準到剩下一個，或是剩下多個)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter3/03.png)

### 第三步

- 發現`搜尋的結果地址表`的結果，只剩下一個
- 對該地址點擊左鍵兩下，添加到下方的`CHEAT TABLE`裡面

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter3/04.png)

### 第四步

- 根據題目需求，我們將`Value`的值進行修改為1000
- 點擊`Value`左鍵兩下
- 將數值`97` 修改為 `1000`，並點擊`OK`
- 將會發現 `Next` 的按鈕變成可以按下，並前往下一關了。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter3/05.png)


## 完整動態解題

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter3/06.gif)

## 總結
通常，在遊戲中面板顯示的數值應該和內存裡的數值一樣。第一次，顯示 100 ，搜索 100 。然後打了自己一下，地址列表中有些數值還是 100 ，有些則變成了 97 ，那麼哪個地址才是真正的存儲地址呢？如果這個地址是我們想要的，無論打自己幾下，他就會一直與我們的血量有關。這就是個不斷篩選的過程，把始終跟目標有關的數據篩選出來就可以了。

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>