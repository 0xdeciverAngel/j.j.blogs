---
title: Cheat Engine - 第九章 | CE Tutorial Step 8
date: 2019-06-29 09:00:00
categories:
- Cheat Engine
tags: 
- Cheat Engine
---

## 教程分析

第八關：多級指針（密碼是 525927）。

和第六關差不多，不過這次不是一級指針，而是4級指針，就是指向目標數值 的指針 的指針 的指針 的指針

你需要做的是：點擊 Change pointer之後的 3 秒內 將新的數值鎖定在 5000。

## 解題

### 第一步
(基本上參照[第七章 | CE Tutorial Step 6](https://morosedog.gitlab.io/j.j.blogs/cheatengine-20190627-cheatEngine7/)的步驟，找到指針並搜尋)

- 首先看到頁面上的 `334`
- 接著我們在需要搜尋的值那邊輸入`334`
- 點擊`首次搜尋`
- 會將搜尋結果顯示在`搜尋的結果地址表`的框內

- 點擊 `Change value`，讓數字變動
- 根據 `1862` 數值，在需要搜尋的值重新輸入 (這邊範例為1862)
- 點擊`再次搜尋`
- 此時會發現`搜尋的結果地址表`的結果變少了 (有時候會精準到剩下一個，或是剩下多個)

- 將該地址加入到`Cheat Table`
- 針對該地址點擊右鍵
- 選擇 `Find out what writes to this address`
- 會跳出`This will attach the debugger on Cheat Engine to the current process. Continue?`，點擊`Yes`
- 點擊 `Change value`，讓數字變動
- 此時會發現`The following opcodes write to xxxxxxxx`視窗內計數器視窗的多了一條指令
- 左鍵雙擊該指令兩下 會看到`0042626C - mov [esi+18],eax` **(表示指針偏移量為+18)**
- 在`Extra info`視窗中，找尋`copy memory The value of the pointer needed to find address is probably xxxxxxxx` 區塊，在該區塊上點右鍵
- 點擊`copy info to clipboard`

- 點擊`New Scan`
- `搜尋類型`選擇`Exact Value`，`數值類型`選擇`4 Bytes`，勾选 `Hex`。
- 將剛剛複製的地址貼上`01830440`，進行搜尋 (指針位置可能不一樣)
- 這邊結果有點不一樣，搜尋後可以看到地址為黑色 (搜索到的地址不是綠色的基址了，這就需要我們再繼續搜索上一級指針。)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter9/01.gif)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter9/02.png)

>註：`0042626C - mov [esi+18],eax`，這個的偏移量，在找尋上一級指針的時候，都要做紀錄下來。

### 第二步

- 針對該地址點擊右鍵
- 選擇 `Find out what accesses to this address`
- 點擊 `Change value`，讓數字變動
- 此時會發現`The following opcodes write to xxxxxxxx`視窗內計數器視窗的多了兩條指令
```
0042622A - 83 3E 00 - cmp dword ptr [esi],00
0042622F - 8B 36  - mov esi,[esi]
```
- cmp 指令跟指針沒什麼關係，對我們有用的是第二條指令 `mov esi,[esi]`。
- 左鍵雙擊`0042622F - 8B 36  - mov esi,[esi]`
- 在`Extra info`視窗中，找尋`copy memory The value of the pointer needed to find address is probably 01830440` 會發現 `01830440` 與 第一次搜尋的時候是一樣的

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter9/03.png)

#### 分析
這裡提示的地址和上一次提示的地址是一樣的，這是為什麼呢？

CE 默認使用硬件斷點的方式，斷點只能停在指令執行之後，而這條指令正好是把esi 原來指向的地址中的值再賦值給esi，所以執行之後esi 的值已經是被覆蓋掉的值了，而我們想知道的恰恰是執行這條指令之前的esi 值，那麼怎麼辦呢。

我們的 Find out what accesses this address 是感什麼的？查找訪問這個地址的指令，然後我們又發現了 mov esi,[esi] 這條指令訪問了這個地址，那麼 [esi] 原來是啥？原來的 esi 不就是這個我們監視的地址嘛。

所以直接搜索這個地址即可 `01810720` 

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter9/04.png)

### 第三步

- 針對該地址點擊右鍵
- 選擇 `Find out what accesses to this address`
- 點擊 `Change value`，讓數字變動
- 此時會發現`The following opcodes write to xxxxxxxx`視窗內計數器視窗的多了兩條指令
```
004261EB - 83 7E 14 00 - cmp dword ptr [esi+14],00
004261F5 - 8B 76 14  - mov esi,[esi+14]
```
- cmp 指令跟指針沒什麼關係，對我們有用的是第二條指令 `mov esi,[esi+14]`。
- 左鍵雙擊`004261F5 - 8B 76 14  - mov esi,[esi+14]`  **(表示指針偏移量為+14)**
- 在`Extra info`視窗中，找尋`copy memory The value of the pointer needed to find address is probably 0183DBA0`
- 點擊`New Scan`
- `搜尋類型`選擇`Exact Value`，`數值類型`選擇`4 Bytes`，勾选 `Hex`。
- 將剛剛複製的地址貼上`0183DBA0`，進行搜尋 (指針位置可能不一樣)
- 搜索到的地址不是綠色的基址了，繼續搜索上一級指針

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter9/05.png)

### 第四步

- 針對該地址點擊右鍵
- 選擇 `Find out what accesses to this address`
- 點擊 `Change value`，讓數字變動
- 此時會發現`The following opcodes write to xxxxxxxx`視窗內計數器視窗的多了兩條指令
```
004261AA - 83 7E 0C 00 - cmp dword ptr [esi+0C],00
004261B4 - 8B 76 0C  - mov esi,[esi+0C]
```
- cmp 指令跟指針沒什麼關係，對我們有用的是第二條指令 `mov esi,[esi+0C]`。
- 左鍵雙擊`004261B4 - 8B 76 0C  - mov esi,[esi+0C]`  **(表示指針偏移量為+0C)**
- 在`Extra info`視窗中，找尋`copy memory The value of the pointer needed to find address is probably 0184DFC0`
- 點擊`New Scan`
- `搜尋類型`選擇`Exact Value`，`數值類型`選擇`4 Bytes`，勾选 `Hex`。
- 將剛剛複製的地址貼上`0184DFC0`，進行搜尋 (指針位置可能不一樣)
- 終於找到綠色基址了

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter9/06.png)

### 第五步

- 將基地加入`Cheat Table`裡面
- 左鍵兩下`Address`，複製`Change address`視窗內的`Address` (類似：`"Tutorial-i386.exe"+201660`)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter9/07.png)

### 第六步

我們可以藉用彙編語言的指針表示方法來表示一下我們需要的內存地址

[[[["Tutorial-i386.exe"+201660]+0C]+14]+00]+18

把基址（一級指針） "Tutorial-i386.exe"+201660 的值取出來，加上一級偏移0C，當做地址，這是二級指針的地址，再把二級指針的值取出來，加上14，這是三級指針的地址，依次類推。

- 點擊`Add Address Manuallly`，手動添加地址
- 勾選`Pointer`
- 點擊`Add Offset`增加偏移量，變為四級偏移的指針
- 輸入剛剛複製的`Address`
- 點擊`OK`
- 可以看到`Cheat Table`，多了一個指針的地址 (類似：`P->01830458`)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter9/08.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter9/09.png)

### 第六步

- 點擊剛剛手動添加的指針地址`Value`
- 修改成`5000`
- 勾選該指針前方的`Check box` (用於鎖定該值)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter9/10.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter9/11.png)

### 第七步

- 點擊`Cheange poniter`
- 可以看到`Next`已經為可以點擊進入下一關

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter9/12.png)

## 完整動態解題

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter9/13.gif)

## 注意
- 可以通過代碼注入的方式通過此關。
- 上述過程中，除了對目標數值的地址使用`Find writes`，其餘都使用`Find accesses`。其實，對目標數值的地址使用`Find accesses`也可以。

## Pointer scan
CE 內置了搜索指針的工具

- 首先看到頁面上的 `3226`
- 接著我們在需要搜尋的值那邊輸入`3226`
- 點擊`首次搜尋`
- 會將搜尋結果顯示在`搜尋的結果地址表`的框內

- 點擊 `Change value`，讓數字變動
- 根據 `3188` 數值，在需要搜尋的值重新輸入 (這邊範例為3188)
- 點擊`再次搜尋`
- 此時會發現`搜尋的結果地址表`的結果變少了 (有時候會精準到剩下一個，或是剩下多個)

- 將該地址加入到`Cheat Table`
- 針對該地址點擊右鍵
- 選擇 `Pointer scan for this address`
- 彈出的搜索設置提示框，通常使用默認配置即可。
- 由於 Pointer 列表過於龐大，所以需要有一個保存的地方，這個自己隨意保存。
- 點擊確定開始搜索，可以發現有非常多的路徑可以指向我們要找的地址。
- 點擊 `Change pointer` (不是`Change Value`)

- 看到頁面上的 `1114`
- 接著我們在需要搜尋的值那邊輸入`1114`
- 點擊`首次搜尋`
- 會將搜尋結果顯示在`搜尋的結果地址表`的框內

- 點擊 `Change value`，讓數字變動
- 根據 `910` 數值，在需要搜尋的值重新輸入 (這邊範例為910)
- 點擊`再次搜尋`
- 此時會發現`搜尋的結果地址表`的結果變少了 (有時候會精準到剩下一個，或是剩下多個)
- 將該地址加入到`Cheat Table`
- 複製該地址`018BE0F8`
- 然後選擇`Pointer scan`窗口中的菜單選項`Pointer scanner` -> `Rescan memory - removes pointers not pointing to the right address`
- 然後把新的地址填上就好了，點擊`OK`就好了
- 最後只剩下一筆資料，雙擊左鍵將其加入`Cheat Table`
- 將該指針修改成`5000`
- 勾選該指針前方的`Check box` (用於鎖定該值)

- 點擊`Cheange poniter`
- 可以看到`Next`已經為可以點擊進入下一關

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter9/14.gif)

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>