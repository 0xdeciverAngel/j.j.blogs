---
title: Cheat Engine - 第七章 | CE Tutorial Step 6
date: 2019-06-27 09:00:00
categories:
- Cheat Engine
tags: 
- Cheat Engine
---

## 教程分析

第六關：指針（密碼是 098712）。

上一關使用了代碼查找器，這一關則使用指針的方法確定地址。

點擊 Change value 數值都會改變。點擊 Change pointer 有點類似重新開始一局遊戲，這意味著，數值的地址會改變。

你需要做的是：將數值鎖定在5000，即使點擊 Change pointer 改變了數值所在地址之後。

## 解題

以下待更新～

### 第一步

- 首先看到頁面上的 `100`
- 接著我們在需要搜尋的值那邊輸入`100`
- 點擊`首次搜尋`
- 會將搜尋結果顯示在`搜尋的結果地址表`的框內

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter7/01.png)

### 第二步

- 點擊 `Change value`，讓數字變動
- 根據 `368` 數值，在需要搜尋的值重新輸入 (這邊範例為368)
- 點擊`再次搜尋`
- 此時會發現`搜尋的結果地址表`的結果變少了 (有時候會精準到剩下一個，或是剩下多個)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter7/02.png)

### 第三步

- 針對該地址點擊右鍵
- 選擇 `Find out what writes to this address`
- 會跳出`This will attach the debugger on Cheat Engine to the current process. Continue?`，點擊`Yes`
- 點擊 `Change value`，讓數字變動
- 此時會發現`The following opcodes write to xxxxxxxx`視窗內計數器視窗的多了一條指令
- 點擊`More information`
- 在`Extra info`視窗中，找尋`copy memory The value of the pointer needed to find address is probably xxxxxxxx` 區塊，在該區塊上點右鍵
- 點擊`copy info to clipboard`

>註：copy memory The value of the pointer needed to find address is probably xxxxxxxx，表示`copy memory查找地址所需指針的值可能是xxxxxxxx`；`copy info to clipboard`將訊息複製到剪貼板。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter7/03.png)

### 第四步

- `搜尋類型`選擇`Exact Value`，`數值類型`選擇`4 Bytes`，勾选 `Hex`。
- 將剛剛複製的地址貼上`018183F0`，進行搜尋 (指針位置可能不一樣)
- 搜尋後可以看到地址為**綠色**

>註：綠色的地址表示基址，每次遊戲啟動時基址都是不會改變的。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter7/04.png)

### 第五步

- 將基地加入`Cheat Table`裡面
- 左鍵兩下`Address`，複製`Change address`視窗內的`Address` (類似：`"Tutorial-i386.exe"+201630`)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter7/05.png)

### 第六步

- 點擊`Add Address Manuallly`，手動添加地址
- 勾選`Pointer`
- 輸入剛剛複製的`Address`
- 點擊`OK`
- 可以看到`Cheat Table`，多了一個指針的地址 (類似：`P->018183F0`)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter7/06.gif)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter7/07.png)

>註：偏移量的部分，如果只有一個表示為一級偏移，兩個為二級偏移，以此類推，這個後面章節也會特別在提到。

### 第七步

- 點擊剛剛手動添加的指針地址`Value`
- 修改成`5000`
- 勾選該指針前方的`Check box` (用於鎖定該值)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter7/08.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter7/09.png)

### 第八步

- 點擊`Cheange poniter`
- 可以看到`Next`已經為可以點擊進入下一關

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter7/10.png)

## 完整動態解題

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter7/11.gif)

## 總結

### 那個一級偏移為什麼是 0？
方法一：
我們通過第二關的方法找到的地址為018183C0（你的地址應該與我的不一樣），然後在More Information 中復制的地址為018183C0，用第一個地址（目標地址）減去第二個（基址的值）就是0（這兩個完全一樣嘛...所以就是0）

方法二：
mov [edx],eax，方括號中沒有任何加減法操作，就是 0。

如果出現 mov [edx+10],eax ，就填 10 就行了，這裡的 10 是 16 進制的 10 換成 10 進制就是 16。

如果出現[eax*2+edx+00000310] 這種複雜的運算，這時就要看More Information 下面的寄存器了，如果eax=4c、edx=00801234，那麼，應該搜索較大的一個數值，然後將較小的一個數值運算得到一級偏移的結果（另外，有乘法的一定是作為偏移存在的，基址的值一定不存在乘法），即搜索00801234 找到基址，然後一級偏移為4c * 2 + 310 = 3a8，這個為16 進制運算，請使用Windows 自帶的計算器

x86 系列的指令集就是複雜，可以把好幾條運算集成成一條指令。

### 什麼是指針
這個可以從C語言講起（不過指針不是起源於C語言的哦），C語言最牛逼的東西就屬指針啦，指針本身是一個4 字節（32 位程序）或8 字節（64 位程序）的整數，如果將其解釋為一個整數，那麼就是指向那塊內存的地址。 （注意，內存裡的東西只是字節組成的數據，具體是什麼含義要看代碼是怎麼解釋，代碼將其用作指針那就是指針，代碼將其用作整數型變量那就是普通的整數型變量）

看下面這段C語言代碼，代碼中演示瞭如何對同一個地址以不同的方式解釋。

```C
#include <stdio.h>
#include <stdlib.h>
int main()
{
    int *a = 0; // 定义指针
    a = (int *)malloc(4); // 分配内存
    *a = 1; // 令其等于1
    printf("%d\n", *a); // 输出指针a指向的值
    printf("0x%08x\n", (unsigned int)a); // 以整数型的方式读取a
    free(a); // 释放内存
    return 0;
}
```

x86指令集支持指針這種東西，就是彙編語言中的方括號，mov [edx]，eax的意思就是把eax的值存到edx指向的地址中去，如果僅僅為mov edx，eax那麼就僅僅是是 令edx等於eax了。

### 基址
基址在程序每次啟動時都不會變，因為他們的地址是直接寫在代碼中的，如果他們的地址變了，這個程序還怎麼運行了？

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>