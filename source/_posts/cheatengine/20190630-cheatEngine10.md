---
title: Cheat Engine - 第十章 | CE Tutorial Step 9
date: 2019-06-30 09:00:00
categories:
- Cheat Engine
tags: 
- Cheat Engine
---

## 教程分析

第9關​​：共享代碼

有時候使敵人掉血的代碼和使自己掉血的代碼是同一個代碼，單純修改這個代碼會使要么同時對自己和敵人有利，要么同時有害，而不是對自己有利，對敵人有害。

這一關重現了這樣一種情況：己方初始血量很少，且被攻擊一次掉血很多，敵方初始血量很多，且每次攻擊只掉1滴血。

你需要做的是：不使用鎖定，讓己方不死，而敵方陣亡。

注意：生命值是 Float 類型。

## 解題

首先我們使用4 Bytes，搜尋會無法搜尋檔我們的地址，改用Double也是無法，最後使用Float可以找到。
這就是多嘗試幾次就可以知道程序裡面的值是用什麼類型的了。

### 第一步

- 首先看到頁面上的 Player 1: Dave `100`
- 接著我們在需要搜尋的值那邊輸入`100`
- `搜尋類型`選擇`Float`
- 點擊`首次搜尋`
- 會將搜尋結果顯示在`搜尋的結果地址表`的框內

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/01.png)

### 第二步

- 點擊 Player 1 的 `Attack`，讓數字變動
- 根據 `95` 數值，在需要搜尋的值重新輸入 (這邊範例為95)
- 點擊`再次搜尋`
- 此時會發現`搜尋的結果地址表`的結果變少了 (有時候會精準到剩下一個，或是剩下多個)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/02.png)

### 第三步

- 將該地址加入到`Cheat Table`
- 針對該地址點擊右鍵
- 選擇 `Find out what writes to this address`
- 會跳出`This will attach the debugger on Cheat Engine to the current process. Continue?`，點擊`Yes`
- 點擊 Player 1 的 `Attack`，讓數字變動
- 此時會發現`The following opcodes write to xxxxxxxx`視窗內計數器視窗的多了一條指令
- 點擊`Show disassembler`
- 會打開`Memory viewer`視窗

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/03.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/04.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/05.png)

### 第四步

- 在該指令上右鍵選擇`Find out what addresses this instruction accesses` (找出該指令訪問的地址)
- 會開啟 `Change addresses by ****` 視窗
- 此時各別點擊Player 1 、 2 和 3 、 4 的 `Attack` 
- 在視窗中會看到四個地址 (表示這四個血量的地址)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/06.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/07.gif)

### 第五步

- 全選這四個地址
- 點擊右鍵選擇 `Open dissect data with selected addresses` (使用選定的地址打開剖析數據)
- 會開啟 `Lock and add to structure dissect` 視窗
- 點擊 `OK`
- `Structure define` `Give the name for this structure` (結構定義，給出這個結構的名稱)
- 點擊 `OK`
- `Do you want Cheat Engine to try and fill in the most basic types of the struct using the current address?` (您是否希望Cheat Engine嘗試使用當前地址填寫結構的最基本類型？)
- 點擊 `Yes`
- `please give a starting size of the struct (You can change thos later if needed)` (請給出結構的起始大小（如果需要，可以稍後更改）)
- 點擊 `OK` 

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/08.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/09.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/10.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/11.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/12.png)

### 第六步

- 開啟的`Structure dissect:xxxxxx` 視窗，可以看到剖析數據
- 此時我們分析一下數據，看有沒有關聯性
- 我們可以大膽假設陣營 我方為：1 敵方為：2

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/13.png)

### 第七步

- 點擊`Tools` -> `Auto assemble` (簡稱：AA) 
- 將會打開`Auto assemble`視窗
- 點擊`Template` -> `Code injection`
- 確認地址就是剛才在 `Disassembler` 中選擇的那個地址，直接點 OK 即可。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/14.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/15.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/16.png)


### 第八步

- 因為彙編我也不是很熟很懂，稍微參考了下資料，照下方的修改即可。
- `This code can be injected. Are you sure?` 點擊 `Yes` 
- `The code injection eas successfull newmem= ******** Got to ********?` 點擊 `No`

修改前/後：
```C
alloc(newmem,2048)
label(returnhere)
label(originalcode)
label(exit)

newmem: //this is allocated memory, you have read,write,execute access
//place your code here

originalcode:
mov [ebx+04],eax
fldz 

exit:
jmp returnhere

"Tutorial-i386.exe"+268E7:
jmp newmem
returnhere:
```

```C
alloc(newmem,2048)
label(returnhere)
label(originalcode)
label(exit)

newmem: //this is allocated memory, you have read,write,execute access
//place your code here

mov eax,0
cmp [ebx+10],1
jne originalcode
mov eax,42C80000

originalcode:
mov [ebx+04],eax
fldz

exit:
jmp returnhere

"Tutorial-i386.exe"+268E7:
jmp newmem
returnhere:
```

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/17.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/18.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/19.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/20.png)

代碼說明：
```C
mov eax,0 //將要賦值的血量eax設置為0
cmp [ebx+10],1 //是否為我方陣營[ebx+10]->camp
jne originalcode //不是我方就直接調到源代碼塊，秒死他們!
mov eax,42C80000 //是我方的，回到初始滿血(100)注意是浮點數不能直接填100，如果不知道100浮點的十六進制，可以自己在CE中去調後復製過來
                       //(100浮點數十六進制為42C80000)
```

### 第九步

- 點擊`Restart gane and autoplay`
- 可以看到`Next`已經為可以點擊進入下一關

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/21.gif)

## 完整動態解題

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter10/22.gif)

## 總結
首先這個不是唯一解法，想信有個更多大大有其他的解法；由於本人對於彙編語言不熟，所以無法針對彙編那邊做更多的解釋。
基本上CE教學到這邊就算是結束了，後面還有一個進階CE教程，我有時間會再去實作並寫成文章。

而且剛好這篇文章是部落格的第100篇文章。真的是可喜可賀！！！

<br />
>註：以上參考了
[Cheat Engine step9攻略](https://zhuanlan.zhihu.com/p/33702835)
[CE 自带Tutorial -第9关](https://blog.csdn.net/lyq240919525/article/details/77096790)

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>