---
title: Cheat Engine - 第八章 | CE Tutorial Step 7
date: 2019-06-28 09:00:00
categories:
- Cheat Engine
tags: 
- Cheat Engine
---

## 教程分析

第七關：代碼注入（密碼是 013370）。

點一下打我減少1滴血。

你需要做的是：點一下打我改成加2滴血。

## 解題

### 第一步

- 首先看到頁面上的 `100`
- 接著我們在需要搜尋的值那邊輸入`100`
- 點擊`首次搜尋`
- 會將搜尋結果顯示在`搜尋的結果地址表`的框內

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter8/01.png)

### 第二步

- 點擊 `Hit me`，讓數字變動
- 根據 `99` 數值，在需要搜尋的值重新輸入 (這邊範例為99)
- 點擊`再次搜尋`
- 此時會發現`搜尋的結果地址表`的結果變少了 (有時候會精準到剩下一個，或是剩下多個)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter8/02.png)

### 第三步

- 針對該地址點擊右鍵
- 選擇 `Find out what writes to this address`
- 會跳出`This will attach the debugger on Cheat Engine to the current process. Continue?`，點擊`Yes`
- 點擊 `Hit me`，讓數字變動
- 此時會發現`The following opcodes write to xxxxxxxx`視窗內計數器視窗的多了一條指令
- 點擊`Show disassembler`
- 會打開`Memory viewer`視窗

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter8/03.gif)

### 第四步

- 選中指令`Tutorial-i386.exe+258ED - 83 AB 78040000 01     - sub dword ptr [ebx+00000478],01 { 1 }
` (可能會有點不一樣)
- 點擊`Tools` -> `Auto assemble` (簡稱：AA) 
- 將會打開`Auto assemble`視窗

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter8/04.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter8/05.png)

### 第五步

- 點擊`Template` -> `Code injection`
- 確認地址就是剛才在 `Disassembler` 中選擇的那個地址，直接點 OK 即可。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter8/06.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter8/07.png)

### 第六步

- 把 `sub dword ptr [ebx+00000478],01` 改成 `add dword ptr [ebx+00000478],02` 就可以了。
- `This code can be injected. Are you sure?` 點擊 `Yes` 
- `The code injection eas successfull newmem= ******** Got to ********?` 點擊 `No`


>註：原本這條代碼是把 [ebx+00000478] 這個內存地址的數據減 1，把 sub 改成 add 就是加 1 了，再把 01 改成 02 就是加 2 了。如果想更近一步了解組合語言可以參考[X86組合語言/基本指令集](https://zh.m.wikibooks.org/zh-tw/X86%E7%B5%84%E5%90%88%E8%AA%9E%E8%A8%80/%E5%9F%BA%E6%9C%AC%E6%8C%87%E4%BB%A4%E9%9B%86)

修改前/後:

```C
alloc(newmem,2048)
label(returnhere)
label(originalcode)
label(exit)

newmem: //this is allocated memory, you have read,write,execute access
//place your code here

originalcode:
sub dword ptr [ebx+00000478],01

exit:
jmp returnhere

"Tutorial-i386.exe"+258ED:
jmp newmem
nop
nop
returnhere:
```

```C
alloc(newmem,2048)
label(returnhere)
label(originalcode)
label(exit)

newmem: //this is allocated memory, you have read,write,execute access
//place your code here

originalcode:
add dword ptr [ebx+00000478],02

exit:
jmp returnhere

"Tutorial-i386.exe"+258ED:
jmp newmem
nop
nop
returnhere:
```

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter8/08.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter8/09.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter8/10.png)

### 代碼說明
`newmem:`、`originalcode:`、`exit:`、`"Tutorial-i386.exe"+258ED:`、`returnhere:` 這幾個個東西雖然都有冒號，但是的用途是不一樣的。

`newmem` 和 `"Tutorial-i386.exe"+258ED` 是具體的內存地址，他表示接下來所有指令將被寫入這個內存地址。

`originalcode`、`exit` 和 `returnhere` 是標籤，不佔據字節，不佔據指令，他表示一個暫時不知道的地址，僅僅用於跳轉。

所以從`newmem:` 到`"Tutorial-i386.exe"+258ED:` 之間的指令都會寫到新分配的地址中，`"Tutorial-i386.exe"+258ED:` 到結尾的指令都會覆蓋原始指令（實現代碼注入的效果）

而代碼最前面的這四句，就是用來定義這些內存地址或標籤的
```C
alloc(newmem,2048) // 申請 2048 字節的內存，newmem 就是這個內存地址
label(returnhere) // 定義用於跳轉的標籤 returnhere
label(originalcode) // 定義用於跳轉的標籤 originalcode
label(exit) // 定義用於跳轉的標籤 exit
```

### 第七步

- 點擊`Hit me`
- 可以看到`Next`已經為可以點擊進入下一關

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter8/11.png)


## 完整動態解題

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter8/12.gif)

## 其他修改方式
在第六步，我們使用注入的方式去修改代碼；更簡單的方式，直接在反彙編窗口中把 `sub dword ptr [ebx+00000478],01` 中的 `01` 改成 `-02`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter8/13.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter8/14.png)

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>