---
title: Cheat Engine - 第六章 | CE Tutorial Step 5
date: 2019-06-26 09:00:00
categories:
- Cheat Engine
tags: 
- Cheat Engine
---

## 教程分析

第五關：代碼查找器（密碼是 888899）。

通常，儲存數據的地址不是固定不變的，每一次重啟遊戲通常都會變動，甚至在你玩遊戲的過程中地址都有可能改變。為此，我們有兩個方法，這一關講解使用代碼查找的方法。

每一次點擊 Change value 數值都會改變。

你需要做的是：讓其不再改變。

## 解題

### 第一步

- 首先看到頁面上的 `100`
- 接著我們在需要搜尋的值那邊輸入`100`
- 點擊`首次搜尋`
- 會將搜尋結果顯示在`搜尋的結果地址表`的框內

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter6/01.png)

### 第二步

- 點擊 `Change value`，讓數字變動
- 根據 `60` 數值，在需要搜尋的值重新輸入 (這邊範例為60)
- 點擊`再次搜尋`
- 此時會發現`搜尋的結果地址表`的結果變少了 (有時候會精準到剩下一個，或是剩下多個)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter6/02.png)

### 第三步

- 針對該地址點擊右鍵
- 選擇 `Find out what writes to this address`
- 會跳出`This will attach the debugger on Cheat Engine to the current process. Continue?`，點擊`Yes`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter6/03.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter6/04.png)

>註：This will attach the debugger on Cheat Engine to the current process. Continue?
表示為：這會將Cheat Engine上的調試器附加到當前進程。 是否要繼續？

### 第四步

- 點擊 `Change value`，讓數字變動
- 此時會發現`The following opcodes write to xxxxxxxx`視窗內計數器視窗的多了一條指令

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter6/05.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter6/06.png)

### 第五步

- 對該指令點擊`Replace` (將指令變成NOP)
- 會彈跳出，是否需要對這段代碼取名 (不變更則點擊`OK`)
- 當然，右鍵這條指令，選擇替換為什麼都不做的代碼(NOP)，效果是一樣的。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter6/07.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter6/08.png)

### 第六步

- 點擊 `Change value`
- 則會看到`Next`，已經可以點選

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter6/09.png)


## 完整動態解題

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter6/10.gif)

## 總結
這一關，我們到底做了些什麼？

首先，找出改寫當前地址的操作，這個是調試器的一個功能，所以必須先把調試附加到指定進程
（這個附加操作可能被檢測到，網絡遊戲需要過非法）

找出改寫當前地址的操作，其實是設了一個內存斷點，每次有指令改寫這個內存，都會使目標進程中斷在改寫指令之後（為什麼是之後，這個是CPU所決定的，沒辦法）， 然後CE會自動記錄這條地址，並儲存寄存器信息，然後繼續運行程序。

- 在Debugger狀態下，可以點擊地址右鍵選`Browse this memory region`，查看該內存的區塊，也可以在調試器窗口點擊該指令右鍵`show this address in the disassembler`，查看該內存的區塊

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter6/11.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter6/12.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter6/13.png)

- 對該指令點擊`Replace` (將指令變成NOP)
- 此時看到反彙編窗口，會發現被替換成NOP了。
- 替換成什麼也不做之後，這個地址就不會再改變了，其他指令使用這個地址時就一直是同一個值了。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter6/14.png)

替換前/後比較圖：
![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter6/15.png)
![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter6/16.png)

### 代碼列表 (還原代碼)
點擊 Replace 會提示一個輸入名稱，這是因為這條代碼會保存在地址列表中，點擊主界面的 Advanced Options 打開地址列表。

紅色表示當前地址已經被替換成無用代碼了。

Replace 會自動將其替換成無用代碼，如果想復原可以在 Advanced Options 中右鍵這條指令，選擇 Restore with original code 恢復原始代碼。

示範：

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter6/17.gif)

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>