---
title: Cheat Engine - 第五章 | CE Tutorial Step 4
date: 2019-06-25 09:00:00
categories:
- Cheat Engine
tags: 
- Cheat Engine
---

## 教程分析

第四關：浮點類型（密碼是 890124）。

現代的遊戲中已經很少使用整數了，這一關就是搜索含小數的數值，Health（生命值）是float 類型（單精度浮點型）， Ammo（彈藥）是double 類型（雙精度浮點型）。

你需要做的是：把生命和彈藥都改成 5000。

## 解題

### 第一步

- 首先看到頁面上的 `Health:100`，`float`
- `搜尋類型`那邊選擇`Float`
- 接著我們在需要搜尋的值那邊輸入`100`
- 點擊`首次搜尋`
- 會將搜尋結果顯示在`搜尋的結果地址表`的框內

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter5/01.png)

### 第二步

- 點擊 `Hit me`，讓其扣血
- 根據 `Health:95.86` 數值，在需要搜尋的值重新輸入 (這邊範例為95.86)
- 點擊`再次搜尋`
- 此時會發現`搜尋的結果地址表`的結果變少了 (有時候會精準到剩下一個，或是剩下多個)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter5/02.png)

### 第三步

- 發現`搜尋的結果地址表`的結果，只剩下一個
- 對該地址點擊左鍵兩下，添加到下方的`CHEAT TABLE`裡面
- 根據題目需求，我們將`Value`的值進行修改為5000
- 點擊`Value`左鍵兩下
- 將數值`95.86` 修改為 `5000`，並點擊`OK`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter5/03.png)

### 第四步

- 首先點擊`新的搜索`
- 看到頁面上的 `Ammo:100`，`double`
- `搜尋類型`那邊選擇`Double`
- 接著我們在需要搜尋的值那邊輸入`100`
- 點擊`首次搜尋`
- 會將搜尋結果顯示在`搜尋的結果地址表`的框內

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter5/04.png)

### 第五步

- 點擊 `Fire`，開火
- 根據 `Ammo:99.5` 數值，在需要搜尋的值重新輸入 (這邊範例為99.5)
- 點擊`再次搜尋`
- 此時會發現`搜尋的結果地址表`的結果變少了 (有時候會精準到剩下一個，或是剩下多個)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter5/05.png)

### 第六步

- 發現`搜尋的結果地址表`的結果，只剩下一個
- 對該地址點擊左鍵兩下，添加到下方的`CHEAT TABLE`裡面
- 根據題目需求，我們將`Value`的值進行修改為5000
- 點擊`Value`左鍵兩下
- 將數值`99.5` 修改為 `5000`，並點擊`OK`
- 將會發現 `Next` 的按鈕變成可以按下，並前往下一關了。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter5/06.png)


## 完整動態解題

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter5/07.gif)

## 總結
通常遊戲中為了計算速度會選擇 Float 類型，至於遊戲開發者或遊戲框架會選擇什麼類型誰也不清楚，靠自己瞎猜吧，一個一個試，實在不行就把 Value Type 改成 All 吧。

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>