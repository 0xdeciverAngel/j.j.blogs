---
title: Cheat Engine - 第一章 | 簡介
date: 2019-06-21 09:00:00
categories:
- Cheat Engine
tags: 
- Cheat Engine
---

在部落格文章停擺約莫兩星期(備註)，現在又開始找到個有興趣的東西來做練習和寫部落格文章。這邊開始介紹Cheat Engine，主要也是為了後面所要學習嘗試的東西做鋪路。

>備註：這兩星期剛好工作太過忙碌，加上一些私人事情，造成時間完全無法分配。

## Cheat Engine 是什麼？
Cheat Engine 是一款內存修改編輯工具 ，它允許你修改你的遊戲或軟件內存數據，以得到一些其他功能。它包括16進制編輯，反彙編程序，內存查找工具。與同類修改工具相比，它具有強大的反彙編功能，且自身附帶了外掛製作工具，可以用它直接生成外掛。

## 簡介
Cheat Engine：作弊引擎，簡稱為 CE。

- 通常用於單機遊戲的內存數據修改，可以搜索遊戲中的內存數據，並進行修改、鎖定等操作
- 內置調試器，可以進行反彙編調試、斷點跟踪、代碼注入等諸多高級功能
- 支持 lua 語言，可以實現自己定義的邏輯功能，而不僅僅是簡單的鎖定數據。也可以在代碼注入的同時注入 lua 插件，使遊戲進程與 CE 進程進行交互。 CE 的大部分功能都可以通過 lua 來操作
- 它還支持 D3D Hook 功能，可以在遊戲中顯示十字準星，也可以繪製功能菜單，同時也可以對 D3D 的調用棧進行跟踪調試
- 自帶變速功能，通過 Hook 遊戲相關函數改變遊戲速度
- 自帶了一個 Trainer 的功能，可以將自己在 CE 中實現的功能單獨生成一個 exe 文件，並可以使用 lua 創建比默認樣式更加複雜的窗體或功能
- 除了 CE 界面上提供的功能，可以 lua 引擎使用更多隱藏功能，具體就要看幫助文件了

## 下載 & 安裝

[Cheat Engine 官方](https://www.cheatengine.org/)

1.從官方下載CE後 (目前支援：Windows/MacOS)
2.直接點擊安裝，全部預設即可。

## 中文化

[CE 官方中文翻譯檔](http://cheatengine.org/download/ch_cn.zip)

1.下載中文化檔
2.解壓縮至C:\Program Files\Cheat Engine 6.8.3\languages\ch_cn
3.開啟CE -> Edit -> Settings -> Languages -> Select `ch_cn` -> Click `Select Languages` -> CLick `OK`
4.重啟CE，此時將會看到已經中文化

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter1/01.png) 

>個人是不建議使用中文化，裡面的英文其實都不難。

## 介面簡介

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter1/02.png)

- 選單：暫時用不到。
- 一直在閃的按鈕是進程選擇器，旁邊兩個是打開和保存CE 的存檔文件的按鈕，記得隨時保存哦，改內存數據很可能導致遊戲崩潰的哦，如果調試的話連CE 也會一起崩潰。
- 然後是當前選擇的進程的名稱。
- 下面是一個進度條，顯示搜索進度用的。
- 右邊有個設置按鈕。
- 然後左邊是搜索到的內存地址，右邊是搜索的類型、內容、條件、內存區域等等，下面有個↘右下箭頭，是把選中的地址添加到下方的區域裡。
- 下方稱之為 Cheat Table（作弊表），就是我們得到的內存地址、數據，以及自己添加的功能等等。
- 表格上方三個按鈕：顯示內存窗口、清空作弊列表、手動添加地址。
- 下方兩個按鈕：高級選項和附加信息。

內存窗口用於調試，手動添加地址通常是手動添加帶指針的地址，高級選項中儲存了一些指令，還可以暫停當前進程，附加信息可以附帶一些使用方法、作者信息等等，高級選項中的指令與附加信息會一同保存在存檔中。

## 總結
這篇文章只是非常簡單的介紹CE這套軟體。而後面文章，將會介紹該軟件提供的教學，可以一步一步了解CE的使用方式。
這邊附上CE官方提供的[CE Wiki](https://wiki.cheatengine.org/index.php?title=Main_Page)

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>