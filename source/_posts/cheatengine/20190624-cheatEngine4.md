---
title: Cheat Engine - 第四章 | CE Tutorial Step 3
date: 2019-06-24 09:00:00
categories:
- Cheat Engine
tags: 
- Cheat Engine
---

## 教程分析

第三關：未知初始值（密碼是 419482）。

有些遊戲的血量不會直接以數值形式顯示，比如說某些小怪，之後顯示一個血條，但是打他們的時候，怪的身上會顯示承受傷害值，就是減少的血量。

你需要做的是：把它改成 5000。

## 解題
如果您是從Step 2 直接 Next 到這，可以點選`New Scan`，開始一個新的掃描。

### 第一步

- `搜尋類型`那邊選擇`Unknown initial value`
- 點擊`首次搜尋`

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter4/01.png)

>因為是未知初始值，所以第一次搜索之後列表中什麼也不會顯示，因為顯示什麼也沒有用，畢竟未知初始值沒什麼意義。同時左上角的 Found 顯示找到的數值非常多，未知初始值就是把內存中所有數值都保存下來，所以非常多，所以最好不要使用未知初始值這個東西。

### 第二步

- 點擊 `Hit me`，讓其扣血
- 根據 `-5` 數值，在需要搜尋的值重新輸入 (這邊範例為5)
- 在`搜尋類型`中選擇 `Decreased value by ...`（數值減少了...）
- 點擊`再次搜尋`
- 此時會發現`搜尋的結果地址表`的結果變少了 (有時候會精準到剩下一個，或是剩下多個)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter4/02.png)

### 第三步

- 發現`搜尋的結果地址表`的結果，剩下四個
- 其中393是最為可疑的
- 點擊左鍵兩下，加入Cheat Table內
- 對其`Value`進行修改為5000
- 將會發現 `Next` 的按鈕變成可以按下，並前往下一關了。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter4/03.png)

## 完整動態解題

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter4/04.gif)

## 題外拓展
什麼是`4294965663`?

計算機中的整數分為有符號和無符號兩種，內存就是一堆字節構成的數據，至於如何解釋這堆字節就要看是哪段代碼來讀取他。

比如 4294965663，在內存中是 FF FF F9 9F 這4個字節組成的，如果按無符號來解釋就是 4294965663 如果按有符號來解釋就是 -1633 的意思。

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter4/05.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter4/06.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter4/07.png)

可以通過右鍵該地址 `Browse this memory region` 來查看這段內存區域。 （在我們常用的這種PC機中，採用 Little Endian 的格式存儲多字節數據，即把 FFFFF99F 存儲成 9F F9 FF FF 的形式）

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter4/08.png)

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/cheatengine/chapter4/09.png)

## 總結
當我們不知道其數值的時候，我們可以透過未知的搜尋方式來查找。

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>