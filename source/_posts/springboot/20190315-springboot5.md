---
title: SpringBoot - 第五章 | 常用註解
date: 2019-03-15 17:20:00
categories:
- Spring Boot
tags: 
- Spring Boot
---

在前面文章中我們大量使用到Spring Boot的annotation(@註解)，例如：@RestController、@Controller、@RequestMapping注解，這邊會盡量把一些常用的@註解做使用方式和使用場景的介紹。

## 常用註解

### @SpringBootApplication
在前面幾章中，在啟動類裡面，都會看到這個啟動類註解，此註解是集合以下這些註解，@SpringBootConfiguration、@EnableAutoConfiguration 和 @ComponentScan 注解。

- @SpringBootConfiguration 繼承 @Configuration，對於熟悉spring的開發者而言，此註解當前類是配置類，並會將當前類內聲明的一個或多個以@Bean註解標記的方法的實例納入到Spring容器中，並且實例名就是方法名。

- @EnableAutoConfiguration 這個註解就是SpringBoot能自動配置的所在了。通過此註解，能將所有符合自動配置條件的bean的定義加載到Spring容器中。這個在後面會再拉出來特別說。

- @ComponentScan 顧名思義，就是掃描當前包及其子包下被 @Component、@Controller、@Service、@Repository 等註解的類並加入到Spring容器中進行管理。

<br />
### @Controller 和 @RestController

- @Controller 註解在類上，表示這是一個控制層bean

- @RestController 是一個結合了 @ResponseBody 和 @Controller 的註解

> @RestController Spring4之後加入的註解，原來在@Controller中返回json需要@ResponseBody來配合，如使用@RestController就不需要再配置@ResponseBody。

<br />
### @RequestMapping
處理請求網址對應的註解，可用於類或方法上。用於類上，表示類中的所有響應請求的方法都是以該網址作為父路徑。

參數           | 說明  
--------------|--------------------------------------------------------------------
value         | 指定請求的網址 
method        | 請求方法類型
consumes      | 請求的提交內容類型
produces      | 指定返回的內容類型 僅當request請求頭中的(Accept)類型中包含該指定類型才返回
params        | 指定request中必須包含某些引數值
headers       | 指定request中必須包含指定的header值
name          | 指定給mapping一個名稱，沒有什麼作用

- 常用的基本上就是 value 和 method

```java
@RequestMapping(value = "/get" , method = RequestMethod.GET)
@RequestMapping(value = "/post" , method = RequestMethod.POST)
@RequestMapping(value = "/put" , method = RequestMethod.PUT)
@RequestMapping(value = "/delete" , method = RequestMethod.DELETE)

// 另一種撰寫方式，與上面相同意思
@GetMapping(value = "/get")
@PostMapping(value = "/post")
@PutMapping(value = "/put")
@DeleteMapping (value = "/delete")
```

<br />
### @RequestBody 和 @ResponseBody

- @RequestBody註解允許request的參數在reqeust體中，常常結合前端POST請求，進行前後端交互。

- @ResponseBody註解支持將的參數在reqeust體中，通常返回json格式給前端。

<br />
### @PathVariable、@RequestParam、@RequestAttribute

- @PathVariable，如http://localhost:8080/path/${value}

<script src="https://gitlab.com/snippets/1835715.js"></script>

- @RequestParam
	-- GET如http://localhost:8080/path?${key}=${value}
	-- POST如http://localhost:8080/path ，新增form-data${key}=${value}

<script src="https://gitlab.com/snippets/1835716.js"></script>

- @RequestAttribut，用於取得過濾器或攔截器創建的、預先存在的請求屬性

<script src="https://gitlab.com/snippets/1835717.js"></script>

<br />
### @Component、@Service、@Repository

- @Component 一般的組件，可以被注入到Spring容器進行管理

- @Repository 註解於類上，表示於持久層

- @Service 註解於類上，表示於邏輯層

>註：通常一些類無法確定是使用@Service還是@Component時，註解使用@Component，比如Redis的配置類..等

<br />
### @ModelAttribute
@ModelAttribute註解可被應用在方法或方法參數上。

#### 方法使用 @ModelAttribute

- 註解在方法上的@ModelAttribute 說明方法是用於添加一個或多個屬性到model上。這樣的方法能接受與@RequestMapping註解相同的參數類型，只不過不能直接被對應到具體的請求上。

- 在同一個控制器中，註解了@ModelAttribute的方法實際上會在@RequestMapping方法之前被調用。

<script src="https://gitlab.com/snippets/1835717.js"></script>

>方法使用 @ModelAttribute通常被用來加入一些公共的屬性或數據，比如一個下拉列表所預設的幾種狀態，或者寵物的幾種類型，或者去取得一個HTML表單渲染所需要的命令對象，比如Account等。

<br />
#### 方法參數使用 @ModelAttribute
方法參數使用 @ModelAttribute 說明了該方法參數的值將由model中取得。如果model中找不到，那麼該參數會先被實例化，然後被添加到model中。在model中存在以後，請求中所有名稱匹配的參數都會填充到該參數中。

>-它可能因為@SessionAttributes註記的使用已經存在於model中
-它可能因為在同個控制器中使用了@ModelAttribute方法已經存在於model中
-它可能是由URI模板變量和類型轉換中取得的
-它可能是調用了自身的默認構造器被實例化出來的

簡單範例：

- http://localhost:8080/ma/id=jjHuang 和 http://localhost:8080/ma/jjHuang

<script src="https://gitlab.com/snippets/1835724.js"></script>

<br />
## 建立RESTful API與單元測試

### RESTful API具體設計如下

類型           | URL             |說明  
--------------|-----------------|----------------------------------------------------
GET           | /customers      | 查詢客戶列表
POST          | /customers      | 新增一個客戶
GET           | /customers/id   | 根據id查詢一個客戶
PUT           | /customers/id   | 根據id更新一個客戶
DELETE        | /customers/id   | 根據id刪除一個客戶


- Customer 的定義

<script src="https://gitlab.com/snippets/1835733.js"></script>

- 實作對 Customer 對象的操作API

<script src="https://gitlab.com/snippets/1835749.js"></script>

- 針對 Customer API 實作測試案例

<script src="https://gitlab.com/snippets/1835751.js"></script>

- 驗證

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter5/01.png)

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>