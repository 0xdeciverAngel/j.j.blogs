---
title: SpringBoot - 第四十二章 | Web開發之WebJars應用
date: 2019-05-06 22:49:48
categories:
- Spring Boot
tags: 
- Spring Boot
- WebJars
---

在之前的文章中有講到SpringBoot讀取靜態資源的位置是放在/src/main/resources/static下面，像是一些css、js、圖檔⋯等等，而最常見的就是像是Bootstrap、jquery，然而這些也都是有版本的，當專案一多的時候依賴一多的，版本上的控管就沒有這麼方便；這陣子看文章看到一個滿好用的東西WebJars，WebJars是將web前端資源（如jQuery＆Bootstrap）打成jar包文件。借助版本管理工具（Maven，gradle等）進行版本管理，保證這些Web資源版本唯一性。避免了文件混亂，版本不一致等問題。

而已下將會簡單介紹如何使用WebJars，而WebJars的原理將會在下一篇文章在特別解釋。

[WebJars官方](https://www.webjars.org/)

<br/>
## 相關配置

- 加入pom的依賴
```html
		<dependency>
			<groupId>org.webjars</groupId>
			<artifactId>bootstrap</artifactId>
			<version>4.3.1</version>
		</dependency>

		<dependency>
			<groupId>org.webjars</groupId>
			<artifactId>jquery</artifactId>
			<version>3.4.1</version>
		</dependency>
```

可以看到project依賴jar包，依賴中就有了bootstrap.jar和jquery.jar

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter42/01.png)

<br/>
## 建立index.html

<script src="https://gitlab.com/snippets/1854364.js"></script>

<br/>
## 測試
啟動應用並瀏覽 http://localhost:8080

可以看到頁面呈現的效果是已經套了bootstrap的效果

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter42/02.png)

>以上非常無敵簡單的步驟，只是想要先知道如何使用WebJars，然而原理的部分，將會再下一章特別提出來演示。

>註：以上參考了
[SpringBoot使用WebJars](https://www.jianshu.com/p/b083a4f8611d)

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>