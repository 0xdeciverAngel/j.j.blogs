---
title: SpringBoot - 第三章 | 目錄結構
date: 2019-03-14 10:46:37
categories:
- Spring Boot
tags: 
- Spring Boot
---

## 目錄結構
專案的目錄結構建置，基本上照著大原則去做初步的分類，像是應用主類、實體層、邏輯層、Web層；而在不同的專案中，常常會再根據自己的開發需求、功能...等等去做目錄結構的細分。

### 典型範例

- root package結構：com.example.myproject
- 應用主類Application.java置於root package下，通常我們會在應用主類中做一些框架配置掃描等配置，放在root package下可以幫助程序減少手工配置來加載到我們希望被Spring加載的內容
- 實體層（Entity）與資料訪問層（Repository）置於com.example.myproject.domain下
- 邏輯層（Service）置於com.example.myproject.service下
- Web層（web）置於com.example.myproject.web下

```
com
  └── example
    └── myproject
      ├── Application.java
      │
      ├── domain
      │  ├── Customer.java
      │  └── CustomerRepository.java
      │
      ├── service
      │  └── CustomerService.java
      │
      └── web
        └── CustomerController.java
```
>註：以上的典型範例參考了 [程序猿DD-翟永超](http://blog.didispace.com/) 的 [Spring Boot工程结构推荐](http://blog.didispace.com/springbootproject/#) 文章。

<br />

### 常用目錄結構

#### 程式碼層的目錄結構

- 根目錄 com.jj (一般為反向的域名格式，如域名為jj.com，而目錄結構com.jj做為開頭)

- 啟動類 (Application.java)，推薦放置根目錄com.jj下
```
└── com.jj
```

- 實體層 (Entity)
```
├── com.jj.entity (實體)
├── com.jj.model (模型)
├── com.jj.domain (JPA項目)
└── com.jj.pojo (mybatis項目)
```

>Entity：包中的類是必須和資料庫相對應的。
>Model：是為前端頁面提供數據和數據校驗的。
>Domain：字面意思是域的意思。
>POJO：POJO是Plain OrdinaryJava Object的縮寫，但它通指沒有使用Entity Beans的普通java對象，可以把POJO作為支持業務邏輯的協助類。
>註：以上這麼多的說明，如需更詳細的了解 [文章(1)](https://blog.csdn.net/qq_32447321/article/details/53148092)、[文章(2)](https://blog.csdn.net/u011665991/article/details/81201499) ，最後建議參照 [Spring Boot工程结构推荐](http://blog.didispace.com/springbootproject/) 來做配置。

- 資料訪問對象 (Data Access Object 簡稱：DAO)
```
├── com.jj.repository (JPA項目)
└── com.jj.mapper (mybatis項目)
註：它是一個面向對象的資料庫接口，負責持久層的操作，為邏輯層提供接口，主要用來封裝對資料庫的訪問
```

- 邏輯介面層 (Service)
```
└── com.jj.service
```

- 邏輯實作層 (Service Implements)
```
└── com.jj.service.impl
註：使用IntelliJ IDEA的推薦使用com.jj.serviceImpl
```

- Web層 (Controller)
```
└── com.jj.controller
註：有時候我在這邊會再細分頁面的處理和單純的API
├── com.jj.controller.web
└── com.jj.controller.api
```

- 共用工具類 (utils)
```
└── com.jj.utils
註：共用性很高的類，例 DateUtil
```

- 共用工具類 (utils)
```
└── com.jj.utils
註：共用性很高的類，例 DateFormat
```

- 資料傳輸層 (Data Transfer Object 簡稱：DTO)
```
└── com.jj.dto
註：資料傳輸對象（DTO）用於封裝多個實體類（domain）之間的關係，不破壞原有的實體類結構
```

#### 靜態資源的目錄結構

- 根目錄 resources

- 配置文件
```
resources/application.yml
註：官方推薦使用.yml來做撰寫，當然您也可以用.properties
```

- 靜態資源目錄
```
resources/static/
註：用於存放css、js、images等，基本上我會再細分
├── resources/static/css
├── resources/static/js
└── resources/static/images
```

- 模板目錄
```
resources/templates/
註：Spring Boot提供了默認配置的模板引擎主要有以下幾種，官方建議使用這些模板引擎，避免使用JSP
Thymeleaf、FreeMarker、Velocity、Groovy、Mustache
```

- mybatis映射文件
```
resources/mapper/
```

#### 目錄結構圖

<img class="desktop-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter3/01.png" alt="" style="max-width: 50%;">

<img class="mobile-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter3/01.png" alt="">

>這邊提供的目錄結構圖只是做個簡單示意，並沒有實際撰寫整個程式碼


<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>