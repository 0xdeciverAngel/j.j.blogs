---
title: SpringBoot - 第六章 | 配置說明(一)
date: 2019-03-16 23:41:00
categories:
- Spring Boot
tags: 
- Spring Boot
---

基本上使用Spring Boot是不太需要在配置一些額外的設定，但是在專案上的使用，難免會需要再額外設置一些參數作為使用。最常見的就是需要自定義Port或是一些外部服務的配置，例如資料庫的配置、mq服務位置、緩存服務位置...等等。

## 配置文件說明
Spring Boot預設的全局配置文件名為application.properties或者application.yml，應用啟動時會自動加載此文件，無需手動引入。除此之外還有一個bootstrap的全局文件，它的加載順序在application配置文件之前。

>Spring官方推薦使用的格式是.yml格式，官網都是範例都是使用yml格式進行配置講解

<br />
## 自定義屬性
application.properties提供自定義屬性的支持，這樣我們就可以把一些常量配置在這裡：

```
com.jj.name=J.J.Huang
com.jj.want=祝大家新年快樂！
```

自定義的值只需要使用註解@Value("${com.jj.name}")的方式來獲取值。

<script src="https://gitlab.com/snippets/1836383.js"></script>

>啟動應用後，輸入http://localhost:8080/ ，就可以看到"J.J.Huang,祝大家新年快樂！"

<br />
## 配置綁定
當相關聯的屬性多時，一個一個綁定太過繁瑣，官方建議使用Bean的方式做加載。

- 建立一個ConfigBean.class

- 並且使用註解@Component自動加載和指定@ConfigurationProperties(prefix = "com.jj")

<script src="https://gitlab.com/snippets/1836385.js"></script>

- 寫個BeanDemoController來測試

<script src="https://gitlab.com/snippets/1836388.js"></script>

>啟動應用後，輸入http://localhost:8080/bean ，就可以看到"J.J.Huang,祝大家新年快樂！"

<br />
## 參數間引用
在application.properties中的各個參數之間也可以直接引用來使用，就像下面的設置

```bash
com.jj.name=J.J.Huang
com.jj.want=祝大家新年快樂！
con.jj.sentence=${com.jj.name}在這邊${com.jj.want}
```

- 我們在DemoController以及BeanDemoController裡面來測試

<script src="https://gitlab.com/snippets/1836390.js"></script>

<script src="https://gitlab.com/snippets/1836392.js"></script>

>輸入http://localhost:8080/sentence ，就可以看到"J.J.Huang在這邊祝大家新年快樂！"
>輸入http://localhost:8080/bean/sentence ，就可以看到"J.J.Huang在這邊祝大家新年快樂！"

<br />
## 自定義配置文件
配置信息基本上都是放入application.properties中，但是有時候我們不希望把所有配置都放在這，我們希望可以做些區分，所以我們自訂了一些配置文件，如my.properties。
由於自定義的文件，系統不會自動加載。利用@PropertySource註解就可以引入配置文件，需要引入多個時，可使用@PropertySources設置數組，引入多個文件。

- 這邊my.properties配置兩個屬性

```
com.jj.my.age=18
com.jj.my.gender=男
```

- 在啟動應用加入註解@PropertySource(value="classpath:my.properties",encoding="utf-8")

<script src="https://gitlab.com/snippets/1836399.js"></script>

- 我們在DemoController裡面來測試

<script src="https://gitlab.com/snippets/1836405.js"></script>

>輸入http://localhost:8080/my ，就可以看到"性別：男,今年18"

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>