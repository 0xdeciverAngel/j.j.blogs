---
title: SpringBoot - 第三十二章 | @Scheduled 創建計劃任務
date: 2019-04-20 09:00:00
categories:
- Spring Boot
tags: 
- Spring Boot
---

不管在大大小小的專案上，都會遇到一些需要定時去處理的事務，例如：提醒、寄信、結算⋯等等之類的操作；或是在一個固定的循環時間上需進行的動作，例如：檢查、更新、刪除⋯等等。

## 創建計劃任務
依照官方範例，構建一個應用程序，使用Spring的@Scheduled註釋每五秒打印一次當前時間。

### 建立 ScheduledTasks

<script src="https://gitlab.com/snippets/1849066.js"></script>

>在Scheduled當特定的方法運行註解定義。注意：此示例使用`fixedRate`，指定從每次調用的開始時間開始測量的方法調用之間的間隔。還有[其他選項](https://docs.spring.io/spring/docs/current/spring-framework-reference/integration.html#scheduling) ，例如`fixedDelay`，它指定從完成任務開始測量的調用之間的間隔。您還可以使用 [@Scheduled(cron=". . .")](https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/scheduling/support/CronSequenceGenerator.html) 表達式進行更複雜的任務調度。

>注意：要調度的方法必須具有void返回，並且不得指望任何參數。如果該方法需要與應用程序上下文中的其他對象進行交互，則通常會通過依賴項注入來提供這些對象。

### 修改 Chapter32Application (啟用定時任務)

<script src="https://gitlab.com/snippets/1849065.js"></script>

>`@EnableScheduling`確保創建後台任務執行程序。沒有它，沒有任何安排。

### 測試

```crmsh

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.1.4.RELEASE)

2019-04-19 11:14:08.708  INFO 3505 --- [           main] c.j.l.s.chapter32.Chapter32Application   : Starting Chapter32Application on localhost with PID 3505 (/Users/morose/Documents/workspace-SpringBoot/chapter-32/target/classes started by morose in /Users/morose/Documents/workspace-SpringBoot/chapter-32)
2019-04-19 11:14:08.712  INFO 3505 --- [           main] c.j.l.s.chapter32.Chapter32Application   : No active profile set, falling back to default profiles: default
2019-04-19 11:14:09.359  INFO 3505 --- [           main] o.s.s.c.ThreadPoolTaskScheduler          : Initializing ExecutorService 'taskScheduler'
當前時間：2019-04-19 11:14:09
2019-04-19 11:14:09.382  INFO 3505 --- [           main] c.j.l.s.chapter32.Chapter32Application   : Started Chapter32Application in 1.02 seconds (JVM running for 1.687)
當前時間：2019-04-19 11:14:14
當前時間：2019-04-19 11:14:19
當前時間：2019-04-19 11:14:24
當前時間：2019-04-19 11:14:29

```

>顯示日誌輸出，您可以從日誌中看到它在後台線程上。您應該每5秒鐘看一次計劃任務。

<br />
## @Scheduled 詳細介紹

- 上一次執行完畢時間點之後5秒再執行。
```java
@Scheduled(fixedDelay=5000)
public void doSomething() {
    // something that should execute periodically
}
```

- 上一次開始執行時間點之後5秒再執行。
```java
@Scheduled(fixedRate=5000)
public void doSomething() {
    // something that should execute periodically
}
```

- 第一次延遲1秒後執行，之後按fixedRate的規則每5秒執行一次
```java
@Scheduled(initialDelay=1000, fixedRate=5000)
public void doSomething() {
    // something that should execute periodically
}
```

- 簡單的定期調度表達不夠，則可以提供cron表達式。例如，以下僅在工作日執行
```java
@Scheduled(cron="*/5 * * * * MON-FRI")
public void doSomething() {
    // something that should execute on weekdays only
}
```

### cron 表達式說明

\* 第一位，表示秒，取值0-59
\* 第二位，表示分，取值0-59
\* 第三位，表示小時，取值0-23
\* 第四位，日期天/日，取值1-31
\* 第五位，日期月份，取值1-12
\* 第六位，星期，取值1-7，星期一，星期二...，注意：不是第1週，第二週的意思，另外：1表示星期天，2表示星期一。
\* 第7為，年份，可以留空，取值1970-2099

### cron 特殊符號說明

符號 | 說明
--- | ---
(\*)星號 | 可以理解為每的意思，每秒，每分，每天，每月，每年...
(?)問號 | 問號只能出現在日期和星期這兩個位置，表示這個位置的值不確定，<br>每天3點執行，所以第六位星期的位置，我們是不需要關注的，<br>就是不確定的值。同時：日期和星期是兩個相互排斥的元素，<br>通過問號來表明不指定值。比如，1月10日，比如是星期1，<br>如果在星期的位置是另指定星期二，就前後衝突矛盾了。
(-)減號 | 表達一個範圍，如在小時字段中使用“10-12”，則表示從10到12點，即10,11,12
(,)逗號 | 表達一個列表值，如在星期字段中使用“1,2,4”，則表示星期一，星期二，星期四
(/)斜杠 | 如：x/y，x是開始值，y是步長，比如在第一位（秒） 0/15就是，<br>從0秒開始，每15秒，最後就是0，15， 30，45，60 另：*/y，等同於0/y

### cron 在線產生器
http://cron.qqe2.com/
http://www.pppet.net/
http://www.bejson.com/othertools/cron/
https://www.beejson.com/tool/cron.html

>註：以上參考了
[Spring](https://spring.io/) 的 [Scheduling Tasks](https://spring.io/guides/gs/scheduling-tasks/) 文章。
[程序猿DD](http://blog.didispace.com/) 的 [Spring Boot中使用@Scheduled创建定时任务](http://blog.didispace.com/springbootscheduled/) 文章。
[oKong](https://my.oschina.net/xiedeshou) 的 [SpringBoot | 第二十二章：定时任务的使用](https://my.oschina.net/xiedeshou/blog/1930253)文章。

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>