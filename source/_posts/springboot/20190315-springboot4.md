---
title: SpringBoot - 第四章 | Web開發
date: 2019-03-15 09:23:37
categories:
- Spring Boot
tags: 
- Spring Boot
---

在前面 [第一章 - SpringBoot專案創建](https://morosedog.gitlab.io/j.j.blogs/springboot-20190313-springboot1/) 中我們完成了一個簡單的RESTful Service，體驗到快速又簡單的開發特性。而在這邊我們要教學的是如何把處理的結果資料渲染到頁面上。

## 靜態資源
在 [第三章 - SpringBoot目錄結構](https://morosedog.gitlab.io/j.j.blogs/springboot-20190314-springboot3/) 中有特別提到靜態資源的目錄結構，因為我們在開發web的時候，大量使用到js、css、圖片等靜態資源。

### 預設配置

SpringBoot的預設的靜態文件目錄是：
- /static
- /public
- /resources
- /META-INF/resources

>註：我們在src/main/resources/static下面放入一張圖片xxx.jpg，然後啟動專案，瀏覽 http://localhost:8080/xxx.jpg ，將會看到該圖片被正常開啟。

## 渲染頁面

在之前的範例中，我們通過@RestController來處理請求，這邊稍微講解一下：
- @RestController 註解相當於 @ResponseBody ＋ @Controller合在一起的作用。
- 如只使用 @RestController註解Controller，則Controller中的方法無法返回web頁面，配置的視圖解析器InternalResourceViewResolver不起作用，返回的內容就是Return內的內容。
- 如果需要返回到指定頁面，則需要用 @Controller配合視圖解析器InternalResourceViewResolver才行。

### 模板引擎
為了實現動態的html，SpringBoot是通過模版引擎進行頁面結果渲染的，官方提供預設配置的模版引擎主要為：

- [Thymeleaf](https://www.thymeleaf.org/)
- [FreeMarker](https://freemarker.apache.org/)
- [Velocity](http://velocity.apache.org/engine/1.7/user-guide.html)
- [Groovy](http://docs.groovy-lang.org/docs/next/html/documentation/template-engines.html#_the_markuptemplateengine)
- [Mustache](https://mustache.github.io/)

>以下教學主要拿Thymeleaf、FreeMarker做教學，而官方已經不推薦使用JSP了，因網路上已經有需多JSP使用教學，這邊就不再贅述

## Thymeleaf
- Thymeleaf是一個適用於Web和獨立環境的服務器端Java模板引擎。

- Thymeleaf的主要目標是為您的開發工作流程帶來優雅的自然模板 - 可以在瀏覽器中正確顯示的HTML，也可以用作靜態原型，從而在開發團隊中實現更強大的協作。

- 通過Spring Framework模塊，與您喜歡的工具的大量集成，以及插入您自己的功能的能力，Thymeleaf是現代HTML5 JVM Web開發的理想選擇 - 儘管它可以做得更多。

#### 官方範例：
Thymeleaf編寫的HTML模板仍然像HTML一樣工作，讓在您的應用程序中運行的實際模板繼續作為有用的設計工件。
<script src="https://gitlab.com/snippets/1835631.js"></script>

Spring Boot中使用Thymeleaf，需要引入下面依賴，並在預設的模板路徑src/main/resources/templates下編寫模板文件。
```html
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-thymeleaf</artifactId>
		</dependency>
```

#### 使用教學：

- 新建`controller`package

```
com.jj.learning.springboot.chapter4.controller
```

- 新建ThymeleafController.class

<script src="https://gitlab.com/snippets/1835635.js"></script>

- 新建模板文件`thymeleaf.html`

```
src/main/resources/templates/thymeleaf.html
```

<script src="https://gitlab.com/snippets/1835636.js"></script>

如上頁面，直接打開html頁面展現Hello World!預設值，但是啟動程序後，瀏覽 http://localhost:8080/thymeleaf/mv?name=jjHuang 或者 http://localhost:8080/thymeleaf/map?name=jjHuang ，則是展示Controller中name 和 from的值，做到了不破壞HTML自身內容的數據邏輯分離。

<img class="desktop-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter4/01.png" alt="" style="max-width: 50%;"><img class="desktop-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter4/02.png" alt="" style="max-width: 50%;">

<img class="mobile-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter4/01.png" alt="">
<img class="mobile-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter4/02.png" alt="">

#### Thymeleaf的預設參數配置：

```YAML
# 啟用緩存:建議生產開啟
spring.thymeleaf.cache=false
# 建議模版是否存在
spring.thymeleaf.check-template-location=true
# Content-Type 值
spring.thymeleaf.content-type=text/html
# 是否啟用
spring.thymeleaf.enabled=true
# 模版編碼
spring.thymeleaf.encoding=UTF-8
# 應該從解析中排除的視圖名稱列表（用逗號分隔）
spring.thymeleaf.excluded-view-names=
# 模版模式
spring.thymeleaf.mode=HTML5
# 模版存放路徑
spring.thymeleaf.prefix=classpath:/templates/
# 模版後綴
spring.thymeleaf.suffix=.html
```

## Freemarker
- Apache FreeMarker™是一個模板引擎：一個Java庫，用於根據模板和更改數據生成文本輸出（HTML網頁，電子郵件，配置文件，源代碼等）。模板是用FreeMarker模板語言（FTL）編寫的，這是一種簡單的專用語言（不像PHP這樣的完整編程語言）。通常，使用通用編程語言（如Java）來準備數據（發布資料庫查詢，進行業務計算）。然後，Apache FreeMarker使用模板顯示準備好的數據。在模板中，您將專注於如何呈現數據，而在模板之外，您將關注於要呈現的數據。
![](https://freemarker.apache.org/images/overview.png)

#### 官方範例：
<script src="https://gitlab.com/snippets/1835639.js"></script>

Spring Boot中使用Freemarker，需要引入下面依賴，並在預設的模板路徑src/main/resources/templates下編寫模板文件。
```html
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-freemarker</artifactId>
		</dependency>
```

#### 使用教學：

- application.properties加入相關配置：

```bash
# 緩存配置 開發階段應該配置為false 因為經常會改
spring.freemarker.cache=false
# 模版後綴名 默認為ftl
spring.freemarker.suffix=.html
# 文件編碼
spring.freemarker.charset=UTF-8
# 模版加載的目錄
spring.freemarker.template-loader-path=classpath:/templates/
# 配置
# locale 該選項指定該模板所用的國家/語言選項
# number_format 指定格式化輸出數字的格式：currency、
# boolean_format 指定兩個布爾值的語法格式,默認值是true,false
# date_format,time_format,datetime_format 定格式化輸出日期的格式
# time_zone 設置格式化輸出日期時所使用的時區
# 數字 千分位標識
spring.freemarker.settings.number_format=,##0.00
```

- 新建FreemarkerController.class

<script src="https://gitlab.com/snippets/1835640.js"></script>

- 新建模板文件`freemarker.html`

```
src/main/resources/templates/freemarker.html
```

<script src="https://gitlab.com/snippets/1835641.js"></script>

如上頁面，直接打開html頁面展現Hello World!名稱：${name}，来自：${from}，但是啟動程序後，瀏覽 http://localhost:8080/freemarker/mv?name=jjHuang 或者 http://localhost:8080/freemarker/map?name=jjHuang

<img class="desktop-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter4/03.png" alt="" style="max-width: 50%;"><img class="desktop-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter4/04.png" alt="" style="max-width: 50%;">

<img class="mobile-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter4/03.png" alt="">
<img class="mobile-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter4/04.png" alt="">

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>