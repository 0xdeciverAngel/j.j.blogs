---
title: SpringBoot - 第九章 | Swagger2的集成和使用
date: 2019-03-19 08:10:00
categories:
- Spring Boot
tags: 
- Spring Boot
- Swagger
---

Spring Boot 能夠快速開發、建置、部署的特性，所以很常被運用在建置RESTful API。而我們構建RESTful API的目的通常都是由於多終端的原因，這些終端會共用很多底層業務邏輯，因此我們會抽像出這樣一層來同時服務於多個移動端或者Web前端。

而在現實中RESTful API很有可能要面對多個開發人員或多個開發團隊：IOS開發、Android開發或是Web開發等。為了減少與其他團隊平時開發期間的頻繁溝通成本，傳統做法我們會創建一份RESTful API文件來記錄所有接口細節，然而這樣的做法有以下幾個問題

- 由於接口眾多，並且細節複雜（需要考慮不同的HTTP請求類型、HTTP頭部信息、HTTP請求內容等），高質量地創建這份文件本身就是件非常吃力的事。

- 隨著時間推移，不斷修改接口實現的時候都必須同步修改接口文件，而文件與代碼又處於兩個不同的媒介，除非有嚴格的管理機制，不然很容易導致不一致現象。

本文將介紹RESTful API的重磅好夥伴Swagger2，它可以輕鬆的整合到Spring Boot中，並與Spring MVC程序配合組織出強大RESTful API文件。可以減少我們創建文件的工作量，同時說明內容又整合入實現代碼中，讓維護文件和修改代碼整合為一體，可以讓我們在修改代碼邏輯的同時方便的修改文件說明。另外Swagger2也提供了強大的頁面測試功能來調試每個RESTful API。

## Swagger2

>以下會使用 [第五章 - SpringBoot常用註解](https://morosedog.gitlab.io/j.j.blogs/springboot-20190315-springboot5/) 中的 [建立RESTful API與單元測試](https://morosedog.gitlab.io/j.j.blogs/springboot-20190315-springboot5/#%E5%BB%BA%E7%AB%8BRESTful-API%E8%88%87%E5%96%AE%E5%85%83%E6%B8%AC%E8%A9%A6) 來做示範

- Spring Boot中使用Swagger2，需要引入下面依賴

```html
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>2.9.2</version>
		</dependency>
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>2.9.2</version>
		</dependency>
```

- 創建Swagger2配置類
於Application.java同級創建Swagger2的配置類Swagger2

<script src="https://gitlab.com/snippets/1836479.js"></script>

>此時其實已經可以生產文件內容。

效果圖片：

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter9/01.png)

>但是這樣的文件主要針對請求本身，而描述主要來源於函數等命名產生，對用戶並不友好，我們通常需要自己增加一些說明來豐富文件內容。

- 增加 Customer 文件內容

<script src="https://gitlab.com/snippets/1836495.js"></script>

- 增加 CustomerController 文件內容

<script src="https://gitlab.com/snippets/1836494.js"></script>

效果圖片：

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter9/02.png)

<br />
## 文件瀏覽及使用

- 瀏覽的網址：http://localhost:8080/swagger-ui.html

- 點擊需要使用的API，並點擊「try it out」，並輸入相對應的參數，執行「Execute」

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter9/03.png)

- 返回的結果

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter9/04.png)

>Swagger上面提供了curl的指令還有Request URL，讓開發者可以快速使用

- 這邊我們使用查詢客戶列表

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter9/05.png)

>可見 Response body ，有剛剛新建的客戶資料

>註：以上參考了 
[程序猿DD-翟永超](http://blog.didispace.com/) 的 [Spring Boot中使用Swagger2构建强大的RESTful API文档](http://blog.didispace.com/springbootswagger2/) 文章。

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>