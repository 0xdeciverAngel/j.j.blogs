---
title: SpringBoot - 第一章 | 專案創建
date: 2019-03-13 11:18:55
categories:
- Spring Boot
tags: 
- Spring Boot
---

Spring框架功能很強大，但是需要配置很多東西。因此就有了Spring Boot。Spring Boot框架的核心就是自動配置，只要存在相應的jar包，Spring就幫我們自動配置。如果默認配置不能滿足需求，我們還可以替換掉自動配置類，使用我們自己的配置。另外，Spring Boot還集成了嵌入式的Web服務器，系統監控等很多有用的其他的功能，讓我們可以快速的構建企業及應用程序。

## Spring Boot 核心功能
>1.獨立Spring項目：獨立運行一個Spring Boot項目只需要通過java -jar xxx.jar。<br />
2.內置Servlet容器：內嵌Tomcat、Jetty，這樣可不用以war包形式部署。<br />
3.Starter簡化配置：提供了一堆的starter來簡化 maven 依賴。<br />
4.自動配置：Spring Boot會根據在類路徑中的jar包、類，為jar包裡的類自動配置Bean。亦可以自定義自動配置。<br />
5.xml配置：Spring Boot通過條件註解來實現的，不需要任何xml配置即可實現Sping Boot的所有配置。

## STS插件
這邊推薦使用 [Spring Tool Suite](https://spring.io/tools3/sts/all) (簡稱：STS)，來新建Spring Boot專案，如不想使用IDE的話，從 [start.spring.io](start.spring.io) 創建也可。
>網路上已經有很多STS安裝方式，這邊就不再贅述。

## 創建專案

### 新建項目

- 選擇 File -> New -> Sptring Starter Project

- 將相關內容及設定選擇完畢 -> Next

>Group：一般為反向的域名格式，如域名為jj.com，而group一般以com.jj做為開頭。
Artifact：唯一標識，一般為項目名稱。

<img class="desktop-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter1/01.png" alt="" style="max-width: 50%;">

<img class="mobile-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter1/01.png" alt="">

- 選擇Spring Boot Version、依賴包 -> Finish

<img class="desktop-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter1/02.png" alt="" style="max-width: 50%;">

<img class="mobile-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter1/02.png" alt="">

- 建立好後的專案結構

<img class="desktop-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter1/03.png" alt="" style="max-width: 50%;">

<img class="mobile-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter1/03.png" alt="">

```
./chapter-1
├── HELP.md
├── mvnw
├── mvnw.cmd
├── pom.xml
├── src
│   ├── main
│   │   ├── java
│   │   │   └── com
│   │   │       └── jj
│   │   │           └── learning
│   │   │               └── springboot
│   │   │                   └── chapter1
│   │   │                       ├── Chapter1Application.java
│   │   │                       └── ServletInitializer.java
│   │   ├── resources
│   │   │   ├── application.properties
│   │   │   ├── static
│   │   │   └── templates
│   │   └── webapp
│   └── test
│       └── java
│           └── com
│               └── jj
│                   └── learning
│                       └── springboot
│                           └── chapter1
│                               └── Chapter1ApplicationTests.java
└── target
    ├── classes
    ├── m2e-wtp
    └── test-classes
```
>- Chapter1Application.java：主函數，啟動類
- statics：存放靜態資源 js/css/images 等
- templates：存放 html 模板(Thymeleaf、FreeMarker、Velocity、Groovy、Mustache)
- application.properties：主要的配置文件，SpringBoot啟動時候會自動加載
- test：測試案例的目錄
- pom.xml：Maven的基础，包含了所依賴的jar和plugin。


- pom.xml內容

<script src="https://gitlab.com/snippets/1834958.js"></script>

<br />

## Hello World!
開始來建立自己的第一個網站Hello World！

### 編寫Controller

- 新建`controller`package

```
com.jj.learning.springboot.chapter1.controller
```

- 新建DemoController.class

<script src="https://gitlab.com/snippets/1834960.js"></script>

>程式碼中的@RestController、@RequestMapping(value = "/demo", method = RequestMethod.GET)，是什麼？<br />在未來的文章中會在特別解釋。

### 啟動Application

- 對著Chapter1Application -> 右键 -> Run As –> Java Application

```crmsh
Console:


  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.1.3.RELEASE)

2019-03-13 12:19:39.230  INFO 8921 --- [           main] c.j.l.s.chapter1.Chapter1Application     : Starting Chapter1Application on localhost with PID 8921 (/Users/morose/Documents/workspace-SpringBoot/chapter-1/target/classes started by morose in /Users/morose/Documents/workspace-SpringBoot/chapter-1)
2019-03-13 12:19:39.233  INFO 8921 --- [           main] c.j.l.s.chapter1.Chapter1Application     : No active profile set, falling back to default profiles: default
2019-03-13 12:19:40.256  INFO 8921 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
2019-03-13 12:19:40.288  INFO 8921 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2019-03-13 12:19:40.288  INFO 8921 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.16]
2019-03-13 12:19:40.301  INFO 8921 --- [           main] o.a.catalina.core.AprLifecycleListener   : The APR based Apache Tomcat Native library which allows optimal performance in production environments was not found on the java.library.path: [/Users/morose/Library/Java/Extensions:/Library/Java/Extensions:/Network/Library/Java/Extensions:/System/Library/Java/Extensions:/usr/lib/java:.]
2019-03-13 12:19:40.415  INFO 8921 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2019-03-13 12:19:40.415  INFO 8921 --- [           main] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 1130 ms
2019-03-13 12:19:40.679  INFO 8921 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
2019-03-13 12:19:40.921  INFO 8921 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2019-03-13 12:19:40.924  INFO 8921 --- [           main] c.j.l.s.chapter1.Chapter1Application     : Started Chapter1Application in 1.991 seconds (JVM running for 2.33)

```
### 驗證

- http://localhost:8080/demo

>Spring Boot 預設port:8080

<img class="desktop-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter1/04.png" alt="" style="max-width: 50%;">

<img class="mobile-only" src="https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter1/04.png" alt="">

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>