---
title: SpringBoot - 第十六章 | MyBatis整合 (XML方式)
date: 2019-03-30 21:13:54
categories:
- Spring Boot
tags: 
- Spring Boot
- MyBatis
---

在前面的文章中 [第十五章 - SpringBoot MyBatis整合 (註解方式)](https://morosedog.gitlab.io/j.j.blogs/springboot-20190329-springboot15/) 介紹如何使用MyBatis的透過註解的方式基本訪問資料庫。

這邊來介紹使用Spring Boot整合MyBatis，通過xml的方式來做使用。

## 使用範例

### 資料庫範例
這邊用Customer來做範例
```sql
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(64) DEFAULT NULL COMMENT '姓名',
  `age` smallint(3) DEFAULT NULL COMMENT '年齡',
  `create_by` varchar(50) NOT NULL COMMENT '創建人員',
  `create_dt` datetime NOT NULL COMMENT '創建時間',
  `modify_by` varchar(50) DEFAULT NULL COMMENT '修改人員',
  `modify_dt` datetime DEFAULT NULL COMMENT '修改時間',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

### 相關配置

- 加入pom的依賴
```html
		<dependency>
			<groupId>org.mybatis.spring.boot</groupId>
			<artifactId>mybatis-spring-boot-starter</artifactId>
			<version>2.0.0</version>
		</dependency>
```

- src/main/resources/application.properties中配置資料源信息
```bash
# 資料源配置
spring.datasource.url=jdbc:mysql://localhost:3306/test
spring.datasource.username=root
spring.datasource.password=root
spring.datasource.driver-class-name=com.mysql.jdbc.Driver

# 指定mybatis去哪裡掃描mapper
mybatis.mapper-locations=classpath:mapper/*.xml
```

### 建立實體 (Entity)
<script src="https://gitlab.com/snippets/1840468.js"></script>

>此時參數名稱故意跟資料庫欄位名稱不一樣，下方透過xml內的resultMap來做對應。

### 建立資料訪問對象 (Dao)
<script src="https://gitlab.com/snippets/1840469.js"></script>

### 建立Mapper.xml
<script src="https://gitlab.com/snippets/1840470.js"></script>

>Entity裡面的參數名稱和資料庫欄位名稱不一樣的時候，可以設定對應的方式，就是透過以下方式做對應的；
column表示Entity裡面的參數名稱，property 表示資料庫欄位名稱。
```html
	<resultMap id="BaseResultMap" type="com.jj.learning.springboot.chapter16.domain.Customer">
		<id column="id" property="id" jdbcType="INTEGER" />
		<result column="name" property="name" jdbcType="VARCHAR" />
		<result column="age" property="age" jdbcType="INTEGER" />
		<result column="createBy" property="create_by" jdbcType="VARCHAR" />
		<result column="createDt" property="create_dt" jdbcType="TIMESTAMP" />
		<result column="modifyBy" property="modify_by" jdbcType="VARCHAR" />
		<result column="modifyDt" property="modify_dt" jdbcType="TIMESTAMP" />
	</resultMap>
```

### 單元測試
<script src="https://gitlab.com/snippets/1840471.js"></script>

測試結果

![](https://gitlab.com/morosedog/j.j.blogs/raw/master/source/images/springboot/chapter16/01.png)

### 其他
.xml可以放在哪裡呢？

兩個位置可以放，第一個是直接放在UserMapper所在的包下面，但是需要在Application設定
@MapperScan(basePackages = "com.jj.learning.springboot.chapter16.domain")
設定後CustomerMapper上方的@Mapper就可以移除了。

>注：放在這裡的.xml會被自動掃描到，但是有另外一個Maven帶來的問題，就是java目錄下的xml資源在項目打包時會被忽略掉，所以，如果.xml放在包下，需要在pom.xml文件中再添加如下配置，避免打包時java目錄下的XML文件被自動忽略掉
```html
<build>
    <resources>
        <resource>
            <directory>src/main/java</directory>
            <includes>
                <include>**/*.xml</include>
            </includes>
        </resource>
        <resource>
            <directory>src/main/resources</directory>
        </resource>
    </resources>
</build>
```

第二個地方，.xml也可以直接放在resources目錄下，這樣就不用擔心打包時被忽略了，但是放在resources目錄下，又不能自動被掃描到，需要添加額外配置。例如我在resources目錄下創建mapper目錄用來放mapper文件。
```bash
# 指定mybatis去哪裡掃描mapper
mybatis.mapper-locations=classpath:mapper/*.xml
```

>注：如此配置之後，mapper就可以正常使用了。注意第二種方式不需要在pom.xml文件中配置文件過濾。

>註：以上參考了 
[程序猿DD-翟永超](http://blog.didispace.com/) 的 [Spring Boot整合MyBatis](http://blog.didispace.com/springbootmybatis/) 文章。
[江南一点雨](https://segmentfault.com/u/lenve) 的 [最简单的SpringBoot整合MyBatis教程](https://segmentfault.com/a/1190000018559769) 文章。

<div class="footer">
	<i class="fa fa-lightbulb-o"></i>本文由<a href="https://morosedog.gitlab.io/jjblogs/">J.J. Huang</a> 創作，採用<a href="https://creativecommons.org/licenses/by/3.0/tw/" target="_blank" rel="external">CC BY 3.0 TW協議</a> 進行許可。可自由轉載、引用，但需署名作者且註明文章出處。
</div>